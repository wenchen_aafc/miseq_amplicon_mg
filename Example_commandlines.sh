#!/bin/bash
#$ -pe smp 12
#$ -q all.q
#$ -cwd
#$ -S /bin/bash



# paired_join_ 
#perl /isilon/biodiversity/users/chenw_lab/Bitbucket/miseq_amplicon_mg/new_Miseq_debug_trim_then_join.pl  -i /isilon/biodiversity/users/chenw_lab/Processing/New_Miseq_Data/test_miseq/ -o /isilon/biodiversity/users/chenw_lab/Processing/New_Miseq_Data/150414_test2_output3 --mixed TRUE --paired_end TRUE --join TRUE --singletons TRUE --tax_method mothur --trim_method mothur --otu_method uclust --threads 24 --amplicon ITS --oligo_file /isilon/biodiversity/users/chenw_lab/Processing/New_Miseq_Data/oligo_file_ITS52_34 --trim_then_join TRUE --qthreshold 15 --perc_max_diff 10 --min_overlap 25



# new_454_pick_open_reference_otu.pl
# ITS
perl /isilon/biodiversity/users/chenw_lab/Bitbucket/miseq_amplicon_mg/new_454_pick_open_reference_otu.pl -i /isilon/biodiversity/users/chenw_lab/Manuscripts/Wen/T62/ITS/input -o /isilon/biodiversity/users/chenw_lab/Manuscripts/Wen/T62/ITS/output_test --ITS TRUE --tax_method rdp --otu_method cdhit --oligo_file /isilon/biodiversity/users/chenw_lab/Processing/New_Miseq_Data/oligo_file_ITS5_4 --amplicon ITS --threads 24

#16S
perl /isilon/biodiversity/users/chenw_lab/Bitbucket/miseq_amplicon_mg/new_454_pick_open_reference_otu.pl -i /isilon/biodiversity/users/chenw_lab/Manuscripts/Wen/T62/Bac/input -o /isilon/biodiversity/users/chenw_lab/Manuscripts/Wen/T62/Bac/output_test --ITS FALSE --tax_method rdp --otu_method cdhit --oligo_file /isilon/biodiversity/users/chenw_lab/Processing/New_Miseq_Data/oligo_file_UNBac --amplicon 16S --threads 24

# qsub files at: /home/AAFC-AAC/chenw/chenw_lab/Manuscripts/Wen/T62/

rm -fr input_test/fastq_files
rm -fr output_test/*
bash ../qsub_processing_T62_16S.sh

rm -fr input_test/fastq_files
rm -fr output_test/*
bash ../qsub_processing_T62_ITS.sh

