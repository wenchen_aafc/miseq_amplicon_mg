#This is the MSAMP's configuration file.

##IMPORTANT: If you are using this file, do not use any command-line options
##	     (Command line options will be overwritten by this file if used)

------------------------------------------------THREADS------------------------------------------------
#This value determines how many threads will be used. In general, 1 CPU  core = 1 thread.

THREAD_COUNT=8

----------------------------------------------PAIRED-END-----------------------------------------------
#This is used to specify if the data is paired-end or not
PAIRED_END=TRUE

-------------------------------------------MEMORY-ALLOCATION-------------------------------------------
#THIS IS IMPORTANT IF YOU ARE USING:
#  cdhit for clustering
#  rdp for assigning taxonomy
#  MEGAN?
#rdp and cdhit requires memory allocation, the parameter below sets the maximum memory allocation (MB)
MAX_MEMORY=8000

-----------------------------------------------TRIMMING------------------------------------------------
#Trimming method (usearch, mothur)
TRIM_METHOD=usearch

#Minimum length of sequences to keep
MINIMUM_LENGTH=100

#The parameter below has different meanings for different methods
#MOTHUR: Set a minimum average quality score allowed over a window
#USEARCH: Truncate the sequence at the first position having quality score <= N
QUALITY_THRESHOLD=20

#Set a minimum average quality score allowed over a window, only used for mothur
WINDOW_SIZE=10

-----------------------------------------------Joining-------------------------------------------------
#Choose whether to join the sequences or not
JOIN=FALSE

#IF NO_JOIN=FALSE, THEN:
#Minimum overlap required when joining forward and reverse sequence
MINIMUM_OVERLAP=50

#Maximum difference for the overlapping region when joining
#(i.e. 1% maximum difference = 99% minimum similarity)
MAXIMUM_PERCENTAGE_DIFFERENCE=1

#IF SINGLETONS=TRUE THEN: process all the unjoined sequences along with joined sequences
#IF SINGLETONS=FALSE THEN: do not process unjoined sequences
SINGLETONS=TRUE

----------------------------------------ITS-REGION-EXTRACTION------------------------------------------
#ITS region extraction using ITSx (ITS1, ITS2, BOTH, NULL)
#If NULL, then no ITS extraction (e.g. 16S region)
ITS_EXTRACTION=BOTH

#To pick which organism groups you want to look for
#MORE DETAILS
KINGDOMS=F

---------------------------------------------CLUSTERING------------------------------------------------
#Clustering/OTU picking method (usearch, cdhit)
#USEARCH may have memory problems
OTU_METHOD=cdhit

#Similarity used for clustering
SIMILARITY=0.97

#Cluster size (only for usearch)
OTU_SIZE=1

----------------------------------------TAXONOMY-ASSIGNMENT--------------------------------------------
#Assigning taxonomy methods (mothur, rdp, MEGAN, Blast2lca) 
##IMPORTANT: If using MEGAN or LCA for assigning taxonomy then pipeline will use BLASTN regardless of this parameter
TAX_METHOD=mothur

#Confidence level
CONFIDENCE=0.85

#BLASTN parameters (MUST PROVIDE A BLAST DATABASE)
#Expected value for saving hits
EVALUE=10
#Show alignments for this number of database sequences, or number of top-hits
#(Output file size depend on how many sequences you keep)
NUM_ALIGNMENTS=50

--------------------------------------------DATABASES--------------------------------------------------
#If run MSAMP in the working directory containing MSAMP.pl, it looks for reference fasta and taxonomy files in the Reference folder.

#If you want to use another location, you can specify the FULL PATH
REFERENCE_FASTA=/home/AAFC-AAC/zhuk/reference/unite/mothur/sh_refs_qiime_ver7_dynamic_s_02.03.2015.fasta
REFERENCE_TAX=/home/AAFC-AAC/zhuk/reference/unite/mothur/sh_taxonomy_qiime_ver7_dynamic_s_02.03.2015.txt

#BLAST database
#(Currently testing)
BLAST_DB=/Absolute/Path/to/blastdb

----------------------------------------CLOSED-REFERENCE-----------------------------------------------
#If you choose closed reference, the parameters in the sections CLUSTERING and TAXONOMY-ASSIGNMENT will not affect anything.
##IMPORTANT: Will require a reference sequences file and a taxonomy mapping file
CLOSED_REFERENCE=FALSE
