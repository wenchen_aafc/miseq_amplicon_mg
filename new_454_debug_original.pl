#!/usr/bin/env perl

use strict;
use warnings;
#cat rdp_assigned_taxonomy/ITS2_rep_set_tax_assignments.txt | iconv -f utf8 -t ascii//TRANSLIT//IGNORE > ITS2_rep_set_tax_assignments.noacc.txt
use Bio::SeqIO;
use Getopt::Long;
use File::Basename;
use File::Copy;
use File::Spec;
use FindBin;
use Cwd;

#modules
use lib "$FindBin::Bin"; #Path to the modules
use LogFile;
use sub_1;

#variables for options
my $dir;
my $outdir = getcwd();

my $help = 0;
#my $extract = 0;  #default is no extracting
my $singletons = "TRUE";  #default is to process all the singletons
my $closed_ref = "FALSE";  #default is de novo OTU picking
my $join = "TRUE";    
my $pairedend = "TRUE";  #for non-paired ended data
my $ITS = "FALSE";
my $trim_m = "usearch";  #default trim method
my $otu_m = "uclust";  #default clustering method in qiime
my $tax_m = "rdp";  #default assign taxonomy method
my $kingdoms = "F";

my $e_value = 10;
my $threads = 24;
my $min_overlap = 50;
my $perc_max_diff = 1;
my $minlen = 100;
my $qthreshold = 25;
my $qwindowsize = 10;
my $OTU_size = 1;
my $similarity = 0.97;
my $confidence = 0.70;
my $num_alignments = 50;
my $max_memory = 80000;  #in MB (used for both cd-hit and rdp)

my $usearch = "~/usearch7.0.1090_i86linux32"; #usearch version
my $blastdb = "/isilon/biodiversity/reference/blastdb/fungal_its/fno.fasta";
# id-to-taxonomy database formated to 
# mothur format: 1) ; at the end; 2) no space in lineage
# qiime mothur format: 1) NO ; at the end; 2) no space in lineage
# qiime rdp format: 1) NO ; at the end; 2) exactly 6-levels for each lineage
my $config_path;
my $primer_file;
my $oligo_file;
my $ref_seqs;
my $id_tax;
my $amplicon;
my $mothur="/isilon/biodiversity/users/chenw_lab/AAFC_Training_Material/Intro_Metagenomics/mothur";
my $scripts_dir="/isilon/biodiversity/users/chenw_lab/Bitbucket/scripts_repo/Bio_tools/";

## CONFIG & SETUP
&setup;

#options
sub setup{
  GetOptions ( 
  "-o=s" => \$outdir, 
  "-i=s" => \$dir, 
  "--closed!" => \$closed_ref, 
  "--reference=s" => \$ref_seqs, 
  "--taxonomy=s" => \$id_tax, 
  "--ITS=s" => \$ITS, 
  "--threads=i" => \$threads, 
  "--trim_method=s" => \$trim_m, 
  "--help!" => \$help, 
#  "--extract!" => \$extract, 
  "--min_overlap=i" => \$min_overlap, 
  "--perc_max_diff=i" => \$perc_max_diff, 
  "--minlen=i" => \$minlen, 
  "--qthreshold=i" => \$qthreshold, 
  "--qwindowsize=i" => \$qwindowsize, 
  "--OTU_size=i" => \$OTU_size, 
  "--similarity=s" => \$similarity, 
  "--singletons=s" => \$singletons, 
  "--join=s" => \$join, 
  "--otu_method=s" => \$otu_m, 
  "--tax_method=s" => \$tax_m,
  "--max_memory=i" => \$max_memory, 
  "--confidence=i" => \$confidence, 
  "--evalue" => \$e_value, 
  "--blastdb" => \$blastdb, 
  "--paired_end=s" => \$pairedend, 
  "-c=s" =>\$config_path, 
  "--num_alignments=s" =>\$num_alignments,
  "--amplicon=s" =>\$amplicon, 
  "--primer_file=s" =>\$primer_file,
  "--oligo_file=s" =>\$oligo_file) 
};
if(defined $config_path){
    config();
  #reads in config file
  #check for config file existence 
   unless(-e "$config_path") {
     die "The specified config file: $config_path does not exist.\n";
  }   
}

#help page of the pipeline
if ($help) {
  system ("perldoc /home/AAFC-AAC/zhuk/miseq_amplicon/scripts/MSAMP.pl");
  exit
}


#get full command line string
my $cmd_str= qx/ps -o args $$/;
$cmd_str=~s/COMMAND\n//g;
chomp $cmd_str;
LogFile::addToLog("Runtime","Command Line",$cmd_str);

my $DATE=POSIX::strftime('%Y_%B%d_%H_%M',localtime()); 
my $HR_DATE=POSIX::strftime('%B %d, %Y %H:%M',localtime());            
#gets the date into the format YEAR/MONTH/HOUR/MINUTE

LogFile::addToLog('Date','Date and time',$HR_DATE);

#my $output_directory = "$DATE.Output"; #create output directory based on time

my @runtime_array;
my $runtime_hash = {};
my $start_time = time();
my $st = time();
my $et;

#getting the absolute directory paths for input and output directory
if ( -d $dir ) {
  print"\nInput directory is $dir\n";
} else {
  die "\nInvilid input directory, $dir does not exist\n";
}
if ( -d $outdir) {
  print"Output directory is $outdir\n\n";
} else {
  die "Invilid output directory, $outdir does not exist\n\n";
}


if ( $amplicon eq "ITS" ) {
  $ref_seqs = "/isilon/biodiversity/users/chenw_lab/AAFC_Training_Material/Intro_Metagenomics/UNITE/UNITEv6_sh_97_s.fasta";
  $id_tax = "/isilon/biodiversity/users/chenw_lab/AAFC_Training_Material/Intro_Metagenomics/UNITE/UNITEv6_sh_97_s_rdp.ascii.txt";
} elsif ( $amplicon eq "16S" ) {
  $ref_seqs = "/isilon/biodiversity/users/chenw_lab/Reference/gg_13_5_otus/rep_set/97_otus.fasta";
  $id_tax = "/isilon/biodiversity/users/chenw_lab/Reference/gg_13_5_otus_FixTaxonomy/97_otu_taxonomy.fix.ascii.tax";
}

# check format of $primer_file
#my $in_pr = Bio::SeqIO->new(-file=>"$primer_file", format=>"fasta");
#my @pr_ids;
#while (my $seq_pr = $in_pr->next_seq()) {
#  my $id = $seq_pr->id;
#  push (@pr_ids, $id);
#}
#my @pr_id_check=("forward", "reverse");
#@pr_ids = sort { $a cmp $b } @pr_ids;
#@pr_id_check = sort {$a cmp $b } @pr_id_check;
#unless ( @pr_id_check ~~ @pr_ids ) {
#  print "the primer file should be in fasta format, with seqid for forward primer sequence being 'forward' and seqid for reverse primer being 'reverse'! Please correct then try again!\n\n";

#  die;
#}

# check format of $oligo_file
unless ( -e $oligo_file) {
  die "please provide primer information in mothur oligo file format, and then re-try: $!";
}
open (OL, "<$oligo_file");

my $primer_nameF;
my $primer_nameR;
while (<OL>) {
  chomp $_;
  my ($oli, $sequence, $primer) = split /\t/, $_;
  my ($reg, $direction, $primer_name) = split /:/, $primer;
  if ($direction eq "forward") {
    $primer_nameF = $primer_name;
    print $primer_nameF, "\t";
  }
  if ($direction eq "reverse") {
    $primer_nameR = $primer_name;
    print $primer_nameR, "\n";
  } 
}
#my @pr_id_check=("forward", "reverse");
#@pr_ids = sort { $a cmp $b } @pr_ids;
#@pr_id_check = sort {$a cmp $b } @pr_id_check;
#unless ( @pr_id_check ~~ @pr_ids ) {
#  print "the primer file should be in fasta format, with seqid for forward primer sequence being 'forward' and seqid for reverse primer being 'reverse'! Please correct then try again!\n\n";

#  die;
#}

LogFile::addToLog("IO","Input Folder",$dir);
LogFile::addToLog("IO","Output Folder",$outdir);

print "Start processing...", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";
#mkdir "temp";

###############################################################################
# Step1: decompress fastq.gz files or 
# copy already decompressed fastq files to $outdir
###############################################################################
chdir $outdir;
mkdir "temp";
mkdir "fast_qc";
mkdir "raw_data"; #new directory to store the raw data
mkdir "forward";
mkdir "reverse";
mkdir "forward/temp";
mkdir "reverse/temp";
mkdir "forward/fast_qc";
mkdir "reverse/fast_qc";
mkdir "both";
mkdir "both/temp";



# make fastq files
opendir(DIR, "$dir");
my @files = readdir(DIR);
closedir(DIR);
chdir $dir;
foreach my $file (@files)
{   
  if ($file =~ /\.fna$/) {
    my $file_name = basename($file, ".fna");
    print $file, "\n";
    print $file_name, "\n";
    system("convert_fastaqual_fastq.py -f $file_name.fna -q $file_name.qual -o fastq_files/")
  } else {
    next;
  }
}

#system ("fastqc fastq_files/*fastq -o $outdir/fast_qc/");
#system ("rm $outdir/fast_qc/*.zip");
#removing any potential characters in the 
#file names that could cause a problem downstream
#sub_1::name_correction_1 ();

##### split foward and reverse sequences
chdir $outdir;
opendir(DIR, "$dir");
my @raw_fna = grep(/\.fna$/, readdir(DIR));
closedir(DIR);
#print join(",", @files), "\n";
foreach my $fna (@raw_fna) {
#  my $fna_base=sub_1::blast_primer_ID($primer_file, $fna, $threads);
  #print $fna;
  $fna="$dir/$fna";
  my $fna_base = basename($fna, ".fna");
  
  open (MOTHUR, ">mothur.bat");
  print MOTHUR "trim.seqs(fasta=$fna, oligos=$oligo_file, pdiffs=2)\n";
  close MOTHUR;
  system("$mothur/mothur mothur.bat");
  system("grep $primer_nameF $dir/$fna_base.groups | cut -f1 > $fna_base.$primer_nameF.id");
  system("grep $primer_nameR $dir/$fna_base.groups | cut -f1 > $fna_base.$primer_nameR.id");
  my $count_all=`grep '>' $fna |wc -l`;
  chomp $count_all;
  my $count_scrap=`grep '>' $dir/$fna_base.scrap.fasta | wc -l`;
  chomp $count_scrap;
  my $count_F;
  my $count_R;
  unless ( -z "$fna_base.$primer_nameF.id" ) {
    $count_F= `wc -l $fna_base.$primer_nameF.id`;
    chomp $count_F;
    system("~/seqtk/seqtk subseq $dir/fastq_files/$fna_base.fastq $fna_base.$primer_nameF.id > $outdir/forward/$fna_base.$primer_nameF.fastq");
    print "moved $fna_base.$primer_nameF.fastq to $outdir/forward, \n";
  } else {
    $count_F=0;
  }
  unless ( -z "$fna_base.$primer_nameR.id" ) {
    $count_R=`wc -l $fna_base.$primer_nameR.id`;
    chomp $count_R;
    system("~/seqtk/seqtk subseq $dir/fastq_files/$fna_base.fastq $fna_base.$primer_nameR.id > $outdir/reverse/$fna_base.$primer_nameR.fastq");
    print "moved $fna_base.$primer_nameR.fastq to $outdir/reverse/, \n";
  } else {
    $count_R=0;
  }
  print "$count_all $fna_base.fastq;\n$count_F;\n$count_R;\n$count_scrap were removed!", "\n\n";
} 
system("mv *.id temp");
#system("mv *blast temp | mv *id temp");

#####################################################
# Step2: fastqc raw data and move to raw_data folder
#####################################################

chdir $outdir;

# make pick_open_parametre file
system( `echo "pick_open_reference_otus:similarity $similarity" > params_pick_open.txt`);
system (`echo "pick_otus:similarity 0.97" >> params_pick_open.txt`);
#system( `pick_open_reference_otus:prefilter_percent_id 0.97" >> params_pick_open.txt`);
system( `assign_taxonomy:id_to_taxonomy_fp $id_tax" >> params_pick_open.txt`);
system( `assign_taxonomy:reference_seqs_fp $ref_seqs" >> params_pick_open.txt`);
system( `assign_taxonomy:assignment_method rdp" >> params_pick_open.txt`);
system( `assign_taxonomy:rdp_max_memory 80000" >> params_pick_open.txt`);
system( `assign_taxonomy:confidence 0.8" >> params_pick_open.txt`);
my $params_open="$outdir/params_pick_open.txt";


#######################
## forward sequences
#######################
print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nProcess forward sequences...\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n";

chdir "$outdir/forward";

## check if non-zero sized fastq files exist
my $dirF = "$outdir/forward";  # current dir
opendir(DIRHANDLE, $dirF) || die "Couldn't open directory $dirF: $!\n";
my @fastqF = grep {  /\.fastq$/ && -f "$dirF/$_" } readdir DIRHANDLE;
closedir DIRHANDLE;

if (scalar @fastqF != 0 ) {
  # non-zero sized fastq file exist
  system("mv *fastq temp");

  chdir "$outdir/forward/temp";
  opendir(DIR, "$outdir/forward/temp");
  my @files = grep(/\.fastq$/,readdir(DIR));
  closedir(DIR);
  print join(",", @files), "\n";

  ### Trimming
  print "Start trimming and converting fastq to qiime-formated fasta: ", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";
 
  # trim sequences: joined and unjoined(un1, un2);
  foreach my $file (@files) {
    my $basename = basename($file, ".fastq");
    print "\n\nStart trimming fastq files: $file, \n";
    open (MOTHUR, ">mothur.bat");
    print MOTHUR "fastq.info(fastq=$file)\n"; # split fasta, qual files
    print MOTHUR "trim.seqs(fasta=$basename.fasta, qfile=$basename.qual, maxambig=0, qwindowsize=$qwindowsize, qwindowaverage=$qthreshold, processors=$threads, minlength=$minlen, maxhomop=10)\n";
    print MOTHUR "make.fastq(fasta=$basename.trim.fasta, qfile=$basename.trim.qual)\n";
    close MOTHUR;
    system ("$mothur/mothur mothur.bat");
    print "trimmed all forward fastq sequences! \n\n";
    system ("perl /isilon/biodiversity/users/chenw_lab/AAFC_Training_Material/Intro_Metagenomics/scripts/qiime_format_seqid.pl $dirF/temp/$basename.trim.fasta fasta");
  }
  system (`cat $outdir/forward/temp/*qiime.fasta > $outdir/forward/all.qiime.fasta`);
  chdir "$outdir/forward";
  #system (`time pick_open_reference_otus.py -i all.qiime.fasta -p $params_open -o pick_open_otu -r $ref_seqs --suppress_align_and_tree --percent_subsample 1 -f`);
}
chdir "$outdir";
 
#######################
## reverse sequences
#######################
print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nProcess reverse sequences...\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n";

chdir "$outdir/reverse";

## check if non-zero sized fastq files exist
my $dirR = "$outdir/reverse";  # current dir
opendir(DIRHANDLE, $dirR) || die "Couldn't open directory $dirR: $!\n";
my @fastqR = grep {  /\.fastq$/ && -f "$dirR/$_" } readdir DIRHANDLE;
closedir DIRHANDLE;

if (scalar @fastqR != 0 ) {
  # non-zero sized fastq file exist
  system("mv *fastq temp");

  chdir "$outdir/reverse/temp";
  opendir(DIR, "$outdir/reverse/temp");
  my @files = grep(/\.fastq$/,readdir(DIR));
  closedir(DIR);
  print join(",", @files), "\n";

  ### Trimming
  print "Start trimming and converting fastq to qiime-formated fasta: ", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";
 
  # trim sequences: joined and unjoined(un1, un2);
  foreach my $file (@files) {
    my $basename = basename($file, ".fastq");
    print "\n\nStart trimming fastq files: $file, \n";
    open (MOTHUR, ">mothur.bat");
    print MOTHUR "fastq.info(fastq=$file)\n"; # split fasta, qual files
    print MOTHUR "trim.seqs(fasta=$basename.fasta, qfile=$basename.qual, maxambig=0, qwindowsize=$qwindowsize, qwindowaverage=$qthreshold, processors=$threads, minlength=$minlen, maxhomop=10)\n";
    print MOTHUR "make.fastq(fasta=$basename.trim.fasta, qfile=$basename.trim.qual)\n";
    close MOTHUR;
    system ("$mothur/mothur mothur.bat");
    print "trimmed all reverse fastq sequences! \n\n";
    system ("perl /isilon/biodiversity/users/chenw_lab/AAFC_Training_Material/Intro_Metagenomics/scripts/qiime_format_seqid.pl $dirR/temp/$basename.trim.fasta fasta");
    reverse_complement("$dirR/temp/$basename.trim.qiime.fasta")
  }
  system (`cat $outdir/reverse/temp/*qiime.rc.fasta > $outdir/reverse/all.qiime.rc.fasta`);
  chdir "$outdir/reverse";
  #system (`time pick_open_reference_otus.py -i all.qiime.rc.fasta -p $params_open -o pick_open_otu -r $ref_seqs --suppress_align_and_tree --percent_subsample 1 -f`);
}

chdir "$outdir";

#####################################
## process both forward and 
## reverse.rc trimmed sequences
## pick_closed_otus
######################################
print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nProcess combined forward and reverse sequences...\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n";

my $params_close_temp="$scripts_dir/params_pick_closed.txt";

chdir "$outdir/both";

# make pick_close_parametre file
system( `echo "pick_closed_reference_otus:similarity $similarity" > params_pick_closed.txt`);
#system( `echo "pick_otus:similarity $similarity" >> params_pick_closed.txt`);
system( `echo "assign_taxonomy:id_to_taxonomy_fp $id_tax" >> params_pick_closed.txt`);
system( `echo "assign_taxonomy:reference_seqs_fp $ref_seqs" >> params_pick_closed.txt`);
system( `echo "assign_taxonomy:assignment_method rdp" >> params_pick_closed.txt`);
system( `echo "assign_taxonomy:rdp_max_memory 80000" >> params_pick_closed.txt`);
system( `echo "assign_taxonomy:confidence 0.8" >> params_pick_closed.txt`);
my $params_close="$outdir/both/params_pick_closed.txt";

## check if non-zero sized fastq files exist
my $dirB = "$outdir/both";  # current dir
system ("cat $outdir/forward/all.qiime.fasta $outdir/reverse/all.qiime.rc.fasta > $outdir/both/all.qiime.fasta");

if ($amplicon eq "16S"){
  # 16S 
  system( "pick_closed_reference_otus.py -i $outdir/both/all.qiime.fasta -r $ref_seqs -t $id_tax -o  $outdir/both/pick_closed_otu_16S -p $params_close -f ");
} elsif ($amplicon eq "ITS") {
  # ITS sequences
  system ("ITSx -i $outdir/both/all.qiime.fasta --preserve T -o $outdir/both/all.qiime --cpu $threads --multi_thread T");
  system( "pick_closed_reference_otus.py -i $outdir/both/all.qiime.ITS1.fasta -r $ref_seqs -t $id_tax -o  $outdir/both/pick_closed_otu_ITS1 -p $params_close -f");
  system( "pick_closed_reference_otus.py -i $outdir/both/all.qiime.ITS2.fasta -r $ref_seqs -t $id_tax -o  $outdir/both/pick_closed_otu_ITS2 -p $params_close -f");
} else {
  print "amplicon type is missing, 16S or ITS?\n";
  exit;
}

#####################################
## process both forward and 
## reverse.rc trimmed sequences
## failed in pick_closed_otus
######################################
chdir "$outdir/both";

my $ref_chimera;
if ($amplicon eq "16S"){
  # 16S 
  $ref_chimera="/isilon/biodiversity/users/chenw_lab/Reference/rdp_gold.fa";
  # extract failed seqs for forward and reverse;
  my @direction=($primer_nameF, $primer_nameR);
  foreach my $primer (@direction) {
    system (` grep $primer pick_closed_otu_16S/uclust_ref_picked_otus/all.qiime_failures.txt > all.qiime_failures.$primer.id `);
    system( `perl $scripts_dir/extract_seq.pl all.qiime_failures.$primer.id  all.qiime.fasta > all.qiime_failures.$primer.fasta`);
    # pick denovo OTUs first step
    system (`time pick_otus.py -i all.qiime_failures.$primer.fasta -m cdhit -s 0.97 -o cdhit_picked_failures.$primer\_otus/ --max_cdhit_memory 80000`); # check time used for this step, --minsize only work for usearch method;

   # pick representative sequences;
    system( `time pick_rep_set.py -i cdhit_picked_failures.$primer\_otus/all.qiime_failures.$primer\_otus.txt -f all.qiime_failures.$primer.fasta -o all.qiime_failures.$primer.rep_set.fna`);
    #Chimera checking (can be slow, break) using mothur;
    system (`echo "chimera.uchime(fasta=all.qiime_failures.$primer.rep_set.fna, reference=$ref_chimera, processors=$threads)" > mothur.bat`);
    system(`time $mothur/mothur mothur.bat`);
    # rep_set.ref.uchime.accnos: otus with representative sequences being identified as chimera
    # extract sequences belonging to otus with representative sequences being identified as chimera

    if ( -z "all.qiime_failures.$primer.rep_set.ref.uchime.accnos" ) {
      system ( `cp all.qiime_failures.$primer.fasta > all.qiime_failures.$primer.pick.fasta`);
    } else {
      my $accnos="all.qiime_failures.$primer.rep_set.ref.uchime.accnos";
      my $names="cdhit_picked_failures.$primer\_otus/all.qiime_failures.$primer\_otus.txt";
      chimera_seqid($accnos, $names);
      system ( `perl $scripts_dir/extract_seq.pl all.qiime_failures.$primer.rep_set.ref.uchime.accnos.seqid all.qiime_failures.$primer.fasta > all.qiime_failures.$primer.rep_set.ref.uchime.accnos.fasta`);
      # check chimera again for each sequence 
      # in the OTUs with chimeratic representative sequence
      system ( `echo "chimera.uchime(fasta=all.qiime_failures.$primer.rep_set.ref.uchime.accnos.fasta, reference=$ref_chimera, processors=2)" > mothur.bat`);
      system ( `time $mothur/mothur mothur.bat`);
      # remove all chimers sequences
      system ( `echo "remove.seqs(fasta=all.qiime_failures.$primer.fasta, accnos=all.qiime_failures.$primer.rep_set.ref.uchime.accnos.ref.uchime.accnos)" > mothur.bat`);
      system ( `time $mothur/mothur mothur.bat`);
    }  
    # pick de novo OTU again
    system (`grep ">" all.qiime_failures.$primer.pick.fasta |wc -l`); # number of sequences in the input fasta file
    system (`time pick_otus.py -i all.qiime_failures.$primer.pick.fasta -m cdhit -s 0.97 -o cdhit_picked_failures.$primer\_nonchimera_otus/ --minsize 2 --max_cdhit_memory 80000`); # check time used for this step;

    # pick representative sequences
    system (`time pick_rep_set.py -i cdhit_picked_failures.$primer\_nonchimera_otus/all.qiime_failures.$primer.pick_otus.txt -f all.qiime_failures.$primer.pick.fasta -o all.qiime_failures.$primer.pick.rep_set.fna`);
    # assign taxonomy to representative sequences (slow)
    # taxonomy file for rdp classifier: 1) all lineages have the same number of ranks; 2) remove semi colons ';' at the end of lines in the $ref_tax_mothur, 
    system (`time assign_taxonomy.py -i all.qiime_failures.$primer.pick.rep_set.fna -r $ref_seqs -t $id_tax -m rdp -c 0.8 --rdp_max_memory 80000`);
  
    # make otu table, 
    system( "time pick_closed_reference_otus.py -i all.qiime_failures.$primer.pick.fasta -r all.qiime_failures.$primer.pick.rep_set.fna -t rdp_assigned_taxonomy/all.qiime_failures.$primer.pick.rep_set_tax_assignments.txt -o pick_closed_otu_16S_failed -p $params_close -f ");
    system(`rm *_formatted`);
  }
} elsif ( $amplicon eq "ITS"){
  # ITS
  my @its_all=("ITS1","ITS2");
  foreach my $its (@its_all) {
    $ref_chimera="/isilon/biodiversity/users/chenw_lab/AAFC_Training_Material/Intro_Metagenomics//UNITE/uchime_reference_dataset_01.01.2016/ITS1_ITS2_datasets/uchime_sh_refs_dynamic_develop_985_01.01.2016.$its.fasta";
    # extract failed seqs;
    system( "perl $scripts_dir/extract_seq.pl pick_closed_otu_$its/uclust_ref_picked_otus/all.qiime.$its\_failures.txt all.qiime.$its.fasta > all.qiime.$its.open.fasta");
    system ("time pick_otus.py -i all.qiime.$its.open.fasta -m cdhit -s 0.97 -o cdhit_picked_open_otus\_$its/ --max_cdhit_memory 80000 "); # check time used for this step, --minsize only work for usearch method;

    # pick representative sequences;
    system( "time pick_rep_set.py -i cdhit_picked_open_otus\_$its/all.qiime.$its.open_otus.txt -f all.qiime.$its.open.fasta -o all.qiime.$its.open.rep_set.fna");
    #Chimera checking (can be slow, break) using mothur;
    system (`echo "chimera.uchime(fasta=all.qiime.$its.open.rep_set.fna, reference=$ref_chimera, processors=$threads)" > mothur.bat`);
    system(`time $mothur/mothur mothur.bat`);
    # rep_set.ref.uchime.accnos: otus with representative sequences being identified as chimera
    # extract sequences belonging to otus with representative sequences being identified as chimera

    if ( -z "all.qiime.$its.open.rep_set.ref.uchime.accnos" ) {
      system (` cp all.qiime.$its.open.fasta > all.qiime.$its.open.pick.fasta`);
    } else {
      my $accnos="all.qiime.$its.open.rep_set.ref.uchime.accnos";
      my $names="cdhit_picked_open_otus\_$its/all.qiime.$its.open_otus.txt";
      chimera_seqid($accnos, $names);
      system (`perl $scripts_dir/extract_seq.pl all.qiime.$its.open.rep_set.ref.uchime.accnos.seqid all.qiime.$its.open.fasta > all.qiime.$its.open.rep_set.ref.uchime.accnos.fasta`);
      # check chimera again for each sequence 
      # in the OTUs with chimeratic representative sequence
      system (`echo "chimera.uchime(fasta=all.qiime.$its.open.rep_set.ref.uchime.accnos.fasta, reference=$ref_chimera, processors=2)" > mothur.bat`);
      system (`time $mothur/mothur mothur.bat`);
      # remove all chimers sequences
      system (`echo "remove.seqs(fasta=all.qiime.$its.open.fasta, accnos=all.qiime.$its.open.rep_set.ref.uchime.accnos.ref.uchime.accnos)" > mothur.bat`);
      system (`time $mothur/mothur mothur.bat`);
    }  
    # pick de novo OTU again
    system (`grep ">" all.qiime.$its.open.pick.fasta |wc -l`); # number of sequences in the input fasta file
    system (`time pick_otus.py -i all.qiime.$its.open.pick.fasta -m cdhit -s 0.97 -o cdhit_picked_open_nonchimera_otus\_$its/ --minsize 2 --max_cdhit_memory 80000`); # check time used for this step;

    # pick representative sequences
    system (`time pick_rep_set.py -i cdhit_picked_open_nonchimera_otus\_$its/all.qiime.$its.open.pick_otus.txt -f all.qiime.$its.open.pick.fasta -o all.qiime.$its.open.pick.rep_set.fna`);
    # assign taxonomy to representative sequences (slow)
    # taxonomy file for rdp classifier: 1) all lineages have the same number of ranks; 2) remove semi colons ';' at the end of lines in the $ref_tax_mothur, 
    system (`time assign_taxonomy.py -i all.qiime.$its.open.pick.rep_set.fna -r $ref_seqs -t $id_tax -m rdp -c 0.8 --rdp_max_memory 80000`);
  
    # make otu table, 
    system( "time pick_closed_reference_otus.py -i all.qiime.$its.open.pick.fasta -r all.qiime.$its.open.pick.rep_set.fna -t rdp_assigned_taxonomy/all.qiime.$its.open.pick.rep_set_tax_assignments.txt -o pick_closed_otu_$its\_failed -p $params_close -f ");
    system(`rm *_formatted`);
  }
} else {
  print "amplicon type, ITS or 16S?\n";
  exit;
}


my $end_time = time();
my $run_time = $end_time - $start_time;
print "Finished running. Took a total of $run_time seconds.\n";

push (@runtime_array, {'Total'=> "$run_time seconds"});
push (@runtime_array, $runtime_hash);
LogFile::addToLog("Runtime","Running time",\@runtime_array);  
LogFile::writeLog("$DATE.yaml");


chdir $outdir;

#system (`time pick_open_reference_otus.py -i all.qiime.fasta -p params_pick_open.txt -o pick_open_otu -r $ref_seqs --suppress_align_and_tree --percent_subsample 1 -f`);


###################################################
# Process finished
###################################################

sub reverse_complement {
  my $file=shift;
  my $base=basename($file, ".fasta");
  print "Making reverse complement of $file\t". POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";  
  my $in = Bio::SeqIO->new(-file=>"$file",-format=>'fasta');
  open (OUT, ">$base.rc.fasta");
  while (my $seq=$in->next_seq()){
    my $seq_rev = $seq->revcom;
    print OUT ">", $seq_rev->id, "\t", $seq_rev->desc, "\n", $seq_rev->seq, "\n";
  }  
}

sub chimera_seqid {
   my $accnos=shift;
   my $maps=shift;

   open (ACCNOS, "<$accnos");
    my @otuIDs;
    my $count_ch=0;
    while (<ACCNOS>) {
      chomp $_;
      push (@otuIDs, $_);
      $count_ch++;
    }
    close ACCNOS;
    print "\nRepresentative sequences of $count_ch OTUs were identified as chimera\n";


    my $names={};
    open (NAMES, "<$maps");
    my $count_otuall=0;
    my $sum_seq=0;
    while (<NAMES>) {
      chomp $_;
      $count_otuall++;
      my @cols=split/\t/, $_;
      my $num=scalar @cols;
      $sum_seq += $num;
      #print $cols[0], "\t", $cols[1], "\n";
      $names->{$cols[0]}=[@cols[1..(scalar @cols -1)]]
    }
    close NAMES;

    open (OUT, ">$accnos.seqid");  
    my $count_otu=0;
    my $sum_seq_ch=0;
    foreach my $otuid (@otuIDs) {
      if (exists $names->{$otuid}) {
        my @value=@{$names->{$otuid}};
        $count_otu++;
        my $num=scalar @value;
        $sum_seq_ch += $num;
        print OUT join("\n", @value), "\n";
      }
    }    
    close OUT;
    print "Found ", $sum_seq_ch, " sequences in ", $count_otu, " otus that are chimera\n\n";
}


#sub timer {
#  my $process = shift;
#  $et1 = time();
#  $runtime_hash->{$process}=join(" ", $et1-$st1, "seconds");
#  $st1 = $et1;
#}

sub config{
  #this sub reads from the configuration file and checks if everything is defined.
  open (CONF,"$config_path") or die;
  while (<CONF>){
    my $line = $_;
    chomp $line;
    $line =~ s/\R//g;
    my @split;
    if ($line =~ m/^THREAD_COUNT/){
      @split = split(/=/,$line);
      $threads = $split[1];    
    }elsif($line =~ m/^MAX_MEMORY/){
      @split = split(/=/,$line);
      $max_memory = $split[1];
    }elsif($line =~ m/^TRIM_METHOD/){
      @split = split(/=/,$line);
      $trim_m = $split[1];
    }elsif($line =~m/^MINIMUM_LENGTH/){
      @split = split(/=/,$line);
      $minlen = $split[1];
    }elsif($line =~m/^QUALITY_THRESHOLD/){
      @split = split(/=/,$line);
      $qthreshold = $split[1];
    }elsif($line =~m/^WINDOW_SIZE/){
      @split = split(/=/,$line);
      $qwindowsize = $split[1];
    }elsif($line =~m/^JOIN/){
      @split = split(/=/,$line);
      $join = $split[1];
    }elsif($line =~m/^MINIMUM_OVERLAP/){
      @split = split(/=/,$line);
      $min_overlap = $split[1];
    }elsif($line =~m/^MAXIMUM_PERCENTAGE_DIFFERENCE/){
      @split = split(/=/,$line);
      $perc_max_diff = $split[1];
    }elsif($line =~m/^SINGLETONS/){
      @split = split(/=/,$line);
      $singletons = $split[1];
    }elsif($line =~m/^ITS_EXTRACTION/){
      @split = split(/=/,$line);
      $ITS = $split[1];
    }elsif($line =~m/^OTU_METHOD/){
      @split = split(/=/,$line);
      $otu_m = $split[1];
    }elsif($line =~m/^SIMILARITY/){
      @split = split(/=/,$line);
      $similarity = $split[1];
    }elsif($line =~m/^OTU_SIZE/){
      @split = split(/=/,$line);
      $OTU_size = $split[1];
    }elsif($line =~m/^TAX_METHOD/){
      @split = split(/=/,$line);
      $tax_m = $split[1];
    }elsif($line =~m/^CONFIDENCE/){
      @split = split(/=/,$line);
      $confidence = $split[1];
    }elsif($line =~m/^EVALUE/){
      @split = split(/=/,$line);
      $e_value = $split[1];
    }elsif($line =~m/^NUM_ALIGNMENTS/){
      @split = split(/=/,$line);
      $num_alignments = $split[1];
    }elsif($line =~m/^CLOSED_REFERENCE/){
      @split = split(/=/,$line);
      $closed_ref = $split[1];
    }elsif($line =~m/^PAIRED_END/){
      @split = split(/=/,$line);
      $pairedend = $split[1];
    }elsif($line =~m/^KINGDOMS/){
      @split = split(/=/,$line);
      $kingdoms = $split[1];
    }
    LogFile::addToLog("Options","Configuration",{'Path' => $config_path, 'Threads' => $threads});

  }
  
  #check if all the values are defined

  close CONF;
}


__END__

=head1 NAME

=over 12

=item B<M>iB<S>eq B<A>mplicon B<M>etagenomics B<P>ipeline (MSAMP)

=back

=head1 SYNOPSIS

perl MSAMP.pl [-i] [Options] [--help]

=head1 DESCRIPTION

The MiSeq Amplicon Metagenomics Pipeline process sequences generated by MiSeq sequencing machines using data gathered from various biodiversity surveys. 

To run, you must give a path to the input raw data folder. If the raw data files are not zipped (FASTQ files), they will be copied to output folder before running the pipeline. Output folder will be your current working directory if you do not provide one.

=head1 OUTPUTS

Stuff...

=head1 OPTIONS

=over 12

=item B<--help>

Program information and usage instructions. Detailed information and instructions are in the manual

=item B<-i>

Specify the path to the directory of the input data files [REQUIRED]

=item B<-o>

Specify the path of the output directory [Default: current working directory]

=item B<--closed>

Closed reference otu picking [Default: FALSE]

=item B<--reference>

Specify the path to the reference sequences for assigning taxonomy and/or closed reference otu picking

=item B<--taxonomy>

Specify the path to tab-delimited file mapping sequences to assigned taxonomy and/or closed reference otu picking

=item B<--ITS>

Specify which region of ITS you want to extract, must be one of ITS1, ITS2 [Default: none]

=item B<--threads> [N]

Specify how many processors/threads you want to use to run the multi-threaded portions of this pipeline. N processors will be used [Default: 1]

=item B<--trim_method>

Sequence trimming method, must be one of usearch, mothur [Default: usearch]

=item B<--extract>

For if the raw data file(s) need to be decompressed [Default: FALSE]

=item B<--min_overlap> [N]

N-minimum overlap required when joining forward and reverse sequence [Default: 50]

=item B<--perc_max_diff> [N]

N-percent B<MAXIMUM> difference for the overlapping region when joining [Default: 1]E<10> E<8>(i.e. 1% maximum difference = 99% minimum similarity)

=item B<--minlen> [N]

Specify a minimum sequence length to keep. Lengths of sequences will be at least N bp long after sorting [Default: 100]

=item B<--qthreshold> [N]

B<mothur>: Set a minimum average quality score allowed over a window [Default: 20]E<10> E<8>B<usearch>: Truncate the sequence at the first position having quality score <= N [Default: 20]

=item B<--qwindowsize> [N]

Set a minimum average quality score allowed over a window, only used for mothur [Default: 10]

=item B<--OTU_size>

Minimum cluster size for size filtering with usearch [Default: 1]

=item B<--similarity> [N]

Sequence similarity threshold, only used for usearch, cd-hit [Default: 0.97]

=item B<--nosingletons>

Does not process unjoined forward/reverse reads [Default: FALSE]

=item B<--otu_method>

Clustering method, must be one of usearch, cdhit [Default: usearch]

=item B<--tax_method>

Taxonomy assignment method, must be one of mothur, rdp, blast [Default: mothur]

=item B<--max_memory> [N]

Maximum memory allocation, in MB, only used for cdhit, rdp [Default: 12000]

=item B<--confidence>

Minimum confidence to record an taxonomy assignment, only used for rdp and mothur methods [Default: 0.5]

=item B<--evalue> [N]

Expectation value threshold for saving hits

=item B<--blastdb>

Specify the path to the blast database when it is needed


