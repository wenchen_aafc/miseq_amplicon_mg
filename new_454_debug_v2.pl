#!/usr/bin/env perl

use strict;
use warnings;
#cat rdp_assigned_taxonomy/ITS2_rep_set_tax_assignments.txt | iconv -f utf8 -t ascii//TRANSLIT//IGNORE > ITS2_rep_set_tax_assignments.noacc.txt
use Bio::SeqIO;
use Getopt::Long;
use File::Basename;
use File::Copy;
use File::Spec;
use FindBin;
use Cwd;

#modules
use lib "$FindBin::Bin"; #Path to the modules
use LogFile;
use sub_1;

#variables for options
my $dir;
my $outdir = getcwd();

my $help = 0;
#my $extract = 0;  #default is no extracting
my $singletons = "TRUE";  #default is to process all the singletons
my $closed_ref = "FALSE";  #default is de novo OTU picking
my $join = "TRUE";    
my $pairedend = "TRUE";  #for non-paired ended data
my $ITS = "FALSE";
my $trim_m = "usearch";  #default trim method
my $otu_m = "uclust";  #default clustering method in qiime
my $tax_m = "rdp";  #default assign taxonomy method
my $kingdoms = "F";

my $e_value = 10;
my $threads = 24;
my $min_overlap = 50;
my $perc_max_diff = 1;
my $minlen = 100;
my $qthreshold = 25;
my $qwindowsize = 10;
my $OTU_size = 1;
my $similarity = 0.97;
my $confidence = 0.70;
my $num_alignments = 50;
my $max_memory = 12000;  #in MB (used for both cd-hit and rdp)

my $usearch = "~/usearch7.0.1090_i86linux32"; #usearch version
my $blastdb = "/isilon/biodiversity/reference/blastdb/fungal_its/fno.fasta";
# id-to-taxonomy database formated to 
# mothur format: 1) ; at the end; 2) no space in lineage
# qiime mothur format: 1) NO ; at the end; 2) no space in lineage
# qiime rdp format: 1) NO ; at the end; 2) exactly 6-levels for each lineage
my $config_path;
my $primer_file;
my $oligo_file;
my $ref_seqs;
my $id_tax;
my $amplicon;
my $mothur="/isilon/biodiversity/users/chenw_lab/AAFC_Training_Material/Intro_Metagenomics/mothur";

## CONFIG & SETUP
&setup;

#options
sub setup{
  GetOptions ( 
  "-o=s" => \$outdir, 
  "-i=s" => \$dir, 
  "--closed!" => \$closed_ref, 
  "--reference=s" => \$ref_seqs, 
  "--taxonomy=s" => \$id_tax, 
  "--ITS=s" => \$ITS, 
  "--threads=i" => \$threads, 
  "--trim_method=s" => \$trim_m, 
  "--help!" => \$help, 
#  "--extract!" => \$extract, 
  "--min_overlap=i" => \$min_overlap, 
  "--perc_max_diff=i" => \$perc_max_diff, 
  "--minlen=i" => \$minlen, 
  "--qthreshold=i" => \$qthreshold, 
  "--qwindowsize=i" => \$qwindowsize, 
  "--OTU_size=i" => \$OTU_size, 
  "--similarity=s" => \$similarity, 
  "--singletons=s" => \$singletons, 
  "--join=s" => \$join, 
  "--otu_method=s" => \$otu_m, 
  "--tax_method=s" => \$tax_m,
  "--max_memory=i" => \$max_memory, 
  "--confidence=i" => \$confidence, 
  "--evalue" => \$e_value, 
  "--blastdb" => \$blastdb, 
  "--paired_end=s" => \$pairedend, 
  "-c=s" =>\$config_path, 
  "--num_alignments=s" =>\$num_alignments,
  "--amplicon=s" =>\$amplicon, 
  "--primer_file=s" =>\$primer_file,
  "--oligo_file=s" =>\$oligo_file) 
};
if(defined $config_path){
    config();
  #reads in config file
  #check for config file existence 
   unless(-e "$config_path") {
     die "The specified config file: $config_path does not exist.\n";
  }   
}

#help page of the pipeline
if ($help) {
  system ("perldoc /home/AAFC-AAC/zhuk/miseq_amplicon/scripts/MSAMP.pl");
  exit
}


#get full command line string
my $cmd_str= qx/ps -o args $$/;
$cmd_str=~s/COMMAND\n//g;
chomp $cmd_str;
LogFile::addToLog("Runtime","Command Line",$cmd_str);

my $DATE=POSIX::strftime('%Y_%B%d_%H_%M',localtime()); 
my $HR_DATE=POSIX::strftime('%B %d, %Y %H:%M',localtime());            
#gets the date into the format YEAR/MONTH/HOUR/MINUTE

LogFile::addToLog('Date','Date and time',$HR_DATE);

#my $output_directory = "$DATE.Output"; #create output directory based on time

my @runtime_array;
my $runtime_hash = {};
my $start_time = time();
my $st = time();
my $et;

#getting the absolute directory paths for input and output directory
if ( -d $dir ) {
  print"\nInput directory is $dir\n";
} else {
  die "\nInvilid input directory, $dir does not exist\n";
}
if ( -d $outdir) {
  print"Output directory is $outdir\n\n";
} else {
  die "Invilid output directory, $outdir does not exist\n\n";
}


if ( $amplicon eq "ITS" ) {
  $ref_seqs = "/isilon/biodiversity/users/chenw_lab/AAFC_Training_Material/Intro_Metagenomics/UNITE/UNITEv6_sh_97_s.fasta";
  $id_tax = "/isilon/biodiversity/users/chenw_lab/AAFC_Training_Material/Intro_Metagenomics/UNITE/UNITEv6_sh_97_s_rdp.ascii.txt";
} elsif ( $amplicon eq "16S" ) {
  $ref_seqs = "/isilon/biodiversity/users/chenw_lab/Reference/gg_13_5_otus/rep_set/97_otus.fasta";
  $id_tax = "/isilon/biodiversity/users/chenw_lab/Reference/gg_13_5_otus_FixTaxonomy/97_otu_taxonomy.fix.ascii.tax";
}

# check format of $primer_file
#my $in_pr = Bio::SeqIO->new(-file=>"$primer_file", format=>"fasta");
#my @pr_ids;
#while (my $seq_pr = $in_pr->next_seq()) {
#  my $id = $seq_pr->id;
#  push (@pr_ids, $id);
#}
#my @pr_id_check=("forward", "reverse");
#@pr_ids = sort { $a cmp $b } @pr_ids;
#@pr_id_check = sort {$a cmp $b } @pr_id_check;
#unless ( @pr_id_check ~~ @pr_ids ) {
#  print "the primer file should be in fasta format, with seqid for forward primer sequence being 'forward' and seqid for reverse primer being 'reverse'! Please correct then try again!\n\n";

#  die;
#}

# check format of $oligo_file
unless ( -e $oligo_file) {
  die "please provide primer information in mothur oligo file format, and then re-try: $!";
}
open (OL, "<$oligo_file");

my $primer_nameF;
my $primer_nameR;
while (<OL>) {
  chomp $_;
  my ($oli, $sequence, $primer) = split /\t/, $_;
  my ($direction, $primer_name) = split /:/, $primer;
  if ($direction eq "forward") {
    $primer_nameF = $primer_name;
  }
  if ($direction eq "reverse") {
    $primer_nameR = $primer_name;
  } 
}
#my @pr_id_check=("forward", "reverse");
#@pr_ids = sort { $a cmp $b } @pr_ids;
#@pr_id_check = sort {$a cmp $b } @pr_id_check;
#unless ( @pr_id_check ~~ @pr_ids ) {
#  print "the primer file should be in fasta format, with seqid for forward primer sequence being 'forward' and seqid for reverse primer being 'reverse'! Please correct then try again!\n\n";

#  die;
#}

LogFile::addToLog("IO","Input Folder",$dir);
LogFile::addToLog("IO","Output Folder",$outdir);

print "Start processing...", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";
#mkdir "temp";

###############################################################################
# Step1: decompress fastq.gz files or 
# copy already decompressed fastq files to $outdir
###############################################################################
chdir $outdir;
mkdir "temp";
mkdir "fast_qc";
mkdir "raw_data"; #new directory to store the raw data
mkdir "forward";
mkdir "reverse";
mkdir "forward/temp";
mkdir "reverse/temp";
mkdir "forward/fast_qc";
mkdir "reverse/fast_qc";
mkdir "both";
mkdir "both/temp";



# make fastq files
opendir(DIR, "$dir");
my @files = readdir(DIR);
closedir(DIR);
chdir $dir;
foreach my $file (@files)
{   
  if ($file =~ /\.fna$/) {
    my $file_name = basename($file, ".fna");
    print $file_name, "\n";
    system("convert_fastaqual_fastq.py -f $file_name.fna -q $file_name.qual -o fastq_files/")
  } else {
    next;
  }
}

system ("fastqc fastq_files/*fastq -o $outdir/fast_qc/");
system ("rm $outdir/fast_qc/*.zip");
#removing any potential characters in the 
#file names that could cause a problem downstream
#sub_1::name_correction_1 ();

##### split foward and reverse sequences
chdir $outdir;
opendir(DIR, "$dir");
my @raw_fna = grep(/\.fna$/, readdir(DIR));
closedir(DIR);
#print join(",", @files), "\n";
foreach my $fna (@raw_fna) {
#  my $fna_base=sub_1::blast_primer_ID($primer_file, $fna, $threads);
  my $fna_base = basename($fna, ".fna");
  
  open (MOTHUR, ">mothur.bat");
  print MOTHUR "trim.seqs(fasta=$fna, oligos=$oligo_file, pdiffs=2)\n";
  close MOTHUR;
  system("$mothur/mothur mothur.bat");
  system("grep $primer_nameF $fna_base.groups | cut -f1 > $fna_base.$primer_nameF.id");
  system("grep $primer_nameR $fna_base.groups | cut -f1 > $fna_base.$primer_nameR.id");
  my $count_all=`grep '>' $fna |wc -l`;
  chomp $count_all;
  my $count_F= `wc -l $fna_base.$primer_nameF.id`;
  chomp $count_F;
  my $count_R=`wc -l $fna_base.$primer_nameR.id`;
  chomp $count_R;
  my $count_scrap=`grep '>' $fna_base.scrap.fasta | wc -l`;
  chomp $count_scrap;
  unless ( -z "$fna_base.$primer_nameF.id" ) {
    system("~/seqtk/seqtk subseq $dir/fastq_files/$fna_base.fastq $fna_base.$primer_nameF.id > $outdir/forward/$fna_base.$primer_nameF.fastq");
    print "moved $fna_base.$primer_nameF.fastq to $outdir/forward, \n";
  } 
  unless ( -z "$fna_base.$primer_nameR.id" ) {
    system("~/seqtk/seqtk subseq $dir/fastq_files/$fna_base.fastq $fna_base.$primer_nameR.id > $outdir/reverse/$fna_base.$primer_nameR.fastq");
    print "moved $fna_base.$primer_nameR.fastq to $outdir/reverse/, \n";
  }
  print "$count_all $fna_base.fastq;\n$count_F;\n$count_R;\n$count_scrap were removed!", "\n\n";
} 
system("rm *.fna* | rm *.qual | rm *trim.fasta| mv mothur.* temp | mv *scrap* temp | mv *.groups temp | mv *id temp");
#system("mv *blast temp | mv *id temp");

chdir $outdir;

#####################################################
# Step2: fastqc raw data and move to raw_data folder
#####################################################

chdir $outdir;

#######################
## forward sequences
#######################
print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nProcess forward sequences...\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n";

chdir "$outdir/forward";

## check if non-zero sized fastq files exist
my $dirF = '.';  # current dir
opendir(DIRHANDLE, $dirF) || die "Couldn't open directory $dirF: $!\n";
my @fastqF = grep {  /\.fastq$/ && -f "$dirF/$_" } readdir DIRHANDLE;
closedir DIRHANDLE;

if (scalar @fastqF != 0 ) {
  # non-zero sized fastq file exist
  system("mv *fastq temp");

  chdir "$outdir/forward/temp";
  opendir(DIR, "$outdir/forward/temp");
  my @files = grep(/\.fastq$/,readdir(DIR));
  closedir(DIR);
  print join(",", @files), "\n";

  ### Trimming
  print "Start trimming and converting fastq to qiime-formated fasta: ", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";
 
  # trim sequences: joined and unjoined(un1, un2);
  foreach my $file (@files) {
    sub_1::trim_1 ($trim_m, $qthreshold, $qwindowsize, $threads, $minlen, $usearch, $file);
  } 
  print "trimmed all forward fastq sequences! \n\n";
  system("mv *trim*fastq $outdir/forward");


  chdir "$outdir/forward";

  system ("fastqc *fastq");
  system ("rm *.zip");
  system ("mv *_fastqc* $outdir/forward/fast_qc");
  sub_1::fastq2fasta_1 (); # only convert trim.fastq files
  system("mv *.fastq temp");
  sub_1::qiime_format_454 (); # generate trim.qiime.fasta
  system("mv *trim.fasta temp");
  system("cat *trim.qiime.fasta > all_$primer_nameF.qiime.fasta");
  system("mv *.trim.*fasta temp");
  # check chimera
  my $base_chimera=sub_1::chimera_uchime("all_$primer_nameF.qiime.fasta", $ref_seqs, $threads);
  # all_$primer_nameF.qiime.nonchimera.fasta
  system("cp all_$primer_nameF.qiime.nonchimera.fasta $outdir/both");
  # sort sequences
  sub_1::sort_seq_length_1($minlen, "all_$primer_nameF.qiime.nonchimera.fasta"); 
  #all_$primer_nameF.qiime.nonchimera.sorted.fasta
  system("mv all_$primer_nameF.qiime.nonchimera.fasta temp ");
  my $basename = sub_1::pick_otu_rep_454 ($otu_m, $similarity, $OTU_size, $max_memory,   $threads, "", "all_$primer_nameF");
  print $basename, "\n\n";
  system("mv *qiime.nonchimera.sorted.fasta temp");
  sub_1::assign_taxon_1 ($otu_m, $tax_m, $ref_seqs, $id_tax, $confidence, $max_memory,   $threads, $basename);
  sub_1::otu_table_1 ($otu_m, $tax_m, $basename);
  system("rm *tmp.uchime_formatted temp| mv *accnos temp | mv *chimeras temp | mv mothur.* temp");
  chdir "$outdir";
} else {
  print "No forward fastq file exists!\n\n";
}

#######################
## reverse sequences
#######################
print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nProcess reverse sequences...\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n";

chdir "$outdir/reverse";

## check if non-zero sized fastq files exist
my $dirR = '.';  # current dir
opendir(DIRHANDLE, $dirR) || die "Couldn't open directory $dirR: $!\n";
my @fastqR = grep {  /\.fastq$/ && -f "$dirR/$_" } readdir DIRHANDLE;
closedir DIRHANDLE;

if (scalar @fastqR != 0) {
  system("mv *fastq temp");

  chdir "$outdir/reverse/temp";
  opendir(DIR, "$outdir/reverse/temp");
  my @files_rev = grep(/\.fastq$/,readdir(DIR));
  closedir(DIR);
  print join(",", @files_rev), "\n";

  ### Trimming
  print "Start trimming and converting fastq to qiime-formated fasta: ", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";
  
  # trim sequences: joined and unjoined(un1, un2);
  foreach my $file_rev (@files_rev) {
    sub_1::trim_1 ($trim_m, $qthreshold, $qwindowsize, $threads, $minlen, $usearch, $file_rev);
  } 
  print "trimmed all reverse fastq sequences! \n\n";
  system("mv *trim*fastq $outdir/reverse");


  chdir "$outdir/reverse";

  system ("fastqc *fastq");
  system ("rm *.zip");
  system ("mv *_fastqc* $outdir/reverse/fast_qc");
  sub_1::fastq2fasta_1 (); # only convert trim.fastq files
  system("mv *.fastq temp");
  sub_1::qiime_format_454 (); # generate trim.qiime.fasta
  system("mv *trim.fasta temp");
  sub_1::reverse_complement_1();
  system("cat *trim.qiime.rc.fasta > all_$primer_nameR.rc.qiime.fasta");
  system("mv *.trim.*fasta temp");
  # check chimera
  my $base_chimera=sub_1::chimera_uchime("all_$primer_nameR.rc.qiime.fasta", $ref_seqs, $threads);
  # all_$primer_nameR.qiime.nonchimera.fasta
  system("cp all_$primer_nameR.rc.qiime.nonchimera.fasta $outdir/both");
  # sort sequences
  sub_1::sort_seq_length_1($minlen, "all_$primer_nameR.rc.qiime.nonchimera.fasta"); 
  #all_$primer_nameR.qiime.nonchimera.sorted.fasta
  system("mv all_$primer_nameR.rc.qiime.nonchimera.fasta temp ");
  my $basename = sub_1::pick_otu_rep_454 ($otu_m, $similarity, $OTU_size, $max_memory,   $threads, "", "all_$primer_nameR.rc");
  print $basename, "\n\n";
  system("mv *qiime.nonchimera.sorted.fasta temp");
  sub_1::assign_taxon_1 ($otu_m, $tax_m, $ref_seqs, $id_tax, $confidence, $max_memory,   $threads, $basename);
  sub_1::otu_table_1 ($otu_m, $tax_m, $basename);
  system("rm *tmp.uchime_formatted temp| mv *accnos temp | mv *chimeras temp | mv mothur.* temp");
  chdir "$outdir";
} else {
  print "No Reverse fastq file exists!\n\n";
}


#####################################
## process both forward and 
## reverse.rc trimmed sequences
######################################
print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nProcess combined forward and reverse sequences...\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n";

chdir "$outdir/both";
## check if non-zero sized fastq files exist
my $dirB = '.';  # current dir
opendir(DIRHANDLE, $dirB) || die "Couldn't open directory $dirB: $!\n";
my @fastaB = grep {  /\.qiime\.nonchimera\.fasta$/ && -f "$dirB/$_" } readdir DIRHANDLE;
closedir DIRHANDLE;

if ( scalar @fastaB != 0) {
  system("cat all_*qiime.nonchimera.fasta > both.qiime.nonchimera.fasta");
  system("mv all_*.fasta temp");
  # sort
  sub_1::sort_seq_length_1($minlen, "both.qiime.nonchimera.fasta"); 
  system("mv both.qiime.nonchimera.fasta temp ");
  # both.qiime.nonchimera.sorted.fasta
  # process both.qiime.nonchimera.sorted.fasta
  my $basename_all = sub_1::pick_otu_rep_454 ($otu_m, $similarity, $OTU_size, $max_memory, $threads, "", "both");
  print $basename_all, "\n\n";
  system("mv *qiime.nonchimera.sorted.fasta temp");
  sub_1::assign_taxon_1 ($otu_m, $tax_m, $ref_seqs, $id_tax, $confidence, $max_memory, $threads, $basename_all);
  sub_1::otu_table_1 ($otu_m, $tax_m, $basename_all);
 
  chdir "$outdir";
} else {
  print "No forward and/or reverse .qiime.nonchimera.fasta file to be processed!\n\n";
}

if ( $ITS eq "FALSE") {
  ############################
  # NON-ITS or Not-using ITSx
  ############################
  chdir $outdir;
} else {
  ############################
  #  ITSx
  ############################
  chdir $outdir; 
   
}





my $end_time = time();
my $run_time = $end_time - $start_time;
print "Finished running. Took a total of $run_time seconds.\n";

push (@runtime_array, {'Total'=> "$run_time seconds"});
push (@runtime_array, $runtime_hash);
LogFile::addToLog("Runtime","Running time",\@runtime_array);  
LogFile::writeLog("$DATE.yaml");


chdir $outdir;


###################################################
# Process finished
###################################################


#sub timer {
#  my $process = shift;
#  $et1 = time();
#  $runtime_hash->{$process}=join(" ", $et1-$st1, "seconds");
#  $st1 = $et1;
#}

sub config{
  #this sub reads from the configuration file and checks if everything is defined.
  open (CONF,"$config_path") or die;
  while (<CONF>){
    my $line = $_;
    chomp $line;
    $line =~ s/\R//g;
    my @split;
    if ($line =~ m/^THREAD_COUNT/){
      @split = split(/=/,$line);
      $threads = $split[1];    
    }elsif($line =~ m/^MAX_MEMORY/){
      @split = split(/=/,$line);
      $max_memory = $split[1];
    }elsif($line =~ m/^TRIM_METHOD/){
      @split = split(/=/,$line);
      $trim_m = $split[1];
    }elsif($line =~m/^MINIMUM_LENGTH/){
      @split = split(/=/,$line);
      $minlen = $split[1];
    }elsif($line =~m/^QUALITY_THRESHOLD/){
      @split = split(/=/,$line);
      $qthreshold = $split[1];
    }elsif($line =~m/^WINDOW_SIZE/){
      @split = split(/=/,$line);
      $qwindowsize = $split[1];
    }elsif($line =~m/^JOIN/){
      @split = split(/=/,$line);
      $join = $split[1];
    }elsif($line =~m/^MINIMUM_OVERLAP/){
      @split = split(/=/,$line);
      $min_overlap = $split[1];
    }elsif($line =~m/^MAXIMUM_PERCENTAGE_DIFFERENCE/){
      @split = split(/=/,$line);
      $perc_max_diff = $split[1];
    }elsif($line =~m/^SINGLETONS/){
      @split = split(/=/,$line);
      $singletons = $split[1];
    }elsif($line =~m/^ITS_EXTRACTION/){
      @split = split(/=/,$line);
      $ITS = $split[1];
    }elsif($line =~m/^OTU_METHOD/){
      @split = split(/=/,$line);
      $otu_m = $split[1];
    }elsif($line =~m/^SIMILARITY/){
      @split = split(/=/,$line);
      $similarity = $split[1];
    }elsif($line =~m/^OTU_SIZE/){
      @split = split(/=/,$line);
      $OTU_size = $split[1];
    }elsif($line =~m/^TAX_METHOD/){
      @split = split(/=/,$line);
      $tax_m = $split[1];
    }elsif($line =~m/^CONFIDENCE/){
      @split = split(/=/,$line);
      $confidence = $split[1];
    }elsif($line =~m/^EVALUE/){
      @split = split(/=/,$line);
      $e_value = $split[1];
    }elsif($line =~m/^NUM_ALIGNMENTS/){
      @split = split(/=/,$line);
      $num_alignments = $split[1];
    }elsif($line =~m/^CLOSED_REFERENCE/){
      @split = split(/=/,$line);
      $closed_ref = $split[1];
    }elsif($line =~m/^PAIRED_END/){
      @split = split(/=/,$line);
      $pairedend = $split[1];
    }elsif($line =~m/^KINGDOMS/){
      @split = split(/=/,$line);
      $kingdoms = $split[1];
    }
    LogFile::addToLog("Options","Configuration",{'Path' => $config_path, 'Threads' => $threads});

  }
  
  #check if all the values are defined

  close CONF;
}


__END__

=head1 NAME

=over 12

=item B<M>iB<S>eq B<A>mplicon B<M>etagenomics B<P>ipeline (MSAMP)

=back

=head1 SYNOPSIS

perl MSAMP.pl [-i] [Options] [--help]

=head1 DESCRIPTION

The MiSeq Amplicon Metagenomics Pipeline process sequences generated by MiSeq sequencing machines using data gathered from various biodiversity surveys. 

To run, you must give a path to the input raw data folder. If the raw data files are not zipped (FASTQ files), they will be copied to output folder before running the pipeline. Output folder will be your current working directory if you do not provide one.

=head1 OUTPUTS

Stuff...

=head1 OPTIONS

=over 12

=item B<--help>

Program information and usage instructions. Detailed information and instructions are in the manual

=item B<-i>

Specify the path to the directory of the input data files [REQUIRED]

=item B<-o>

Specify the path of the output directory [Default: current working directory]

=item B<--closed>

Closed reference otu picking [Default: FALSE]

=item B<--reference>

Specify the path to the reference sequences for assigning taxonomy and/or closed reference otu picking

=item B<--taxonomy>

Specify the path to tab-delimited file mapping sequences to assigned taxonomy and/or closed reference otu picking

=item B<--ITS>

Specify which region of ITS you want to extract, must be one of ITS1, ITS2 [Default: none]

=item B<--threads> [N]

Specify how many processors/threads you want to use to run the multi-threaded portions of this pipeline. N processors will be used [Default: 1]

=item B<--trim_method>

Sequence trimming method, must be one of usearch, mothur [Default: usearch]

=item B<--extract>

For if the raw data file(s) need to be decompressed [Default: FALSE]

=item B<--min_overlap> [N]

N-minimum overlap required when joining forward and reverse sequence [Default: 50]

=item B<--perc_max_diff> [N]

N-percent B<MAXIMUM> difference for the overlapping region when joining [Default: 1]E<10> E<8>(i.e. 1% maximum difference = 99% minimum similarity)

=item B<--minlen> [N]

Specify a minimum sequence length to keep. Lengths of sequences will be at least N bp long after sorting [Default: 100]

=item B<--qthreshold> [N]

B<mothur>: Set a minimum average quality score allowed over a window [Default: 20]E<10> E<8>B<usearch>: Truncate the sequence at the first position having quality score <= N [Default: 20]

=item B<--qwindowsize> [N]

Set a minimum average quality score allowed over a window, only used for mothur [Default: 10]

=item B<--OTU_size>

Minimum cluster size for size filtering with usearch [Default: 1]

=item B<--similarity> [N]

Sequence similarity threshold, only used for usearch, cd-hit [Default: 0.97]

=item B<--nosingletons>

Does not process unjoined forward/reverse reads [Default: FALSE]

=item B<--otu_method>

Clustering method, must be one of usearch, cdhit [Default: usearch]

=item B<--tax_method>

Taxonomy assignment method, must be one of mothur, rdp, blast [Default: mothur]

=item B<--max_memory> [N]

Maximum memory allocation, in MB, only used for cdhit, rdp [Default: 12000]

=item B<--confidence>

Minimum confidence to record an taxonomy assignment, only used for rdp and mothur methods [Default: 0.5]

=item B<--evalue> [N]

Expectation value threshold for saving hits

=item B<--blastdb>

Specify the path to the blast database when it is needed


