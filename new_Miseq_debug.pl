#!/usr/bin/env perl

use strict;
use warnings;
#cat rdp_assigned_taxonomy/ITS2_rep_set_tax_assignments.txt | iconv -f utf8 -t ascii//TRANSLIT//IGNORE > ITS2_rep_set_tax_assignments.noacc.txt
use Bio::SeqIO;
use Getopt::Long;
use File::Basename;
use File::Copy;
use File::Spec;
use FindBin;
use Cwd;

#modules
use lib "$FindBin::Bin"; #Path to the modules
use LogFile;
use sub_1;

#variables for options
my $dir;
my $outdir = getcwd();

my $help = 0;
#my $extract = 0;  #default is no extracting
my $singletons = "TRUE";  #default is to process all the singletons
my $closed_ref = "FALSE";  #default is de novo OTU picking
my $join = "TRUE";    
my $pairedend = "TRUE";  #for non-paired ended data
my $reg = "FALSE";
my $trim_m = "usearch";  #default trim method
my $otu_m = "uclust";  #default clustering method in qiime
my $tax_m = "mothur";  #default assign taxonomy method
my $kingdoms = "F";

my $e_value = 10;
my $threads = 4;
my $min_overlap = 50;
my $perc_max_diff = 1;
my $minlen = 100;
my $qthreshold = 20;
my $qwindowsize = 10;
my $OTU_size = 1;
my $similarity = 0.97;
my $confidence = 0.70;
my $num_alignments = 50;
my $max_memory = 12000;  #in MB (used for both cd-hit and rdp)
my $amplicon = "ITS";
my $usearch = "~/usearch7.0.1090_i86linux32"; #usearch version
my $blastdb = "/isilon/biodiversity/reference/blastdb/fungal_its/fno.fasta";
my $ref_seqs = "/home/AAFC-AAC/chenw/references/UNITE/sh_refs_qiime_ver7_97_s_02.03.2015.fasta";
# id-to-taxonomy database formated to 
# mothur format: 1) ; at the end; 2) no space in lineage
# qiime mothur format: 1) NO ; at the end; 2) no space in lineage
# qiime rdp format: 1) NO ; at the end; 2) exactly 6-levels for each lineage
my $id_tax = "/home/AAFC-AAC/chenw/references/UNITE/sh_taxonomy_qiime_ver7_97_s_02.03.2015.qiime.mothur.txt";
my $config_path;
my $mixed="TRUE";
my $oligo_file;

## CONFIG & SETUP
&setup;

#options
sub setup{
  GetOptions ( 
  "-o=s" => \$outdir, 
  "-i=s" => \$dir, 
  "--closed!" => \$closed_ref, 
  "--reference=s" => \$ref_seqs, 
  "--taxonomy=s" => \$id_tax, 
  "--ITS=s" => \$reg, 
  "--threads=i" => \$threads, 
  "--trim_method=s" => \$trim_m, 
  "--help!" => \$help, 
#  "--extract!" => \$extract, 
  "--min_overlap=i" => \$min_overlap, 
  "--perc_max_diff=i" => \$perc_max_diff, 
  "--minlen=i" => \$minlen, 
  "--qthreshold=i" => \$qthreshold, 
  "--qwindowsize=i" => \$qwindowsize, 
  "--OTU_size=i" => \$OTU_size, 
  "--similarity=s" => \$similarity, 
  "--singletons=s" => \$singletons, 
  "--join=s" => \$join, 
  "--otu_method=s" => \$otu_m, 
  "--tax_method=s" => \$tax_m,
  "--max_memory=i" => \$max_memory, 
  "--confidence=i" => \$confidence, 
  "--evalue" => \$e_value, 
  "--blastdb" => \$blastdb, 
  "--paired_end=s" => \$pairedend, 
  "-c=s" =>\$config_path, 
  "--num_alignments=s" =>\$num_alignments, 
  "--amplicon=s" =>\$amplicon,
  "--mixed=s" =>\$mixed,
  "--oligo_file=s" =>\$oligo_file)
};
if(defined $config_path){
    config();
  #reads in config file
  #check for config file existence 
   unless(-e "$config_path") {
     die "The specified config file: $config_path does not exist.\n";
  }   
}

#help page of the pipeline
if ($help) {
  system ("perldoc /home/AAFC-AAC/zhuk/miseq_amplicon/scripts/MSAMP.pl");
  exit
}


#get full command line string
my $cmd_str= qx/ps -o args $$/;
$cmd_str=~s/COMMAND\n//g;
chomp $cmd_str;
LogFile::addToLog("Runtime","Command Line",$cmd_str);

my $DATE=POSIX::strftime('%Y_%B%d_%H_%M',localtime()); 
my $HR_DATE=POSIX::strftime('%B %d, %Y %H:%M',localtime());            
#gets the date into the format YEAR/MONTH/HOUR/MINUTE

LogFile::addToLog('Date','Date and time',$HR_DATE);

#my $output_directory = "$DATE.Output"; #create output directory based on time

my @runtime_array;
my $runtime_hash = {};
my $start_time = time();
my $st = time();
my $et;

#getting the absolute directory paths for input and output directory
if ( -d $dir ) {
  print"\nInput directory is $dir\n";
} else {
  die "\nInvilid input directory, $dir does not exist\n";
}
if ( -d $outdir) {
  print"Output directory is $outdir\n\n";
} else {
  die "Invilid output directory, $outdir does not exist\n\n";
}

if ( $amplicon eq "ITS" ) {
  $ref_seqs = "/home/AAFC-AAC/chenw/references/UNITE/sh_refs_qiime_ver7_99_s_02.03.2015.genbank_O_20150530.qiime.mothur.fasta";
  $id_tax = "/home/AAFC-AAC/chenw/references/UNITE/sh_taxonomy_qiime_ver7_99_s_02.03.2015.genbank_O_20150530.qiime.mothur.txt";
} elsif ( $amplicon eq "16S" ) {
    $ref_seqs = "/home/AAFC-AAC/chenw/references/greengenes/gg_13_8_99.fasta";
  $id_tax = "/home/AAFC-AAC/chenw/references/greengenes/gg_13_8_99.gg.qiime.mothur.txt";
}

if ( $mixed eq "TRUE") {
  if (-z $oligo_file) {
    die "Please provide oligo file to split libraries\n\n"
  }
}

LogFile::addToLog("IO","Input Folder",$dir);
LogFile::addToLog("IO","Output Folder",$outdir);

print "Start processing...", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";
#mkdir "temp";

###############################################################################
# Step1: decompress fastq.gz files or 
# copy already decompressed fastq files to $outdir
###############################################################################
chdir $outdir;
sub_1::unzip_1($dir, $outdir);
#removing any potential characters in the 
#file names that could cause a problem downstream
sub_1::name_correction_1 ();

chdir $outdir;

########################################################################
# Step2: fastqc raw data and move to raw_data folder
########################################################################

chdir $outdir;

mkdir "temp";
mkdir "fast_qc"; # fastqc raw fastq
#system ("fastqc *fastq");
#system ("rm *.zip");
#system ("mv *_fastqc* ./fast_qc");

mkdir "raw_data"; #new directory to store the raw data
system("mv *fastq raw_data");

chdir $outdir;

###################################################################
# Step3: split regions if they are mixed, need oligo files
###################################################################
if ($mixed eq "TRUE") {
  # check format of $oligo_file
  unless ( -e $oligo_file) {
    die "please provide primer information in the following format and then re-try:\n\nforward\tGGAAGTAAAAGTCGTAACAAGG\tITS1_region:forward:ITS5\nforward\tGCTGCGTTCTTCATCGATGC\tITS1_region:reverse:ITS2\n\n
: $!";
  }
  open (OL, "<$oligo_file");
  my @regions;
  my @region_primer;
  while (<OL>) {
    chomp $_;
    print $_, "\n";
    if ( $_ =~ m/#/ ) {
      next
    } else {
      my ($oli, $sequence, $primer) = split /\t/, $_;
      my ($region, $direction, $primer_name) = split /:/, $primer;
      push (@regions, $region);
      push (@region_primer, $primer);
    }
  }
  close OL;
  my @regions_uniq = do { my %seen; grep { !$seen{$_}++ } @regions };
  my @region_primer_uniq = do { my %seen; grep { !$seen{$_}++ } @region_primer };
  ##### split foward and reverse sequences
  chdir $outdir;
  foreach my $regi (@regions) {
    mkdir "$outdir/$regi";
    mkdir "$outdir/$regi/raw_data";
    mkdir "$outdir/$regi/temp";
    mkdir "$outdir/$regi/fast_qc";
  }

  #system("cp $outdir/raw_data/*fastq $outdir/temp");
  chdir "$outdir/raw_data";
  print "$outdir/raw_data", "\n";

  opendir(DIR, "$outdir/raw_data") or die "$!";
  my @raw_fastqF = grep($_=~ m/_R1_/ && $_=~ m/\.fastq$/,readdir(DIR));
  closedir(DIR);

  opendir(DIR, ".");
  my @raw_fastqR = grep($_=~ m/_R2_/ && $_=~ m/\.fastq$/,readdir(DIR));
  closedir(DIR);

  opendir(DIR, ".");
  my @raw_fastq = grep($_=~ m/\.fastq$/,readdir(DIR));
  closedir(DIR);
 
  print scalar @raw_fastq, " total fastq files\n", scalar @raw_fastqF, " forward fastq files: ", join(", ", @raw_fastqF), "\n", scalar @raw_fastqR, " reverse fastq files: ", join(",", @raw_fastqR), "\n";

  foreach my $fq (@raw_fastq) {
    print $fq, "\n";
    sub_1::fastq2fasta2_1($fq);
  }
  system("mv *fasta $outdir/temp");


  chdir "$outdir/temp";
  opendir(DIR, "$outdir/temp");
  my @raw_fastaF = grep($_=~ m/_R1_/ && $_=~ m/\.fasta$/,readdir(DIR));
  closedir(DIR);

  opendir(DIR, "$outdir/temp");
  my @raw_fastaR = grep($_=~ m/_R2_/ && $_=~ m/\.fasta$/,readdir(DIR));
  closedir(DIR);

  opendir(DIR, "$outdir/temp");
  my @raw_fasta = grep($_=~ m/\.fasta$/,readdir(DIR));
  closedir(DIR);

  print scalar @raw_fastaF, " forward files: ", join(", ", @raw_fastaF), "\n";
  print scalar @raw_fastaR, " reverse files: ", join(", ", @raw_fastaR), "\n";

  sub_1::split_miseq($oligo_file, $outdir, "$outdir/temp", $threads); # files has been moved to $outdir/$reg/

  chdir $outdir;

  # fastq-join won't work if forward and reverse IDs do not match, so need to filter out the ones still exist in both _R1_ and _R2_ files

  foreach my $reg ( @regions_uniq) { 

    print "\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nProcess $reg sequences...\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n";

    chdir "$outdir/$reg/raw_data";
    
    system("mv $outdir/$reg/*fastq $outdir/$reg/raw_data | mv $outdir/$reg/*ID $outdir/$reg/raw_data");
    print "Sort out forward and reverse $reg sequences...", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n\n";    

    opendir(DIR, ".");
    my @fastq_R1 = sort(grep $_ =~ m/_R1_/ && $_ =~ m/\.fastq/, readdir(DIR));
    closedir (DIR);

    opendir(DIR, ".");
    my @fastq_R2 = sort(grep $_ =~ m/_R2_/ && $_ =~ m/\.fastq/, readdir(DIR));
    closedir (DIR);

    print join(", ", @fastq_R1), "\n";
    my @samples;

    if ( $join eq "TRUE" ) {
      ########################
      # join forward & reverse 
      # matched sequences?            
      #########################
      foreach my $fq (@fastq_R1) {
        my $base = basename($fq, ".fastq");
        my @names = split /_R[1,2]_/, $base; 
        my $base1 = $names[0];
        push (@samples, $base1);
        my $base2 = $names[1];
        print $base1, "\t", $base2, "\n";
        system("comm -23 $base1\_R1_$base2.ID $base1\_R2_$base2.ID > R1.ID | comm -13 $base1\_R1_$base2.ID $base1\_R2_$base2.ID > R2.ID | comm -12 $base1\_R1_$base2.ID $base1\_R2_$base2.ID > comm.ID");
        if ( -z "R1.ID" ) {
          print "$base1\_R1_$base2.fastq does not have unmatched sequences in comparison with $base1\_R2_$base2.fastq\n";
          system("~/seqtk/seqtk subseq $base1\_R1_$base2.fastq R1.ID > $base1\_R1_$base2.nomatch.fastq");    
        } else {
          system("~/seqtk/seqtk subseq $base1\_R1_$base2.fastq R1.ID > $base1\_R1_$base2.nomatch.fastq");
        } 
        if ( -z "R2.ID" ) {
          print "$base1\_R2_$base2.fastq does not have unmatched sequences in comparison with $base1\_R1_$base2.fastq\n";   
          system("~/seqtk/seqtk subseq $base1\_R2_$base2.fastq R2.ID > $base1\_R2_$base2.nomatch.fastq");  
        } else {
          system("~/seqtk/seqtk subseq $base1\_R2_$base2.fastq R2.ID > $base1\_R2_$base2.nomatch.fastq");
        }
        if ( -z "comm.ID" ) {
          if ($join eq "TRUE") {
            die "$base1\_R1_$base2.fastq and $base1\_R2_$base2.fastq do not have matched sequences! CANNOT join\n\n"; 
          } else {
            print "$base1\_R1_$base2.fastq and $base1\_R2_$base2.fastq do not have matched sequences\n";
            system("~/seqtk/seqtk subseq $base1\_R1_$base2.fastq comm.ID > $base1\_R1_$base2.match.fastq");
          system("~/seqtk/seqtk subseq $base1\_R2_$base2.fastq comm.ID > $base1\_R2_$base2.match.fastq");
          }     
        } else {
          system("~/seqtk/seqtk subseq $base1\_R1_$base2.fastq comm.ID > $base1\_R1_$base2.match.fastq");
          system("~/seqtk/seqtk subseq $base1\_R2_$base2.fastq comm.ID > $base1\_R2_$base2.match.fastq");
        }
        ### Join sequences
        system("~/fastq-join -m $min_overlap -p $perc_max_diff $base1\_R1_$base2.match.fastq $base1\_R2_$base2.match.fastq -o $base1.%.fastq");
        
        # if $singletons eq true, also move singletons out.
        if ( $singletons eq "FALSE" ) {
          # move joined sequences to $outdir/$reg;
          system("mv  $base1.join.fastq $outdir/$reg");        
        } else {
          ## keep all non-match forward and reverse sequences
          system("cat $base1.un1.fastq $base1\_R1_$base2.nomatch.fastq > $outdir/$reg/$base1.un1.fastq");
          system("cat $base1.un2.fastq $base1\_R2_$base2.nomatch.fastq > $outdir/$reg/$base1.un2.fastq");
          system("mv  $base1.join.fastq $outdir/$reg");
        }
      }
    } else {
      ### Join eq "FALSE"
      system("mv *fastq $outdir/$reg");
    }
    #exit;
    ###########################################
    # Now all samples for $reg has been sorted
    ###########################################
    chdir "$outdir/$reg";
 

    ### Trimming
    #my $st_ITS1=time();
    print "Start trimming and converting fastq to qiime-formated fasta: ", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";
    opendir(DIR, ".");
    my @files = grep(/\.fastq$/,readdir(DIR));
    closedir(DIR);

    
    # trim sequences: joined and unjoined(un1, un2);
    foreach my $file (@files) {
      my $file_base=basename($file, ".fastq");
      if ( -z $file ) {
        print "$file is empty!\n";
        system("mv $file temp");
      } else {
        sub_1::trim_1 ($trim_m, $qthreshold, $qwindowsize, $threads, $minlen, $usearch, $file);   
        system("mv $file temp");  
        # system ("fastqc *trim.fastq"); # fastqc trimmed
        # system ("rm *.zip");
        # system ("mv *_fastqc* $outdir/$reg/fast_qc");
        if ( -z "$file_base.trim.fastq" ) {
          print "$file has no sequences passed trimming criteria, relax your paramemters then re-try\n\n\n";
          #exit;
        } else {
          sub_1::fastq2fasta2_1 ("$file_base.trim.fastq"); # only convert trim.fastq files
          system("mv $file_base.trim.fastq temp");
          sub_1::qiime_format_1 ("$file_base.trim.fasta"); # generate trim.qiime.fasta
          system("mv $file_base.trim.fasta temp");         
          print "trimmed $file and converted to qiime-formatted fasta files: $file_base.trim.qiime.fasta! \n\n";
        }
      } 
    }
    my @files_trim;
    opendir(DIR, "$outdir/$reg/temp");
    @files_trim = grep(/\.trim\.fastq$/,readdir(DIR));
    closedir(DIR); 

    print scalar @files, " fastq files\n", scalar @files_trim, " trimmed fastq files\n", join(", ", @files_trim), "\n\n";     


    opendir(DIR, "$outdir/$reg/temp");
    my @files_trimF = grep( ($_=~ m/_R1_/ || $_=~ m/\.un1\./) && $_=~ m/\.trim\.fastq$/,readdir(DIR));
    closedir(DIR); 

    opendir(DIR, "$outdir/$reg/temp");
    my @files_trimR = grep( ($_=~ m/_R2_/ || $_=~ m/\.un2\./) && $_=~ m/\.trim\.fastq$/,readdir(DIR));
    closedir(DIR); 

    opendir(DIR, "$outdir/$reg/temp");
    my @files_trimJ = grep( $_=~ m/\.join\./ &&  $_=~ m/\.trim\.fastq$/,readdir(DIR));
    closedir(DIR);

    chdir "$outdir/$reg";

    if ( ($join eq "TRUE") && ($singletons eq "FALSE") ) {
      #############################
      # discard unjoined sequences
      #############################
      #my $st_ITS1=time();
      print "Start processing joined sequences: ", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";
      
      opendir (DIR, ".");
      my @files_join = grep(/\.join\.trim\.qiime\.fasta$/,readdir(DIR));
      closedir(DIR);
           
      if ( scalar @files_trimJ == 0 ) {
        print "No joined sequences to be processed!!!\n\n";
      } else {
        system("cat *.qiime.fasta > all_join.qiime.fasta");
        system("mv *.trim.*fasta temp");
        sub_1::recount_1("all_join.qiime.fasta"); 
        system("mv all_join.qiime.fasta temp ");
      
        sub_1::sort_seq_length_1($minlen, "all_join.qiime.recount.fasta"); 
        system("mv all_join.qiime.recount.fasta temp ");
        # process all_join.qiime.recount.sorted.fasta
        my $basename_f = sub_1::pick_otu_rep_1 ($otu_m, $similarity, $OTU_size, $max_memory, $threads, "", "all_join");
        print $basename_f, "\n\n";
        system("mv *qiime.recount.sorted.fasta temp");
        sub_1::assign_taxon_1 ($otu_m, $tax_m, $ref_seqs, $id_tax, $confidence, $max_memory, $threads, $basename_f);
        sub_1::otu_table_1 ($otu_m, $tax_m, $basename_f);
        #sub_1::otu_table_1 ($otu_m, "classify.seqs", $basename_f);    
        chdir "$outdir/$reg";
      }

    } elsif ( ($join eq "TRUE") && ($singletons eq "TRUE") ) {
      ## if consider unmatched forward and
      ## reverse sequences, split to forward
      ## and reverse

      mkdir "forward";
      mkdir "reverse";
      mkdir "forward/temp";
      mkdir "reverse/temp";
      mkdir "join";
      mkdir "join/temp";

      ##### joined only
      print "\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nStart processing joined sequences only: ", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n";
      #sub_1::cat_1("all_join");
      opendir (DIR, ".");
      my @files_join = grep(/\.join\.trim\.qiime\.fasta$/,readdir(DIR));
      closedir(DIR);
           
      if ( scalar @files_trimJ == 0 ) {
        print "No joined sequences to be processed!!!\n\n";
      } else {
        system("cp *join*.qiime.fasta join/");
        chdir "$outdir/$reg/join";
        system("cat *.qiime.fasta > all_join.qiime.fasta");
        system("mv *.trim.*fasta temp");
        sub_1::recount_1("all_join.qiime.fasta");
  
        system("mv all_join.qiime.fasta temp ");
      
        sub_1::sort_seq_length_1($minlen, "all_join.qiime.recount.fasta"); 
        system("mv all_join.qiime.recount.fasta temp ");
        # process all_join.qiime.recount.sorted.fasta
        my $basename_f = sub_1::pick_otu_rep_1 ($otu_m, $similarity, $OTU_size, $max_memory, $threads, "", "all_join");
        print $basename_f, "\n\n";
        system("mv *qiime.recount.sorted.fasta temp");
        sub_1::assign_taxon_1 ($otu_m, $tax_m, $ref_seqs, $id_tax, $confidence, $max_memory, $threads, $basename_f);
        sub_1::otu_table_1 ($otu_m, $tax_m, $basename_f);
          
        chdir "$outdir/$reg";
      }

      
      # reverse all *.un2.*fasta or *_R2_*fasta  to *.rc.fasta
      sub_1::reverse_complement(); 

      opendir(DIR, "$outdir/$reg");
      my @files_F = grep( $_=~ m/\.un1\.trim\.qiime\.fasta$/,readdir(DIR));
      closedir(DIR); 

      opendir(DIR, "$outdir/$reg");
      my @files_R = grep( $_=~ m/\.un2\.trim\.qiime\.rc\.fasta$/,readdir(DIR));
      closedir(DIR); 

      opendir(DIR, "$outdir/$reg");
      my @files_J = grep( $_=~ m/\.join\.trim\.qiime\.fasta$/,readdir(DIR));
    closedir(DIR);
      
      my @files_ = (@files_F, @files_R, @files_J);

      # move files to proper folders, forward or reverse;
      foreach my $f (@files_) {
        if ( -z $f ) {
          print "$f is empty\n";
        } else {
          if ( $f ~~ @files_F  ) {
            print "copy $f to $outdir/$reg/forward/\n";
            system("cp $f $outdir/$reg/forward/");
          } elsif ( $f ~~ @files_R ) {
            print "copy $f to $outdir/$reg/reverse, \n";
            system("cp $f $outdir/$reg/reverse/");
          } elsif ( $f ~~ @files_J ) {
            print "copy $f to $outdir/$reg/forward, \n";
            system("cp $f $outdir/$reg/forward/");
            print "copy $f to $outdir/$reg/reverse, \n";
            system("cp $f $outdir/$reg/reverse/");
          }     
        }
      }

      foreach my $fs (@files_) {
        system("mv $fs temp");
      }
      system("mv *.un2.trim.qiime.fasta  temp/");
            
      ################
      #forward
      ################
           
      { if ( scalar @files_trimF !=0 || scalar @files_trimJ !=0 ) {
          chdir "forward";
          print "\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nStart processing combined joined and forward unjoined sequences: ", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n";
          #sub_1::cat_1("all_forward");
          system("cat *.trim.qiime.fasta > all_forward.qiime.fasta");
          system("mv *.trim.*fasta temp");
          sub_1::recount_1("all_forward.qiime.fasta");
          system("mv all_forward.qiime.fasta temp ");
          # all_forward.recount.fasta
          sub_1::sort_seq_length_1($minlen, "all_forward.qiime.recount.fasta"); 
          system("mv all_forward.qiime.recount.fasta temp ");
          # process all_forward.recount.sorted.fasta
          my $basename_f = sub_1::pick_otu_rep_1 ($otu_m, $similarity, $OTU_size, $max_memory, $threads, "", "all_forward");
          print $basename_f, "\n\n";
          system("mv *qiime.recount.sorted.fasta temp");
          sub_1::assign_taxon_1 ($otu_m, $tax_m, $ref_seqs, $id_tax, $confidence, $max_memory, $threads, $basename_f);
          sub_1::otu_table_1 ($otu_m, $tax_m, $basename_f);
          
          chdir "$outdir/$reg";
        }
     }

      { if ( scalar @files_trimR !=0 || scalar @files_trimJ !=0 ) {
          chdir "reverse";
          print "\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nStart processing combined joined and reverse unjoined sequences: ", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n";

          #sub_1::cat_1("all_reverse.rc");
          system("cat *trim.qiime.*fasta > all_reverse.rc.qiime.fasta");
          system("mv *.trim.*fasta temp");
          sub_1::recount_1("all_reverse.rc.qiime.fasta");
          system("mv all_reverse.rc.qiime.fasta temp ");
          # all_reverse.rc.recount.fasta
          sub_1::sort_seq_length_1($minlen, "all_reverse.rc.qiime.recount.fasta"); 
          system("mv all_reverse.rc.qiime.recount.fasta temp ");
          # process all_reverse.rc.recount.sorted.fasta
          my $basename_f = sub_1::pick_otu_rep_1 ($otu_m, $similarity, $OTU_size, $max_memory, $threads, "", "all_reverse.rc");
          print $basename_f, "\n\n";
          system("mv *qiime.recount.sorted.fasta temp");
          sub_1::assign_taxon_1 ($otu_m, $tax_m, $ref_seqs, $id_tax, $confidence, $max_memory, $threads, $basename_f);
          sub_1::otu_table_1 ($otu_m, $tax_m, $basename_f);
             
          chdir "$outdir/$reg"; 
        }
      }

     
    } else {
      ######################
      # don't join sequences
      ######################
      # $join eq "FALSE
      
      chdir "$outdir/$reg";
      
      mkdir "forward";
      mkdir "reverse";
      mkdir "forward/temp";
      mkdir "reverse/temp";
      
      # reverse all *.un2.*fasta or *_R2_*fasta  to *.rc.fasta
      opendir(DIR, ".");
      my @files_F = sort(grep $_ =~ m/_R1_/ && $_ =~ m/\.fasta/, readdir(DIR));
      close DIR;

      opendir(DIR, ".");
      my @files_R = sort(grep $_ =~ m/_R2_/ && $_ =~ m/\.fasta/, readdir(DIR));
      close DIR;

      if ( scalar @files_R != 0 ) {
        sub_1::reverse_complement(); 
        system("mv *_R2_*trim.qiime.rc.fasta reverse");
        system("mv *_R2_*trim.qiime.fasta reverse/temp");
      } else {
        print "No reverse sequences to be processed\n\n";
      }
      if ( scalar @files_F != 0 ) {
        system("mv *_R1_*trim.qiime.fasta forward");
      } else {
        print "No forward sequences to be processed\n\n";
      }
      
      #system("mv *.trim.qiime.*fasta temp");
            
      ################
      #forward
      ################
           
      {  if ( scalar @files_F != 0 ) {
           chdir "forward";
           #sub_1::cat_1("all_forward");
           system("cat *.qiime.fasta > all_forward.qiime.fasta");
           system("mv *.trim.*fasta temp");
           sub_1::recount_1("all_forward.qiime.fasta");
           system("mv all_forward.qiime.fasta temp ");
           # all_forward.recount.fasta
           sub_1::sort_seq_length_1($minlen, "all_forward.qiime.recount.fasta"); 
           system("mv all_forward.qiime.recount.fasta temp ");
           # process all_forward.recount.sorted.fasta
           my $basename_f = sub_1::pick_otu_rep_1 ($otu_m, $similarity, $OTU_size, $max_memory, $threads, "", "all_forward");
           print $basename_f, "\n\n";
           system("mv *qiime.recount.sorted.fasta temp");
           sub_1::assign_taxon_1 ($otu_m, $tax_m, $ref_seqs, $id_tax, $confidence, $max_memory, $threads, $basename_f);
           sub_1::otu_table_1 ($otu_m, $tax_m, $basename_f);
              
           chdir "$outdir/$reg";
         }
      }

      { if ( scalar @files_R != 0 ) {
          chdir "reverse";
          #sub_1::cat_1("all_reverse.rc");
          system("cat *trim.qiime.rc.fasta > all_reverse.rc.qiime.fasta");
          system("mv *.trim.*fasta temp");
          sub_1::recount_1("all_reverse.rc.qiime.fasta");
          system("mv all_reverse.rc.qiime.fasta temp ");
          # all_reverse.rc.recount.fasta
          sub_1::sort_seq_length_1($minlen, "all_reverse.rc.qiime.recount.fasta"); 
          system("mv all_reverse.rc.qiime.recount.fasta temp ");
          # process all_reverse.rc.recount.sorted.fasta
          my $basename_f = sub_1::pick_otu_rep_1 ($otu_m, $similarity, $OTU_size, $max_memory, $threads, "", "all_reverse.rc");
          print $basename_f, "\n\n";
          system("mv *qiime.recount.sorted.fasta temp");
          sub_1::assign_taxon_1 ($otu_m, $tax_m, $ref_seqs, $id_tax, $confidence, $max_memory, $threads, $basename_f);
          sub_1::otu_table_1 ($otu_m, $tax_m, $basename_f);
           
          chdir "$outdir/$reg"; 
        }
      }
    }
  } 
}

if ( $mixed eq "FALSE") {
  ############################
  # ITS1 or ITS2 only or 16S v4 only etc
  ############################
  chdir $outdir;

  # fastq-join won't work if forward and reverse IDs do not match, so need to filter out the ones still exist in both _R1_ and _R2_ files


  chdir "$outdir/raw_data";

  print "Sort out forward and reverse sequences...", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";    

  opendir(DIR, ".");
  my @fastq_R1 = sort(grep $_ =~ m/_R1_/ && $_ =~ m/\.fastq/, readdir(DIR));
  close DIR;

  opendir(DIR, ".");
  my @fastq_R2 = sort(grep $_ =~ m/_R2_/ && $_ =~ m/\.fastq/, readdir(DIR));
  close DIR;

  print join(",", @fastq_R1), "\n";
  my @samples;

  if ( $join eq "TRUE" ) {
  ########################
  # join forward & reverse 
  # matched sequences?            
  #########################
    foreach my $fq (@fastq_R1) {
      my $base = basename($fq, ".fastq");
      my @names = split /_R[1,2]_/, $base; 
      my $base1 = $names[0];
      push (@samples, $base1);
      my $base2 = $names[1];
      print $base1, "\t", $base2, "\n";
      
      ### Join sequences
      
      system("~/fastq-join -m $min_overlap -p $perc_max_diff $base1\_R1_$base2.fastq $base1\_R2_$base2.fastq -o $base1.%.fastq");
      
      my $CWD= getcwd();
      print $CWD, "\n\n";
      
      # if $singletons eq true, also move singletons out.
      if ( $singletons eq "FALSE" ) {
        # move joined sequences to $outdir;
        system("mv  $base1.join.fastq $outdir"); 
        system("mv $base1.un1.fastq $outdir/temp");
        system("mv $base1.un2.fastq $outdir/temp");  
        
      } else  {
        # ($singletons eq "TRUE")
        ## keep all non-match forward and reverse sequences
        system("mv $base1.un1.fastq $outdir");
        system("mv $base1.un2.fastq $outdir");
        system("mv $base1.join.fastq $outdir");
      }
    }

  } else {
    system("mv *fastq $outdir");
  }
      
  ###########################################
  # Now all samples for $reg has been sorted
  ###########################################
  chdir "$outdir";
  if ( $join eq "TRUE") {
  #  system ("fastqc *fastq"); # fastqc joined and not joined 
  #  system ("rm *.zip");
  #  system ("mv *_fastqc* $outdir/$reg/fast_qc");
  }
  
  ### Trimming
  #my $st_ITS1=time();
  print "Start trimming and converting fastq to qiime-formated fasta: ", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";

  opendir(DIR, ".");
  my @files = grep(/\.fastq$/,readdir(DIR));
  closedir(DIR);
  
  # trim sequences: joined and unjoined(un1, un2);
  foreach my $file (@files) {
    my $file_base=basename($file, ".fastq");
    if ( -z $file ) {
      print "$file is empty!\n";
      system("mv $file temp");
    } else {
      sub_1::trim_1 ($trim_m, $qthreshold, $qwindowsize, $threads, $minlen, $usearch, $file);     
      system("mv $file temp");
      # system ("fastqc *trim.fastq"); # fastqc trimmed
      # system ("rm *.zip");
      # system ("mv *_fastqc* $outdir/$reg/fast_qc");
      if ( -z "$file_base.trim.fastq" ) {
        print "$file has no sequences passed trimming criteria, relax your paramemters then re-try\n\n\n";
        #exit;
      } else {
        sub_1::fastq2fasta2_1 ("$file_base.trim.fastq"); # only convert trim.fastq files
        system("mv $file_base.trim.fastq temp");
        sub_1::qiime_format_1 ("$file_base.trim.fasta"); # generate trim.qiime.fasta
        system("mv $file_base.trim.fasta temp");      
        print "trimmed $file and converted to qiime-formatted fasta files: $file_base.trim.qiime.fasta! \n\n";
      }
    } 
  }
  my @files_trim;

  opendir(DIR, "$outdir/temp");
  @files_trim = grep(/\.trim\.fastq$/,readdir(DIR));
  closedir(DIR); 

  print scalar @files, " fastq files\n", scalar @files_trim, " trimmed fastq files\n", join(", ", @files_trim), "\n\n"; 

  opendir(DIR, "$outdir/temp");
  my @files_trimF = grep( ($_=~ m/_R1_/ || $_=~ m/un1/) && $_=~ m/\.trim\.fastq$/,readdir(DIR));
  closedir(DIR);
 
  opendir(DIR, "$outdir/temp");
  my @files_trimR = grep( ($_=~ m/_R2_/ || $_=~ m/un2/) && $_=~ m/\.trim\.fastq$/,readdir(DIR));
  closedir(DIR); 

  opendir(DIR, "$outdir/temp");
  my @files_trimJ = grep( $_=~ m/.join./ &&  $_=~ m/\.trim\.fastq$/,readdir(DIR));
  closedir(DIR);

  chdir "$outdir";
  if ( ($join eq "TRUE") && ($singletons eq "FALSE") ) {
    #my $st_ITS1=time();
    print "Start processing joined sequences: ", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";

    opendir (DIR, ".");
    my @files_join = grep(/\.join\.trim\.qiime\.fasta$/,readdir(DIR));
    closedir(DIR);
           
    if ( scalar @files_trimJ == 0 ) {
      print "No trimmed joined sequences to be processed!!!\n\n";
    } else {
      system("cat *.qiime.fasta > all_join.qiime.fasta");
      system("mv *.trim.*fasta temp");
      sub_1::recount_1("all_join.qiime.fasta");
  
      system("mv all_join.qiime.fasta temp ");
      
      sub_1::sort_seq_length_1($minlen, "all_join.qiime.recount.fasta"); 
      system("mv all_join.qiime.recount.fasta temp ");
      # process all_join.qiime.recount.sorted.fasta
      my $basename_f = sub_1::pick_otu_rep_1 ($otu_m, $similarity, $OTU_size, $max_memory, $threads, "", "all_join");
      print $basename_f, "\n\n";
      system("mv *qiime.recount.sorted.fasta temp");
      sub_1::assign_taxon_1 ($otu_m, $tax_m, $ref_seqs, $id_tax, $confidence, $max_memory, $threads, $basename_f);
      sub_1::otu_table_1 ($otu_m, $tax_m, $basename_f);
        
      
    }
    chdir "$outdir";
  } elsif ( ($join eq "TRUE") && ($singletons eq "TRUE") ) {
    ## if consider unmatched forward and
    ## reverse sequences, split to forward
    ## and reverse

    mkdir "forward";
    mkdir "reverse";
    mkdir "forward/temp";
    mkdir "reverse/temp";
    mkdir "join";
    mkdir "join/temp";

    ##### joined only
    print "\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nStart processing joined sequences only: ", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n";
   

    opendir (DIR, ".");
    my @files_join = grep(/\.join\.trim\.qiime\.fasta$/,readdir(DIR));
    closedir(DIR);
           
    if ( scalar @files_trimJ == 0 ) {
      print "No trimmed joined sequences to be processed!!!\n\n";
    } else {
      system("cp *join*.qiime.fasta join/");
      chdir "$outdir/join";
      system("cat *.qiime.fasta > all_join.qiime.fasta");
      system("mv *.trim.*fasta temp");
      sub_1::recount_1("all_join.qiime.fasta");
  
      system("mv all_join.qiime.fasta temp ");
      
      sub_1::sort_seq_length_1($minlen, "all_join.qiime.recount.fasta"); 
      system("mv all_join.qiime.recount.fasta temp ");
      # process all_join.qiime.recount.sorted.fasta
      my $basename_f = sub_1::pick_otu_rep_1 ($otu_m, $similarity, $OTU_size, $max_memory, $threads, "", "all_join");
      print $basename_f, "\n\n";
      system("mv *qiime.recount.sorted.fasta temp");
      sub_1::assign_taxon_1 ($otu_m, $tax_m, $ref_seqs, $id_tax, $confidence, $max_memory, $threads, $basename_f);
      sub_1::otu_table_1 ($otu_m, $tax_m, $basename_f);
       
 
    }
    chdir "$outdir";


      
    # reverse all *.un2.*fasta or *_R2_*fasta  to *.rc.fasta
    if (scalar @files_trimR !=0 ) {
      sub_1::reverse_complement(); 
    } else {
      print "No trimmed reverse sequences to be processed\n\n";
    }

    opendir(DIR, "$outdir");
    my @files_F = grep( $_=~ m/\.un1\.trim\.qiime\.fasta$/,readdir(DIR));
    closedir(DIR); 

    opendir(DIR, "$outdir");
    my @files_R = grep( $_=~ m/\.un2\.trim\.qiime\.rc\.fasta$/,readdir(DIR));
    closedir(DIR); 

    opendir(DIR, "$outdir");
    my @files_J = grep( $_=~ m/\.join\.trim\.qiime\.fasta$/,readdir(DIR));
    closedir(DIR);
      
    my @files_ = (@files_F, @files_R, @files_J);

    # move files to proper folders, forward or reverse;
    foreach my $f (@files_) {
      if ( -z $f ) {
        print "$f is empty\n";
      } else {
        if ( $f ~~ @files_F  ) {
          print "copy $f to $outdir/forward/\n";
          system("cp $f $outdir/forward/");
        } elsif ( $f ~~ @files_R ) {
          print "copy $f to $outdir/reverse/, \n";
          system("cp $f $outdir/reverse/");
        } elsif ( $f ~~ @files_J ) {
          print "copy $f to $outdir/forward, \n";
          system("cp $f $outdir/forward/");
          print "copy $f to $outdir/reverse, \n";
          system("cp $f $outdir/reverse/");
        }     
      }
    }

    foreach my $fs (@files_) {
      system("mv $fs temp");
    }
    system("mv *.un2.trim.qiime.fasta  temp/");

    chdir "$outdir";  
            
    ################
    #forward
    ################
           
    { if ( scalar  @files_trimF !=0 || scalar @files_trimJ !=0 ) {
        chdir "forward";
        print "\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nStart processing combined joined and forward unjoined sequences: ", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n";
        #sub_1::cat_1("all_forward");
        system("cat *.trim.qiime.fasta > all_forward.qiime.fasta");
        system("mv *.trim.*fasta temp");
        sub_1::recount_1("all_forward.qiime.fasta");
        system("mv all_forward.qiime.fasta temp ");
        # all_forward.recount.fasta
        sub_1::sort_seq_length_1($minlen, "all_forward.qiime.recount.fasta"); 
        system("mv all_forward.qiime.recount.fasta temp ");
        # process all_forward.recount.sorted.fasta
        my $basename_f = sub_1::pick_otu_rep_1 ($otu_m, $similarity, $OTU_size, $max_memory, $threads, "", "all_forward");
        print $basename_f, "\n\n";
        system("mv *qiime.recount.sorted.fasta temp");
        sub_1::assign_taxon_1 ($otu_m, $tax_m, $ref_seqs, $id_tax, $confidence, $max_memory, $threads, $basename_f);
        sub_1::otu_table_1 ($otu_m, $tax_m, $basename_f);
            
        chdir "$outdir";
      }
    }

    { if ( scalar  @files_trimR !=0 || scalar @files_trimJ !=0 ) {
        chdir "reverse";
        print "\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nStart processing combined joined and reverse unjoined sequences: ", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n";
        #sub_1::cat_1("all_reverse.rc");
        system("cat *.trim.qiime.*fasta > all_reverse.rc.qiime.fasta");
        system("mv *.trim.*fasta temp");
        sub_1::recount_1("all_reverse.rc.qiime.fasta");
        system("mv all_reverse.rc.qiime.fasta temp ");
        # all_reverse.rc.recount.fasta
        sub_1::sort_seq_length_1($minlen, "all_reverse.rc.qiime.recount.fasta"); 
        system("mv all_reverse.rc.qiime.recount.fasta temp ");
        # process all_reverse.rc.recount.sorted.fasta
        my $basename_f = sub_1::pick_otu_rep_1 ($otu_m, $similarity, $OTU_size, $max_memory, $threads, "", "all_reverse.rc");
        print $basename_f, "\n\n";
        system("mv *qiime.recount.sorted.fasta temp");
        sub_1::assign_taxon_1 ($otu_m, $tax_m, $ref_seqs, $id_tax, $confidence, $max_memory, $threads, $basename_f);
        sub_1::otu_table_1 ($otu_m, $tax_m, $basename_f);
        
        chdir "$outdir"; 
      }
    }
     
  } else {
    ######################
    # don't join sequences
    ######################
    # $join eq "FALSE
      
    chdir "$outdir";
 
    mkdir "forward";
    mkdir "reverse";
    mkdir "forward/temp";
    mkdir "reverse/temp";
   
    # reverse all *.un2.*fasta or *_R2_*fasta  to *.rc.fasta
    opendir(DIR, ".");
    my @files_F = sort(grep $_ =~ m/_R1_/ && $_ =~ m/\.fasta$/, readdir(DIR));
    close DIR;

    opendir(DIR, ".");
    my @files_R = sort(grep $_ =~ m/_R2_/ && $_ =~ m/\.fasta$/, readdir(DIR));
    close DIR;

    if ( scalar @files_R != 0 ) {
      sub_1::reverse_complement(); 
      system("mv *_R2_*trim.qiime.rc.fasta reverse");
      system("mv *_R2_*trim.qiime.fasta reverse/temp");
    } else {
      print "No reverse sequences to be processed\n\n";
    }
    if ( scalar @files_F != 0 ) {
      system("mv *_R1_*trim.qiime.fasta forward");
    } else {
      print "No forward sequences to be processed\n\n";
    }
    system("mv *.trim.qiime.*fasta temp"); 
   # system("mv *fasta temp");
            
    ################
    #forward
    ################
           
    { if ( scalar @files_F != 0 ) {
        chdir "forward";
        #sub_1::cat_1("all_forward");
        system("cat *.qiime.fasta > all_forward.qiime.fasta");
        system("mv *.trim.*fasta temp");
        sub_1::recount_1("all_forward.qiime.fasta");
        system("mv all_forward.qiime.fasta temp ");
        # all_forward.recount.fasta
        sub_1::sort_seq_length_1($minlen, "all_forward.qiime.recount.fasta"); 
        system("mv all_forward.qiime.recount.fasta temp ");
        # process all_forward.recount.sorted.fasta
        my $basename_f = sub_1::pick_otu_rep_1 ($otu_m, $similarity, $OTU_size, $max_memory, $threads, "", "all_forward");
        print $basename_f, "\n\n";
        system("mv *qiime.recount.sorted.fasta temp");
        sub_1::assign_taxon_1 ($otu_m, $tax_m, $ref_seqs, $id_tax, $confidence, $max_memory, $threads, $basename_f);
        sub_1::otu_table_1 ($otu_m, $tax_m, $basename_f);
      
        chdir "$outdir";
      }
    }

    { if ( scalar @files_R != 0 ) {
        chdir "reverse";
        #sub_1::cat_1("all_reverse.rc");
        system("cat *trim.qiime.rc.fasta > all_reverse.rc.qiime.fasta");
        system("mv *.trim.*fasta temp");
        sub_1::recount_1("all_reverse.rc.qiime.fasta");
        system("mv all_reverse.rc.qiime.fasta temp ");
        # all_reverse.rc.recount.fasta
        sub_1::sort_seq_length_1($minlen, "all_reverse.rc.qiime.recount.fasta"); 
        system("mv all_reverse.rc.qiime.recount.fasta temp ");
        # process all_reverse.rc.recount.sorted.fasta
        my $basename_f = sub_1::pick_otu_rep_1 ($otu_m, $similarity, $OTU_size, $max_memory, $threads, "", "all_reverse.rc");
        print $basename_f, "\n\n";
        system("mv *qiime.recount.sorted.fasta temp");
        sub_1::assign_taxon_1 ($otu_m, $tax_m, $ref_seqs, $id_tax, $confidence, $max_memory, $threads, $basename_f);
        sub_1::otu_table_1 ($otu_m, $tax_m, $basename_f);
           
        chdir "$outdir"; 
      }
    }
  }
}
   

my $end_time = time();
my $run_time = $end_time - $start_time;
print "Finished running. Took a total of $run_time seconds.\n";

push (@runtime_array, {'Total'=> "$run_time seconds"});
push (@runtime_array, $runtime_hash);
LogFile::addToLog("Runtime","Running time",\@runtime_array);  
LogFile::writeLog("$DATE.yaml");


chdir $outdir;


###################################################
# Process finished
###################################################


#sub timer {
#  my $process = shift;
#  $et1 = time();
#  $runtime_hash->{$process}=join(" ", $et1-$st1, "seconds");
#  $st1 = $et1;
#}

sub config{
  #this sub reads from the configuration file and checks if everything is defined.
  open (CONF,"$config_path") or die;
  while (<CONF>){
    my $line = $_;
    chomp $line;
    $line =~ s/\R//g;
    my @split;
    if ($line =~ m/^THREAD_COUNT/){
      @split = split(/=/,$line);
      $threads = $split[1];    
    }elsif($line =~ m/^MAX_MEMORY/){
      @split = split(/=/,$line);
      $max_memory = $split[1];
    }elsif($line =~ m/^TRIM_METHOD/){
      @split = split(/=/,$line);
      $trim_m = $split[1];
    }elsif($line =~m/^MINIMUM_LENGTH/){
      @split = split(/=/,$line);
      $minlen = $split[1];
    }elsif($line =~m/^QUALITY_THRESHOLD/){
      @split = split(/=/,$line);
      $qthreshold = $split[1];
    }elsif($line =~m/^WINDOW_SIZE/){
      @split = split(/=/,$line);
      $qwindowsize = $split[1];
    }elsif($line =~m/^JOIN/){
      @split = split(/=/,$line);
      $join = $split[1];
    }elsif($line =~m/^MINIMUM_OVERLAP/){
      @split = split(/=/,$line);
      $min_overlap = $split[1];
    }elsif($line =~m/^MAXIMUM_PERCENTAGE_DIFFERENCE/){
      @split = split(/=/,$line);
      $perc_max_diff = $split[1];
    }elsif($line =~m/^SINGLETONS/){
      @split = split(/=/,$line);
      $singletons = $split[1];
    }elsif($line =~m/^ITS_EXTRACTION/){
      @split = split(/=/,$line);
      $reg = $split[1];
    }elsif($line =~m/^OTU_METHOD/){
      @split = split(/=/,$line);
      $otu_m = $split[1];
    }elsif($line =~m/^SIMILARITY/){
      @split = split(/=/,$line);
      $similarity = $split[1];
    }elsif($line =~m/^OTU_SIZE/){
      @split = split(/=/,$line);
      $OTU_size = $split[1];
    }elsif($line =~m/^TAX_METHOD/){
      @split = split(/=/,$line);
      $tax_m = $split[1];
    }elsif($line =~m/^CONFIDENCE/){
      @split = split(/=/,$line);
      $confidence = $split[1];
    }elsif($line =~m/^EVALUE/){
      @split = split(/=/,$line);
      $e_value = $split[1];
    }elsif($line =~m/^NUM_ALIGNMENTS/){
      @split = split(/=/,$line);
      $num_alignments = $split[1];
    }elsif($line =~m/^CLOSED_REFERENCE/){
      @split = split(/=/,$line);
      $closed_ref = $split[1];
    }elsif($line =~m/^PAIRED_END/){
      @split = split(/=/,$line);
      $pairedend = $split[1];
    }elsif($line =~m/^KINGDOMS/){
      @split = split(/=/,$line);
      $kingdoms = $split[1];
    }elsif($line =~m/^AMPLICON/){
      @split = split(/=/,$line);
      $amplicon = $split[1];
    }
    LogFile::addToLog("Options","Configuration",{'Path' => $config_path, 'Threads' => $threads});

  }
  
  #check if all the values are defined

  close CONF;
}


__END__

=head1 NAME

=over 12

=item B<M>iB<S>eq B<A>mplicon B<M>etagenomics B<P>ipeline (MSAMP)

=back

=head1 SYNOPSIS

perl MSAMP.pl [-i] [Options] [--help]

=head1 DESCRIPTION

The MiSeq Amplicon Metagenomics Pipeline process sequences generated by MiSeq sequencing machines using data gathered from various biodiversity surveys. 

To run, you must give a path to the input raw data folder. If the raw data files are not zipped (FASTQ files), they will be copied to output folder before running the pipeline. Output folder will be your current working directory if you do not provide one.

=head1 OUTPUTS

Stuff...

=head1 OPTIONS

=over 12

=item B<--help>

Program information and usage instructions. Detailed information and instructions are in the manual

=item B<-i>

Specify the path to the directory of the input data files [REQUIRED]

=item B<-o>

Specify the path of the output directory [Default: current working directory]

=item B<--closed>

Closed reference otu picking [Default: FALSE]

=item B<--reference>

Specify the path to the reference sequences for assigning taxonomy and/or closed reference otu picking

=item B<--taxonomy>

Specify the path to tab-delimited file mapping sequences to assigned taxonomy and/or closed reference otu picking

=item B<--ITS>

Specify which region of ITS you want to extract, must be one of ITS1, ITS2 [Default: none]

=item B<--threads> [N]

Specify how many processors/threads you want to use to run the multi-threaded portions of this pipeline. N processors will be used [Default: 1]

=item B<--trim_method>

Sequence trimming method, must be one of usearch, mothur [Default: usearch]

=item B<--extract>

For if the raw data file(s) need to be decompressed [Default: FALSE]

=item B<--min_overlap> [N]

N-minimum overlap required when joining forward and reverse sequence [Default: 50]

=item B<--perc_max_diff> [N]

N-percent B<MAXIMUM> difference for the overlapping region when joining [Default: 1]E<10> E<8>(i.e. 1% maximum difference = 99% minimum similarity)

=item B<--minlen> [N]

Specify a minimum sequence length to keep. Lengths of sequences will be at least N bp long after sorting [Default: 100]

=item B<--qthreshold> [N]

B<mothur>: Set a minimum average quality score allowed over a window [Default: 20]E<10> E<8>B<usearch>: Truncate the sequence at the first position having quality score <= N [Default: 20]

=item B<--qwindowsize> [N]

Set a minimum average quality score allowed over a window, only used for mothur [Default: 10]

=item B<--OTU_size>

Minimum cluster size for size filtering with usearch [Default: 1]

=item B<--similarity> [N]

Sequence similarity threshold, only used for usearch, cd-hit [Default: 0.97]

=item B<--nosingletons>

Does not process unjoined forward/reverse reads [Default: FALSE]

=item B<--otu_method>

Clustering method, must be one of usearch, cdhit [Default: usearch]

=item B<--tax_method>

Taxonomy assignment method, must be one of mothur, rdp, blast [Default: mothur]

=item B<--max_memory> [N]

Maximum memory allocation, in MB, only used for cdhit, rdp [Default: 12000]

=item B<--confidence>

Minimum confidence to record an taxonomy assignment, only used for rdp and mothur methods [Default: 0.5]

=item B<--evalue> [N]

Expectation value threshold for saving hits

=item B<--blastdb>

Specify the path to the blast database when it is needed


