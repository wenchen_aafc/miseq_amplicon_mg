#!/usr/bin/env perl

package Convert;

use strict;
use warnings;

use Bio::SeqIO;
use File::Basename;
use File::Copy;
use Cwd;

sub unzip {

  my $dir = shift;
  my $outdir = shift;
  
  opendir (DIR, $dir) or die $!;
  my @files = grep ($_ =~ m/\Qfastq.gz\E/ && $_ !~ m/Undetermined/, readdir(DIR));
  closedir (DIR);
  
  #unzips all the files in the array @files
        if ( scalar @files == 0 ) {
          print "no fastq.gz files were found in ", $dir, ", will not do decompression";
          copy_files ($dir, $outdir);
        } else {
      foreach my $file (@files) {
    my $basename = basename($file, ".gz");
    
    print "Unzipping $file\n";
    system ("gunzip -c $dir/$file > $outdir/$basename");
      }
          }
}

sub copy_files {
  
  my $dir = shift;
  my $outdir = shift;
  
  opendir (DIR, $dir) or die $!;
  my @files = grep ($_ =~ m/\Q.fastq\E/ && $_ !~ m/\Qfastq.gz\E/ && $_ !~ m/Undetermined/, readdir(DIR));
  closedir (DIR);
  
  foreach my $file (@files) {
    copy ("$dir/$file", "$outdir/$file");
  }
}

sub join_seqs {
  my $min_overlap = shift; #minimum overlap for the forward and reverse reads
  my $perc_max_diff = shift; #maximum percentage difference allowed for the overlapping region
  
  my @forward = sort (grep ($_ =~ m/comm/ && $_ =~ m/R1/, <*>));
  my @reverse = sort (grep ($_ =~ m/comm/ && $_ =~ m/R2/, <*>));
  print join(",", @forward), "\n";
        print join(",", @reverse), "\n";
  #assuming each forward reads file has a matching reverse reads file
  for (my $i = 0; $i < scalar (@forward); $i ++) {
    my $basename = join ('_', (split ('_', $forward[$i]))[0..2]); 
    print "\n\n", $forward[$i], "\t", $reverse[$i], "\n\n";
    print "Joining $forward[$i] and $reverse[$i]\n";
    system ("fastq-join -m $min_overlap -p $perc_max_diff $forward[$i] $reverse[$i] -o $basename.trim.%.fastq");
    
    my @files = <*>;
    foreach my $file (@files) {
      if ($file =~ m/un1/) {
        move ($file, "unjoined_forward");
      }
      elsif ($file =~ m/un2/) {
        move ($file, "unjoined_reverse");
      }
      elsif ($file !~ m/join/ && $file =~ m/$basename/) {
        unlink $file;
        #move ($file, "temp");
      }
      elsif ($file =~ m/join/ && $file =~ m/$basename/ && $file =~ m/fastq/) {
        system ("fastqc $file");
        system ("mv *_fastqc* fast_qc");
      }
    }
  }
}

sub qiime_format {
  my @files = grep ($_ =~ m/fasta$/, <*>);
  foreach my $fasta (@files) {
    my $basename = basename($fasta, ".fasta");
    
    print "Start to process $fasta\n";
    my $sampleid = (split (/_/, $fasta))[0];
    
    open (OUT, ">$basename.qiime.fasta");
    my $in_fasta = Bio::SeqIO->new(-file=>"$fasta", -format=>"Fasta");
    my $count=1;
    while (my $seq = $in_fasta->next_seq()) {
            my $seqid=join("_",$sampleid, $count);
            my $seqdesc=$seq->id; #obtain description line of a sequence
            print OUT ">", $seqid, "\t",$seqdesc, "\n", $seq->seq, "\n";
            $count++;
    }
    close OUT;
  }
}

sub qiime_re_format {
  my @files = grep ($_ =~ m/fasta$/ && $_ =~ m/qiime/ && $_ =~ m/dup/, <*>);
  foreach my $fasta (@files) {
    my $basename = basename($fasta, ".dup.fasta");
    
    print "Start to process $fasta\n";
    
    open (OUT, ">$basename.fasta");
    my $in_fasta = Bio::SeqIO->new(-file=>"$fasta", -format=>"Fasta");
    my $count=1;
    while (my $seq = $in_fasta->next_seq()) {
      my $sampleid=$seq->id;
      my $samplename=(split ("_", $sampleid))[0];
            my $seqid=join("_",$samplename, $count);
            print OUT ">", $seqid, "\n", $seq->seq, "\n";
            $count++;
    }
    close OUT;
  }
}

#name correction if needed
sub name_correction {
  my @special_char=("/", "*", "__", "\\","(",")",":",";",",","-");
  print "Special characters: @special_char will be removed from file name.\n";
  
  open (NAME, ">filename_match.txt"); #mapping file for old and new names
  print NAME "Old name\tNew name\n";
  
  my @files = grep ($_ =~ m/fastq$/, <*>);
  
  foreach my $file (@files) {
    my $new_name = $file;
    
    foreach (@special_char) {
      if ($file =~ m/\Q$_\E/) {
        $new_name =~ s/\Q$_\E//g;
      }
    }
    if ($file ne $new_name) {
      rename ("$file", "$new_name");
      print NAME "$file\t$new_name\n";
      print "$file renamed to $new_name\n";
    }
  }
  close NAME;
}

sub fastq2fasta {
  my @files = grep ($_ =~ m/fastq$/, <*>);
  
  foreach my $file (@files) {
    print "Converting $file\t". localtime, "\n";
    my $basename = basename ($file, ".fastq");
    
    my $in = Bio::SeqIO->new(-file=>"$file",-format=>'fastq');
    my $out = Bio::SeqIO->new(-file=>">$basename.fasta",-format=>'fasta');
    while (my $seq=$in->next_seq()){
      $out->write_seq($seq);
    }
    
    unlink $file;
    #move ($file, "temp");
  }
}

sub cat {
  my $prefix = shift;
  open CAT, ">$prefix.qiime.fasta";
  
  my @files = grep ($_ =~ m/qiime/, <*>);
  foreach my $file (@files) {
    my $in = Bio::SeqIO->new(-file=>"$file", -format=>'fasta');
    while (my $seq=$in->next_seq()) {
      print CAT ">", $seq->id, "\t", $seq->desc, "\n", $seq->seq, "\n";
    }
  }
  close CAT;
  print "Merged all files.\n";
}

sub fastqc_after_join {
  my $name = shift;
  my $dir = shift;
  
  my $number = 2;
  $number = 1 if ($name eq "forward");
  
  
  #my @filter = grep ($_ =~ m/fastq/ && $_ =~ m/uniq.trim/, <*>);
  my @join = sort (grep ($_ =~ m/fastq$/ && $_ =~ m/trim.un/, <*>));
  
  for (my $i=0; $i < scalar (@join); $i ++) {
    my $basename = (split (/\./, $join[$i]))[0];
    
    open (OUT, ">$basename.trim.R$number.fastq");
    
    my @files = ($join[$i], "$basename\_R$number.uniq.trim.fastq");
    foreach my $file (@files) {
      my $in = Bio::SeqIO->new(-file=>"$file", -format=>'fastq');
      my $out = Bio::SeqIO->new(-file=>">>$basename.trim.R$number.fastq",-format=>'fastq');
      while (my $seq=$in->next_seq()) {
        if ($name eq "forward") {
          $out -> write_seq($seq);
        } else {
          #my $seqrc = $seq->revcom();
          $out -> write_seq($seq);
          #$out -> write_seq($seqrc);
        }
      }
      unlink $file;
      #move ($file, "temp");
    }
    system ("fastqc $basename.trim.R$number.fastq");
    system ("mv *_fastqc* $dir/fast_qc");
    
    close OUT;
  }
    
}

sub length_sort {
  my $usearch = shift;
  my $minlen = shift; #minimum length for sequences  
  my $prefix = shift;
  
  system ("$usearch -sortbylength $prefix.qiime.fasta -output $prefix.qiime.sorted.fasta -minseqlength $minlen");
  print "$prefix.qiime.fasta sorted.\n";
}

sub reverse_complement {
  my @files = grep ($_ =~ m/.fastq$/, <*>);
  foreach my $file (@files) {
    print "Converting $file\t". localtime, "\n";
    my $basename = basename ($file, ".fastq");
    
    my $in = Bio::SeqIO->new(-file=>"$file",-format=>'fastq');
    my $out = Bio::SeqIO->new(-file=>">$basename.fasta",-format=>'fasta');
    while (my $seq=$in->next_seq()){
      $out->write_seq($seq);
    }
    
    open (MOTHUR, ">mothur.bat");
    print MOTHUR "reverse.seqs(fasta=$basename.fasta)\n";
    close MOTHUR;
    system ("mothur mothur.bat");
    system ("rm mothur.bat");
    
    unlink "$basename.fasta";
    #move ("$basename.fasta", "temp");
  }
  
  mkdir "mothur_logs";
  
  @files = grep ($_ =~ m/mothur/, <*>);
  foreach my $file (@files) {
    move ($file, "mothur_logs");
  }
  
}

sub fastqc {
  my @files = grep ($_ =~ m/.fastq$/, <*>);
  foreach my $file (@files) {
    system ("fastqc $file");
  }
}
1;
