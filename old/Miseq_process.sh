#! /bin/bash

if [[ $# != 2 ]]; then

	echo "process illumina miseq raw data at AAFC"
	echo "Usage: bash $0 <thread> <summary_table>"

	exit;
fi

folder_of_raw=$1 #/isilon/biodiversity/data/raw/illumina/AAFC/140721_M01696_0010_000000000-AAD2F
mapping=$2

set -x

# fullfilename=$(basename $1)
# extension=${fullfilename##*.}
# filename=${fullfilename%.*}
# DATE=$(date +%Y%m%d_%H_%M)

for i in $folder_of_raw/*gz; do fullfilename=$(basename $i); filename=${fullfilename%.*}; gunzip -c $i > $filename ; done

#for i in *fastq; do fastqc $i --extract --nogroup; done # --nogroup summarize each base
for i in *fastq; do fastqc $i --extract; done


## fastq-stats command
for i in *fastq; do usearch7.0.1090_i86linux32 -fastq_stats $i -log $i.stats.log; done


join_paired_ends.py -f $PWD/forward_reads.fastq -r $PWD/reverse_reads.fastq -b $PWD/barcodes.fastq -o $PWD/fastq-join_joined

#for i in *fastq; do fullfilename=$(basename $i); filename=${fullfilename%.*}; echo "fastq.info(fastq=$i)" > mothur.bat; mothur mothur.bat; rm mothur.bat; done

#Read quality filters often use an average Q score to determine if the read has high or low quality. This is a  really bad idea! The average Q score is not a good indicator of the number of errors predicted by the individual Q scores. This is illustrated by the example in the table below, which describes two reads of length 150nt.Notice that the read with higher average Q has a much larger number of expected errors due to the Q2 bases, which have an error probability of 0.5. With P=0.5, we expect about half of the Q2 bases to be wrong, so the expected number of errors in the read is at least 5. As this example shows, if there are a few low Q scores in a read with generally high Q scores, then the average Q is a very poor indicator of the expected accuracy of the read.

for i in *fastq ; do fullfilename=$(basename $i); filename=${fullfilename%.*}; usearch7.0.1090_i86linux32 -fastq_filter $i -fastq_minlen 100 -fastq_truncqual 15 -fastaout $filename.trim.fasta; done

for i in *_R2_*fastq; do fullfilename=$(basename $i); filename=${fullfilename%.*}; usearch7.0.1090_i86linux32 -fastq_filter $i -fastq_minlen 100 -fastq_truncqual 15 -fastaout $filename.trim.fasta; done

# for i in *trim.fastq; do usearch7.0.1090_i86linux32 -fastq_stats $i -log $i.stats.log; done

# for i in *_R1_*fasta; do fullfilename=$(basename $i); filename=${fullfilename%.*}; echo "trim.seqs(fasta=$i, qfile=$filename.qual, minlength=100, maxlength=400, maxhomop=10, qwindowsize=50, qwindowaverage=35, flip=F) 

# for i in *_R2_*fasta; do fullfilename=$(basename $i); filename=${fullfilename%.*}; echo "trim.seqs(fasta=$i, qfile=$filename.qual, minlength=100, maxlength=400, maxhomop=10, qwindowsize=50, qwindowaverage=35, flip=T) 

# convert fasta file to qiime format
for i in *_R1_*trim.fasta; do fullfilename=$(basename $i); filename=${fullfilename%.*}; time perl ~/perl/qiime.format.sequence.file.pl $i; done # generate .qiime.fna files

for i in *_R2_*trim.fasta; do fullfilename=$(basename $i); filename=${fullfilename%.*}; time perl ~/perl/qiime.format.sequence.file.pl $i; done # generate .qiime.fna files



#combine all trimed qiime formatted fna files
# total size of list of files
find . -name "*_R1_*trim.fasta.qiime.fna" -not -name "Undetermined*" | xargs du -hc
find . -name "*_R2_*trim.fasta.qiime.fna" -not -name "Undetermined*" | xargs du -hc

cat $(find . -name "*_R1_*trim.fasta.qiime.fna" -not -name "Undetermined*") > R1.trim.qiime1.fna

cat $(find . -name "*_R2_*trim.fasta.qiime.fna" -not -name "Undetermined*") > R2.trim.qiime1.fna

# uclust, clust to ref. -r --uclust_otu_id_prefix qiime_otu_
#The following commands implement a recentering strategy.
#usearch -cluster_fast reads.fasta -consout cons.fasta -id 0.97 -sizeout
#usearch -sortbysize cons.fasta -output cons_bysize.fasta
#usearch -cluster_smallmem cons_bysize.fasta -id 0.97 -centroids otus.fasta

/opt/bio/usearch/usearch6.0.307_i86linux32 -sortbylength R1.trim.qiime1.fna -output R1.trim.qiime.sorted.fna -minseqlength 64

/opt/bio/usearch/usearch6.0.307_i86linux32 -sortbylength R2.trim.qiime1.fna -output R2.trim.qiime.sorted.fna -minseqlength 64

pick_otus.py -i R1.trim.qiime.sorted.fna -o uclust_R1/ -s 0.97

pick_otus.py -i R2.trim.qiime.sorted.fna -o uclust_R2/ -s 0.97


/isilon/biodiversity/pipelines/qiime-1.6.0/bin/pick_otus.py -i R1.trim.qiime.sorted.fna -o usearch_R1_results/ -m usearch --word_length 64 --db_filepath ~/UNITE/sh_refs_qiime_ver6_dynamic_19.12.2013.fasta  -s 0.97 --suppress_reference_chimera_detection --suppress_de_novo_chimera_detection


# Pick_rep_set.py
/isilon/biodiversity/pipelines/qiime-1.6.0/bin/pick_rep_set.py -i uclust_R1/R1.trim.qiime.sorted_otus.txt -f R1.trim.qiime.sorted.fna -o R1.trim.qiime.sorted.uclust.rep.fna

/isilon/biodiversity/pipelines/qiime-1.6.0/bin/pick_rep_set.py -i uclust_R2/R2.trim.qiime.sorted_otus.txt -f R2.trim.qiime.sorted.fna -o R2.trim.qiime.sorted.uclust.rep.fna

# /isilon/biodiversity/pipelines/qiime-1.6.0/bin/pick_rep_set.py -i usearch_qf_results/R1.trim.qiime.txt -f R1.trim.qiime.fna -o R1.trim.qiime.uclust.rep.fna

# assign_taxonomy.py #
/isilon/biodiversity/pipelines/qiime-1.6.0/bin/assign_taxonomy.py -i R2.trim.qiime.sorted.uclust.rep.fna -m mothur -r ~/UNITE/sh_refs_qiime_ver6_dynamic_19.12.2013.fasta -t ~/UNITE/sh_taxonomy_qiime_ver6_dynamic_19.12.2013.txt -c 0.5
#-c default 0.8, use 0.5, bootstrap cutoff confidence

/isilon/biodiversity/pipelines/qiime-1.6.0/bin/assign_taxonomy.py -i R1.trim.qiime.sorted.uclust.rep.fna -m mothur -r ~/UNITE/sh_refs_qiime_ver6_dynamic_19.12.2013.fasta -t ~/UNITE/sh_taxonomy_qiime_ver6_dynamic_19.12.2013.txt -c 0.5
#-c default 0.8, use 0.5, bootstrap cutoff confidence

# edit classification change telemorph to anamorph

# make otu biom table
/isilon/biodiversity/pipelines/qiime-1.6.0/bin/make_otu_table.py -i uclust_R2/R2.trim.qiime.sorted_otus.txt -t mothur_assigned_taxonomy/R2.trim.qiime.sorted.uclust.rep_tax_assignments.txt -o R2.trim.qiime.sorted_otus.biom

/isilon/biodiversity/pipelines/qiime-1.6.0/bin/make_otu_table.py -i uclust_R1/R1.trim.qiime.sorted_otus.txt -t mothur_assigned_taxonomy/R1.trim.qiime.sorted.uclust.rep_tax_assignments.txt -o R1.trim.qiime.sorted_otus.biom

# convert biom to otu table

PYTHONPATH_TMP="$PYTHONPATH"
export PYTHONPATH=
export PYTHONPATH="$PYTHONPATH_TMP"

export PYTHONPATH=
#sudo biom convert -i R1.trim.qiime.sorted_otus.biom -o R1.trim.qiime.sorted_otus.tab -b # no taxonomy
sudo biom convert -i R1.trim.qiime.sorted_otus.biom -o R1.trim.qiime.sorted_otus.w.tax.tab -b --header-key taxonomy # with taxonomy



sudo grep -v "p__" R2.trim.qiime.sorted_otus.c0.85.tab |cut -f1|sed "1d" |sed "1d" > R2.trim.qiime.sorted_otus.c0.85.k_only.otuID

sudo perl -e ' ($id,$fasta)=@ARGV; open(ID,$id); while (<ID>) { s/\r?\n//; /^>?(\S+)/; $ids{$1}++; } $num_ids = keys %ids; open(F, $fasta); $s_read = $s_wrote = $print_it = 0; while (<F>) { if (/^>(\S+)/) { $s_read++; if ($ids{$1}) { $s_wrote++; $print_it = 1; delete $ids{$1} } else { $print_it = 0 } }; if ($print_it) { print $_ } }; END { warn "Searched $s_read FASTA records.\nFound $s_wrote IDs out of $num_ids in the ID list.\n" } ' R2.trim.qiime.sorted_otus.c0.85.k_only.otuID ../R2.trim.qiime.sorted.uclust.rep.fna > R2.trim.qiime.sorted_otus.c0.85.k_only.rep.fna

sudo cat R1.trim.qiime.sorted_otus.c0.85.k_only.otuID|while read line; do grep -vw "$line" ../analysis.mothur_assigned_taxonomy_c0.7/R1.trim.qiime.sorted_otus.c0.7.k_only.otuID; done > R1.c0.7.k_only.not.in.c0.85.otuID

sudo cat ../analysis.mothur_assigned_taxonomy_c0.7/R1.trim.qiime.sorted_otus.c0.7.k_only.otuID|while read line; do grep -vw "$line" R1.trim.qiime.sorted_otus.c0.85.k_only.otuID; done > R1.c0.85.k_only.not.in.c0.7.otuID

sudo biom convert -i R2.trim.qiime.sorted_otus.biom -o R2.trim.qiime.sorted_otus.w.tax.tab -b --header-key taxonomy # with taxonomy

sed -e "1d" -e "s/#//g" R2.trim.qiime.sorted_otus.w.tax.tab > R2.trim.qiime.sorted_otus.w.tax.forR.tab

grep -v "p__" R2.trim.qiime.sorted_otus.w.tax.tab |cut -f1 |sed "1d" |sed "1d" > R2.trim.qiime.sorted_otus.not.fungi.id

perl -e ' ($id,$fasta)=@ARGV; open(ID,$id); while (<ID>) { s/\r?\n//; /^>?(\S+)/; $ids{$1}++; } $num_ids = keys %ids; open(F, $fasta); $s_read = $s_wrote = $print_it = 0; while (<F>) { if (/^>(\S+)/) { $s_read++; if ($ids{$1}) { $s_wrote++; $print_it = 1; delete $ids{$1} } else { $print_it = 0 } }; if ($print_it) { print $_ } }; END { warn "Searched $s_read FASTA records.\nFound $s_wrote IDs out of $num_ids in the ID list.\n" } ' R2.trim.qiime.sorted_otus.not.fungi.id R2.trim.qiime.sorted.uclust.rep.fna > R2.trim.qiime.sorted_otus.not.fungi.rep.fna

# check mapping files
# check demultiplexing mapping files with barcodes and primer
# check_id_map.py -m $mapping -o check_id_output/ 
# check mapping files without barcodes and primer (keep the columns though)
check_id_map.py -m $mapping -o check_id_output/ -p -b


echo "$input_zipped_fastq" # fastq.gz file
	gz_fullfilename=$(basename $input_zipped_fastq)	
	gz_extension=${gz_fullfilename##*.}
	gz_filename=${gz_fullfilename%.*}

gunzip $input_zipped_fastq

echo "$f_filename" # fastq file
	fastq_fullfilename=$(basename $f_filename)	
	fastq_extension=${fastq_fullfilename##*.}
	fastq_filename=${fastq_fullfilename%.*}
mkdir $fastq_filename

mv $f_filename $fastq_filename
cd $fastq_filename


# convert fastq file to fasta and qual files
mkdir input.fna
for f in *fastq

do 
convert_fastaqual_fastq.py -c fastq_to_fastaqual -f $f -o $f.fastaqual # generate $fastq_filename.fna; $fastq_filename.qual



sed -e "s/fna/$f\.fna/g" -e "s/qual/$f\.qual/g" -e "s/revcom/T/g" mothur.trim.qual.bat > mothur.trim.qual.bat.1

mothur mothur.trim.qual.bat.1

# convert fasta file to qiime format
for f in $(find input.fna -wholename "*.fna")
do 
	time perl ~/Desktop/ubuntu_64bit_shared/svn_Bioprogram/perl_wen/qiime.format.sequence.file.pl $f

done

#### combine all qiime formatted sequence files
mkdir qiime.input.fna
mv input.fna/*.qiime.fna qiime.input.fna/
cat qiime.input.fna/*qiime.fna > all.fna

# de novo otu clustering using usearch for dereplicate and chimera checking
# chimera checking seems not picking up real chimera sequences: turn off chimera checking: --suppress_reference_chimera_detection --suppress_de_novo_chimera_detection
# --minsize 2 eliminate singletons

## usearch
pick_otus.py -i $fastq_filename.fastaqual/$fastq_filename.fna -o usearch_qf_results/ -m usearch --word_length 64 --db_filepath ~/Desktop/ubuntu_64bit_shared/UNITE/sh_refs_qiime_ver6_dynamic_19.12.2013.fasta  -s 0.97 --suppress_reference_chimera_detection --suppress_de_novo_chimera_detection

pick_otus.py -i $fastq_filename.fastaqual/$fastq_filename.fna -o usearch_qf_results/ -m usearch --word_length 64 --db_filepath ~/Desktop/ubuntu_64bit_shared/UNITE/sh_refs_qiime_ver6_dynamic_19.12.2013.fasta  -s 0.97 --de_novo_chimera_detection False --de_novo_chimera_detection False

pick_otus.py -i $fastq_filename.fastaqual/$fastq_filename.fna -m usearch --word_length 64 --db_filepath ~/Desktop/ubuntu_64bit_shared/UNITE/sh_refs_qiime_ver6_dynamic_19.12.2013.fasta -o usearch_qf_results/ -s 0.97 -t --suppress_reference_chimera_detection # -t collapsing identical sequences

# uclust, clust to ref. -r --uclust_otu_id_prefix qiime_otu_
pick_otus.py -i $fastq_filename.fna -o uclust/ -r ~/Desktop/ubuntu_64bit_shared/UNITE/sh_refs_qiime_ver6_dynamic_19.12.2013.fasta -m uclust_ref --uclust_otu_id_prefix qiime_otu_

# pick_open_reference_otus.py -i $fastq_filename.fastaqual/$fastq_filename.fna -r ~/Desktop/ubuntu_64bit_shared/UNITE/sh_refs_qiime_ver6_dynamic_19.12.2013.fasta -o uclust/ -s 0.97 --suppress_align_and_tree --suppress_taxonomy_assignment

--suppress_align_and_tree

# otu.name
X=$(basename $(ls usearch_qf_results/*otus.txt))

# Pick_rep_set.py
pick_rep_set.py -i usearch_qf_results/$X -f ../P11-1977_S24_L001_R1_001.fastaqual/$fastq_filename.fna -o $fastq_filename.rep.fna

# assign_taxonomy.py #
assign_taxonomy.py -i $fastq_filename.rep.fna -m mothur -c 0.85 -r ~/Desktop/ubuntu_64bit_shared/UNITE/sh_refs_qiime_ver6_dynamic_19.12.2013.fasta -t ~/Desktop/ubuntu_64bit_shared/UNITE/sh_taxonomy_qiime_ver6_dynamic_19.12.2013.txt

# make otu biom table
make_otu_table.py -i usearch_qf_results/$X -t mothur_assigned_taxonomy/$fastq_filename.rep_tax_assignments.txt -o $fastq_filename.biom

# convert biom to otu table

sudo biom convert -i $fastq_filename.biom -o $fastq_filename.tab -b # no taxonomy
sudo biom convert -i $fastq_filename.biom -o $fastq_filename.w.tax.tab -b --header-key taxonomy # with taxonomy

sed -e "1d" -e "s/#//g" BK.2samples.trime.w.tax.tab > BK.2samples.trime.w.tax.R.tab

echo "export RDP_JAR_PATH=$HOME/rdp_classifier_2.2/rdp_classifier-2.2.jar" >> $HOME/.bashrc
source $HOME/.bashrc

### prepare for stats
sed -e ':a;N;$!ba;s/\n/ /g' -e "s/ /,/g" -e "s/>/\n/g" -e "/^$/d" P11-1977_S24_L001_R1_001.qual > P11-1977_S24_L001_R1_001.qual.csv
sed -e ':a;N;$!ba;s/\n/ /g' -e "s/ /,/g" -e "s/>/\n/g" -e "/^$/d" P11-1977_S24_L001_R2_001.qual > P11-1977_S24_L001_R2_001.qual.csv
sed -e ':a;N;$!ba;s/\n/ /g' -e "s/ /,/g" -e "s/>/\n/g" -e "/^$/d" BK407W17OT_S4_L001_R1_001.qual > BK407W17OT_S4_L001_R1_001.qual.csv
sed -e ':a;N;$!ba;s/\n/ /g' -e "s/ /,/g" -e "s/>/\n/g" -e "/^$/d" BK407W17OT_S4_L001_R2_001.qual > BK407W17OT_S4_L001_R2_001.qual.csv



