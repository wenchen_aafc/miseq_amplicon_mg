#!/usr/bin/env perl

package OTU;

use strict;
use warnings;

use File::Basename;
use File::Copy;

sub pick_otu_rep {
  my $method = shift;
  my $similarity = shift;
  my $size = shift;
  my $max_memory = shift; #cdhit only
  
  #for file name purposes
  my $ITS = shift;
  my $prefix = shift;
  
  my $input_file = "$prefix.qiime.sorted$ITS.fasta";
  $input_file =~ s/\.NULL\./\./g;
  my $basename = basename($input_file, ".fasta");
  
  my $out_dir = "$method\_OTU_results";
  
  print "Picking representative sequences.";
  
  if ($method eq "usearch") {
    system ("pick_otus.py -i $input_file -o $out_dir -m $method -s $similarity -g $size --suppress_reference_chimera_detection --suppress_de_novo_chimera_detection");
    
    system ("pick_rep_set.py -i $out_dir/$basename\_otus.txt -f $input_file -o $basename.$method.rep.fasta");
  }
  if ($method eq "cdhit") {
    mkdir $out_dir;
    system ("cd-hit -i $input_file -o $out_dir/$basename -c $similarity -M $max_memory -g 1 -d 999");
    
    {#reformatting the files to match the input for the scripts afterwards
      open (INMAP, "<$out_dir/$basename.clstr") or die $!;

      my %clusters;
      my $OTU_id;
      while (my $line = <INMAP>) {
        if ($line =~ m/Cluster/) {
          my $cluster_id = (split (' ', $line))[1];
          $OTU_id = $cluster_id;
        } else {
          if ($line =~ m/\*/) {
            my $tmp = (split ('>', $line))[1];
            my $seq = (split ('\.\.\.', $tmp))[0];
            push (@{$clusters{$OTU_id}}, $seq."-");
          } else {
            my $tmp = (split ('>', $line))[1];
            my $seq = (split ('\.\.\.', $tmp))[0];
            push (@{$clusters{$OTU_id}}, $seq);
          }
        }
      }

      close INMAP;

      open (OUTMAP, ">$out_dir/$basename\_otus.txt") or die $!;

      my @keys = keys %clusters;
      foreach my $key (@keys) {
        print OUTMAP "$key";
        foreach my $seq (@{$clusters{$key}}) {
          if ($seq =~ m/-/) {
            $seq =~ s/-//;
            print OUTMAP "\t$seq";
          }
        }
        foreach my $seq (@{$clusters{$key}}) {
          print OUTMAP "\t$seq";
        }
        print OUTMAP "\n";
      }


      close OUTMAP;

      open (REP, "<$out_dir/$basename") or die $!;
      open (OTU, "<$out_dir/$basename\_otus.txt") or die $!;
      open (OUTREP, ">$basename.$method.rep.fasta") or die $!;

      my %rep_seqs;
      while (my $line = <OTU>) {
        my @fields = split (/\t/, $line);
        chomp $fields[1];
        $rep_seqs{$fields[1]} = $fields[0];
      }
      while (my $line = <REP>) {
        if ($line =~ m/>/) {
          $line =~ s/>//;
          chomp $line;
          print OUTREP ">$rep_seqs{$line} $line\n";
        } else {
          print OUTREP $line;
        }
      }
      close REP;
      close OTU;
      close OUTREP;
    }
  }
  return $basename;
}

sub assign_taxon {
  my $method = shift;
  my $ref_seqs = shift;
  my $id_tax = shift;
  my $confidence = shift;
  my $max_memory = shift;
  my $threads = shift;
  
  my $basename = shift;
  
  print "Assigning taxonomy.";
  
  if ($method eq "mothur") {
    system ("assign_taxonomy.py -i $basename.rep.fasta -m $method -r $ref_seqs -t $id_tax -c $confidence");
  } 
  elsif ($method eq "rdp") {
    system ("assign_taxonomy.py -i $basename.rep.fasta -m $method -r $ref_seqs -t $id_tax -c $confidence --rdp_max_memory $max_memory");
  }
}

sub blast_related {
  my $method = shift;
  my $otu_method = shift;
  my $blastdb = shift;
  my $evalue = shift;
  my $num_seqs = shift;
  my $threads = shift;
  my $basename = shift;
  
  my $OTU_dir = "$otu_method\_OTU_results";
  
  if ($method eq "MEGAN"){
    system ("blastn -query $basename.rep.fasta -db $blastdb -evalue $evalue -task blastn -out $basename.rep.blast -dust no -outfmt 7 -num_threads $threads -max_target_seqs $num_seqs");
    system ("MEGAN_ANNOTATION.py -I . -U rep.blast -O .");
    system ("MEGAN_TAXONOMY_EXPORT.py -I . -O .");
    system ("perl MEGAN2otu.pl $basename.taxonomy.export.txt $otu_method");
    
    system ("make_otu_table.py -i $OTU_dir/$basename\_otus.txt -t $basename.$otu_method.rep_tax_assignments.txt -o $basename\_otus.biom");
    system ("convert_biom.py -i $basename\_otus.biom -o $basename\_otu.w.tax.tab -b --header_key=taxonomy");
  }
}

sub otu_table {
  my $otu_method = shift;
  my $tax_method = shift;
  my $basename = shift;
  
  my $OTU_dir = "$otu_method\_OTU_results";
  
  print "Making OTU table.";
  
  system ("make_otu_table.py -i $OTU_dir/$basename\_otus.txt -t $tax_method\_assigned_taxonomy/$basename.$otu_method.rep_tax_assignments.txt -o $basename\_otus.biom");
  
  system ("convert_biom.py -i $basename\_otus.biom -o $basename\_otu.w.tax.tab -b --header_key=taxonomy");
}

sub ITS {
  my $ITS = shift;
  my $prefix = shift;
  my $threads = shift;
  my $kingdoms = shift;
  
  my $input_file = "$prefix.qiime.sorted.fasta";
  my $basename = basename ($input_file, ".fasta");
  
  my @non_empty;
  
  mkdir "its_extracted";
  
  print "Extracting ITS from $input_file sequences.\n";
  system ("~/ITSx_1.0.11/ITSx -i $input_file -o its_extracted/$basename -cpu $threads --preserve T -t $kingdoms");
  
  if ($ITS eq "BOTH") {
    if ( ! -z "its_extracted/$basename.ITS2.fasta") {
      push (@non_empty, "ITS2");
      mkdir "ITS2";
      move ("its_extracted/$basename.ITS2.fasta", "./ITS2/");
    } else {
      print "ITS2 region does not exist in these sequences.";
    }
    if ( ! -z "its_extracted/$basename.ITS1.fasta") {
      push (@non_empty, "ITS1");
      mkdir "ITS1";
      move ("its_extracted/$basename.ITS1.fasta", "./ITS1/");
    } else {
      print "ITS1 region does not exist in these sequences.";
    }
    return @non_empty;
  } else {
    if ( ! -z "its_extracted/$basename.$ITS.fasta"){
      move ("its_extracted/$basename.$ITS.fasta", ".");
    } else {
      die "The $ITS region does not exist in these sequences.";
    }
  }
}

sub closed_otu {
  my $ref_seqs = shift;
  my $id_tax = shift;
  my $threads = shift;
  my $ITS = shift;
  my $prefix = shift;
  
  my $input_file = "$prefix.qiime.sorted$ITS.fasta";
  $input_file =~ s/\.\./\./g;
  my $basename = basename ($input_file, ".fasta");
  
  my $out_dir = "otus_w_tax";
  
  print "Closed-reference clustering and assigning taxon.\n";
  system ("pick_closed_reference_otus.py -i $input_file -o $out_dir -r $ref_seqs -t $id_tax -a True -O $threads");
  
  print "Converting biom.\n";
  system ("convert_biom.py -i $out_dir/otu_table.biom -o $basename\_otu.w.tax.tab -b --header_key=taxonomy");
}
1;
