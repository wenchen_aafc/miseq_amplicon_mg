#!/usr/bin/env perl

package Qual_Trim;

use strict;
use warnings;

use File::Basename;
use File::Copy;
use Cwd;

sub trim {
  my $method = shift;  #trimming method
  my $threshold = shift;  #quality threshold
  my $windowsize = shift;  #mothur only
  my $threads = shift;  #mothur only
  my $minlen = shift;  #usearch only
  my $usearch = shift;  #usearch only
  
  mkdir "raw_data"; #new directory to store the raw data
  
  my @files = grep ($_ =~ m/fastq$/, <*>);
  
  if ($method eq "usearch") {
    foreach my $file (@files) {
      my $basename = basename($file, ".fastq");
      system ("$usearch -fastq_filter $file -fastq_truncqual $threshold -fastq_minlen $minlen -fastqout $basename.trim.fastq");
      system ("fastqc $file");      
      #move ($file, "raw_data");
      unlink $file;
      system ("fastqc $basename.trim.fastq");
      system ("mv *_fastqc* fast_qc");
    }
  } elsif ($method eq "mothur") {
    mkdir "mothur_trim_logs";
    mkdir "scraps";
    
    foreach my $file (@files) {
      my $basename = basename($file, ".fastq");
      
      open (MOTHUR, ">mothur.bat");
      print MOTHUR "fastq.info(fastq=$file)\n";
      print MOTHUR "trim.seqs(fasta=$basename.fasta, qfile=$basename.qual, maxambig=0, qwindowsize=$windowsize, qwindowaverage=$threshold, processors=$threads)\n";
      print MOTHUR "make.fastq(fasta=$basename.trim.fasta, qfile=$basename.trim.qual)\n";
      close MOTHUR;
      system ("mothur mothur.bat");
      system ("rm mothur.bat");
      
      system ("fastqc $file");      
      #move ($file, "raw_data");
      unlink $file;
      system ("fastqc $basename.trim.fastq");
      system ("mv *_fastqc* fast_qc");
      
      my @scraps = grep ($_ =~ $basename && $_ =~ ".scrap", <*>);
      foreach my $scrap (@scraps) {
        move ($scrap, "scraps");
      }
      
      #deletes extra files made by Mothur
      my @intermediate = grep ($_ =~ $basename && ($_ =~ ".fasta" || $_ =~ ".qual"), <*>);
      unlink @intermediate;
      
      #moving mothur logfiles to a new directory
      my @logs = grep ($_ =~ "mothur." && $_ =~ ".logfile", <*>);
      foreach my $log (@logs) {
        move ($log, "mothur_trim_logs/mothur.$basename.logfile");
      }
    }
  }
}

sub filter {
  my @forward = sort (grep ($_ =~ m/fastq$/ && $_ =~ m/R1/ && $_ =~ m/trim/, <*>));
  my @reverse = sort (grep ($_ =~ m/fastq$/ && $_ =~ m/R2/ && $_ =~ m/trim/, <*>));

  my $time = localtime;
  print "Starting time: $time\n";
  
  for (my $i=0; $i < scalar (@forward); $i ++) {
    my $basename = join ('_', (split ('_', $forward[$i]))[0..2]);
    
    open (FORWARD, "<$forward[$i]") or die $!;
    open (REVERSE, "<$reverse[$i]") or die $!;
    
    my @R1_ID;
    my @R2_ID;
    
    my %R1;
    my %R2;
    
    #assuming all fastq files have the same 4 lines per sequence format
    #for forward reads
    while (my $line = <FORWARD>.<FORWARD>.<FORWARD>.<FORWARD>) {
      my @sequence = split(/\n/, $line, 2);
      my @identifier = split(' ', $sequence[0]);
      $R1{$identifier[0]} = $sequence[1];
    }
    #for reverse reads
    while (my $line = <REVERSE>.<REVERSE>.<REVERSE>.<REVERSE>) {
      my @sequence = split(/\n/, $line, 2);
      my @identifier = split(' ', $sequence[0]);
      $R2{$identifier[0]} = $sequence[1];
    }
  
    close FORWARD;
    close REVERSE;
  
    #output files for common and unique sequences for forward/reverse reads
    open (R1_UNIQ, ">$basename"."_R1.uniq.trim.fastq");
    open (R2_UNIQ, ">$basename"."_R2.uniq.trim.fastq");
    open (R1_COMM, ">$basename"."_R1.comm.trim.fastq");
    open (R2_COMM, ">$basename"."_R2.comm.trim.fastq");
  
    @R1_ID = keys %R1;
    @R2_ID = keys %R2;
    
    foreach my $key (@R1_ID) {
      if (exists($R2{$key})) {
        print R1_COMM "$key\n$R1{$key}";
        print R2_COMM "$key\n$R2{$key}";
        delete $R2{$key};
      }
      else {
        print R1_UNIQ "$key\n$R1{$key}";
      }
    }
  
    @R2_ID = keys %R2;
    foreach my $key (@R2_ID) {
      print R2_UNIQ "$key\n$R2{$key}";
    }
  
    $time = localtime;
    print "Sample $basename filtered at $time\n";
  
    close R1_UNIQ;
    close R2_UNIQ;
    close R1_COMM;
    close R2_COMM;
  }
  mkdir "unjoined_forward";
  mkdir "unjoined_reverse";
  
  my @files = <*>;
  foreach my $file (@files) {
    if ($file =~ m/uniq/ && $file =~ m/R1/) {
      move ($file, "unjoined_forward");
    } elsif ($file =~ m/uniq/ && $file =~ m/R2/) {
      move ($file, "unjoined_reverse");
    }
  }
}
1;
