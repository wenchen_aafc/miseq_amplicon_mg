#!/usr/bin/env perl

use strict;
use warnings;
#cat rdp_assigned_taxonomy/ITS2_rep_set_tax_assignments.txt | iconv -f utf8 -t ascii//TRANSLIT//IGNORE > ITS2_rep_set_tax_assignments.noacc.txt
use Bio::SeqIO;
use Getopt::Long;
use File::Basename;
use File::Copy;
use FindBin;
use Cwd;

#modules
use lib "$FindBin::Bin"; #Path to the modules
use OTU;
use Convert;
use Qual_Trim;
use LogFile;

#variables for options
my $dir;
my $outdir = getcwd();

my $help = 0;
#my $extract = 0;  #default is no extracting
my $singletons = "TRUE";  #default is to process all the singletons
my $closed_ref = "FALSE";  #default is de novo OTU picking
my $join = "TRUE";    
my $pairedend = "TRUE";  #for non-paired ended data
my $ITS = "";
my $trim_m = "usearch";  #default trim method
my $otu_m = "cdhit";  #default clustering method
my $tax_m = "mothur";  #default assign taxonomy method
my $kingdoms = "F";

my $e_value = 10;
my $threads = 1;
my $min_overlap = 50;
my $perc_max_diff = 1;
my $minlen = 100;
my $qthreshold = 20;
my $qwindowsize = 10;
my $OTU_size = 1;
my $similarity = 0.97;
my $confidence = 0.50;
my $num_alignments = 50;
my $max_memory = 12000;  #in MB (used for both cd-hit and rdp)

my $usearch = "~/usearch7.0.1090_i86linux32"; #usearch version
my $blastdb = "/isilon/biodiversity/reference/blastdb/fungal_its/fno.fasta";
my $ref_seqs = "/isilon/biodiversity/reference/unite/its_12_11_otus/rep_set/99_otus.fasta";
my $id_tax = "/isilon/biodiversity/reference/unite/its_12_11_otus/taxonomy/99_otu_taxonomy.txt";
my $config_path;

## CONFIG & SETUP
&setup;

#options
sub setup{
  GetOptions ( 
  "-o=s" => \$outdir, 
  "-i=s" => \$dir, 
  "--closed!" => \$closed_ref, 
  "--reference=s" => \$ref_seqs, 
  "--taxonomy=s" => \$id_tax, 
  "--ITS=s" => \$ITS, 
  "--threads=i" => \$threads, 
  "--trim_method=s" => \$trim_m, 
  "--help!" => \$help, 
#  "--extract!" => \$extract, 
  "--min_overlap=i" => \$min_overlap, 
  "--perc_max_diff=i" => \$perc_max_diff, 
  "--minlen=i" => \$minlen, 
  "--qthreshold=i" => \$qthreshold, 
  "--qwindowsize=i" => \$qwindowsize, 
  "--OTU_size=i" => \$OTU_size, 
  "--similarity=s" => \$similarity, 
  "--singletons=s" => \$singletons, 
  "--join=s" => \$join, 
  "--otu_method=s" => \$otu_m, 
  "--tax_method=s" => \$tax_m,
  "--max_memory=i" => \$max_memory, 
  "--confidence=i" => \$confidence, 
  "--evalue" => \$e_value, 
  "--blastdb" => \$blastdb, 
  "--paired_end=s" => \$pairedend, 
  "-c=s" =>\$config_path, 
  "--num_alignments=s" =>\$num_alignments) 
};
if(defined $config_path){
    config();
  #reads in config file
  #check for config file existence 
   unless(-e "$config_path") {
     die "The specified config file: $config_path does not exist.\n";
  }   
}

#help page of the pipeline
if ($help) {
  system ("perldoc /home/AAFC-AAC/zhuk/miseq_amplicon/scripts/MSAMP.pl");
  exit
}


#get full command line string
my $cmd_str= qx/ps -o args $$/;
$cmd_str=~s/COMMAND\n//g;
chomp $cmd_str;
LogFile::addToLog("Runtime","Command Line",$cmd_str);

my $DATE=POSIX::strftime('%Y_%B%d_%H_%M',localtime()); 
my $HR_DATE=POSIX::strftime('%B %d, %Y %H:%M',localtime());            
#gets the date into the format YEAR/MONTH/HOUR/MINUTE

LogFile::addToLog('Date','Date and time',$HR_DATE);

my $output_directory = "$DATE.Output"; #create output directory based on time

my @runtime_array;
my $runtime_hash = {};
my $start_time = time();
my $st = time();
my $et;

#getting the absolute directory paths for input and output directory
my $temp = getcwd();
chdir $dir or die "Please give a valid input directory path.";
$dir = getcwd();
chdir $temp;
chdir $outdir or die "Please give a valid output directory path.";
$outdir = getcwd();

LogFile::addToLog("IO","Input Folder",$dir);
LogFile::addToLog("IO","Output Folder",$outdir);

mkdir "temp";
mkdir "fast_qc";

###############################################################################
# Step1: decompress fastq.gz files or copy decompressed fastq files to $outdir
###############################################################################

#option for extracting the files if not extracted already
#if ($extract) {
#if (
Convert::unzip ($dir, $outdir);
timer ("Unzip");
#} elsif ($dir ne $outdir) {
#Convert::copy_files ($dir, $outdir);
#timer ("Copy");
#}
sub unzip {
  my $dir = shift;
  my $outdir = shift;
  opendir (DIR, $dir) or die $!;
  my @files = grep ($_ =~ m/\Qfastq.gz\E/ && $_ !~ m/Undetermined/, readdir(DIR));
  closedir (DIR);
  #unzips all the files in the array @files
  if ( scalar @files == 0 ) {
    print "\n\nNo fastq.gz files were found in ", $dir, ", will not do decompression\n\n";
    copy_files ($dir, $outdir);
  } else {
    foreach my $file (@files) {
    my $basename = basename($file, ".gz");    
    print "Unzipping $file\n";
    system ("gunzip -c $dir/$file > $outdir/$basename");
   }
}

sub copy_files {
  my $dir = shift;
  my $outdir = shift;
  opendir (DIR, $dir) or die $!;
  my @files = grep ($_ =~ m/\Q.fastq\E/ && $_ !~ m/\Qfastq.gz\E/ && $_ !~ m/Undetermined/, readdir(DIR));
  closedir (DIR);
  foreach my $file (@files) {
    copy ("$dir/$file", "$outdir/$file");
  }
}

###############################################################################
# Step 1 finished
###############################################################################



##########################################################################
# Step 2: removing any potential characters in the file names that could cause a problem downstream
Convert::name_correction ();
###############################################################################
# Step 2 finished
###############################################################################

Qual_Trim::trim ($trim_m, $qthreshold, $qwindowsize, $threads, $minlen, $usearch);
timer ("Trim");

if ($pairedend eq "FALSE") {
  Convert::fastq2fasta ();
  timer ("Convert to FASTA");
  Convert::qiime_format ();
  timer ("Convert to QIIME format");
  Convert::cat ("all.trim");
  timer ("Merging files");
  
  otu_table ("all.trim");
  
} else {
  if ($join eq "FALSE") {
    { #for forward reads
      mkdir "forward";
      system ("mv *_R1_* forward/");
      chdir "forward";
      
      Convert::fastq2fasta ();
      timer ("Convert to FASTA (forward)");
      Convert::qiime_format ();
      timer ("Convert to QIIME format (forward)");
      Convert::cat ("all.forward");
      timer ("Merging files (forward)");
  
      otu_table ("all.forward");
  
      chdir $outdir;
    }
    { #for reverse reads
      mkdir "reverse";
      system ("mv *_R2_* reverse/");
      chdir "reverse";
      
      Convert::reverse_complement ();
      timer ("Reverse complement");
      Convert::qiime_format ();
      timer ("Convert to QIIME format (reverse)");
      Convert::cat ("all.reverse");
      timer ("Merging files (reverse)");
  
      otu_table ("all.reverse");
  
      chdir $outdir;
    }
  } else {
    Qual_Trim::filter ();
    timer ("Filtering forward/reverse files");
    Convert::join_seqs ($min_overlap, $perc_max_diff);
    timer ("Joining");
    Convert::fastq2fasta ();
    timer ("Convert to FASTA");
    Convert::qiime_format ();
    timer ("Convert to QIIME format");
    Convert::cat ("all.trim.join");
    timer ("Merging files");
  
    if ($singletons eq "FALSE") {

      otu_table ("all.trim.join");

    } else {
      { #for forward singletons
        chdir "unjoined_forward";
    
        mkdir "temp";
    
        Convert::fastqc_after_join ("forward", $outdir);
        Convert::fastq2fasta ();
        timer ("Convert to FASTA (forward)");
        Convert::qiime_format ();
        timer ("Convert to QIIME format (forward)");
        Convert::cat ("all.R1");
        timer ("Merging files (forward)");
    
        chdir $outdir;
    
        mkdir "forward_and_joined";
        system ("cat unjoined_forward/all.R1.qiime.fasta all.trim.join.qiime.fasta >> forward_and_joined/forward.joined.qiime.dup.fasta");
        chdir "forward_and_joined";
        
        Convert::qiime_re_format ();
        otu_table ("forward.joined");
    
        chdir $outdir;
      }
      { #for reverse singletons
        chdir "unjoined_reverse";
    
        mkdir "temp";
    
        Convert::fastqc_after_join ("reverse", $outdir);
        Convert::reverse_complement ();
        timer ("Reverse complement (reverse)");
        Convert::qiime_format ();
        timer ("Conver to QIIME format (reverse)");
        Convert::cat ("all.R2");
        timer ("Merging files (reverse)");
    
        chdir $outdir;
    
        mkdir "reverse_and_joined";
        system ("cat unjoined_reverse/all.R2.qiime.fasta all.trim.join.qiime.fasta >> reverse_and_joined/reverse.joined.qiime.dup.fasta");
        chdir "reverse_and_joined";
        
        Convert::qiime_re_format ();
        otu_table ("reverse.joined");
    
        chdir $outdir;
      }
    }
  }
}

my $end_time = time();
my $run_time = $end_time - $start_time;
print "Finished running. Took a total of $run_time seconds.\n";

push (@runtime_array, {'Total'=> "$run_time seconds"});
push (@runtime_array, $runtime_hash);
LogFile::addToLog("Runtime","Running time",\@runtime_array);  
LogFile::writeLog("$DATE.yaml");

sub otu_table {
  my $prefix = shift;
  my $basename = "";
  my @non_empty = ();
  
  Convert::length_sort ($usearch, $minlen, $prefix);
  timer ("Sorting ($prefix)");

  if ($ITS ne "BOTH") {
    OTU::ITS ($ITS, $prefix, $threads, $kingdoms) if ($ITS ne "NULL");
    timer ("ITS extraction ($prefix)");
    
    if ($closed_ref eq "TRUE") {
      OTU::closed_otu ($ref_seqs, $id_tax, $threads, ".$ITS", $prefix);
      timer ("Closed reference ($prefix)");
    } else {
      $basename = OTU::pick_otu_rep ($otu_m, $similarity, $OTU_size, $max_memory, ".$ITS", $prefix);
      timer ("Picking representative ($prefix)");
      OTU::assign_taxon ($tax_m, $ref_seqs, $id_tax, $confidence, $max_memory, $threads, "$basename.$otu_m");
      timer ("Assigning taxonomy ($prefix)");
      OTU::otu_table ($otu_m, $tax_m, $basename);
      timer ("Creating OTU table ($prefix)");
    }
  } else {
    @non_empty = OTU::ITS ($ITS, $prefix, $threads, $kingdoms);
    timer ("ITS extraction ($prefix)");
    #print @non_empty, "\n";
    (@non_empty != 0) or die ("There are no ITS regions in your sequences.\nPlease rerun the pipeline without the --ITS option.");
    
    foreach my $region (@non_empty) {
      chdir $region;
    
      if ($closed_ref eq "TRUE") {
        OTU::closed_otu ($ref_seqs, $id_tax, $threads, ".$region", $prefix);
        timer ("Closed reference ($prefix)");
      } else {
        $basename = OTU::pick_otu_rep ($otu_m, $similarity, $OTU_size, $max_memory, ".$region", $prefix);
        timer ("Picking representative ($prefix)");
        OTU::assign_taxon ($tax_m, $ref_seqs, $id_tax, $confidence, $max_memory, $threads, "$basename.$otu_m");
        timer ("Assigning taxonomy ($prefix)");
        OTU::otu_table ($otu_m, $tax_m, $basename);
        timer ("Creating OTU table ($prefix)");
      }
      chdir $outdir;
    }
  }
}

sub timer {
  my $process = shift;
  $et = time();
  $runtime_hash->{$process}=join(" ", $et-$st, "seconds");
  $st = $et;
}

sub config{
  #this sub reads from the configuration file and checks if everything is defined.
  open (CONF,"$config_path") or die;
  while (<CONF>){
    my $line = $_;
    chomp $line;
    $line =~ s/\R//g;
    my @split;
    if ($line =~ m/^THREAD_COUNT/){
      @split = split(/=/,$line);
      $threads = $split[1];    
    }elsif($line =~ m/^MAX_MEMORY/){
      @split = split(/=/,$line);
      $max_memory = $split[1];
    }elsif($line =~ m/^TRIM_METHOD/){
      @split = split(/=/,$line);
      $trim_m = $split[1];
    }elsif($line =~m/^MINIMUM_LENGTH/){
      @split = split(/=/,$line);
      $minlen = $split[1];
    }elsif($line =~m/^QUALITY_THRESHOLD/){
      @split = split(/=/,$line);
      $qthreshold = $split[1];
    }elsif($line =~m/^WINDOW_SIZE/){
      @split = split(/=/,$line);
      $qwindowsize = $split[1];
    }elsif($line =~m/^JOIN/){
      @split = split(/=/,$line);
      $join = $split[1];
    }elsif($line =~m/^MINIMUM_OVERLAP/){
      @split = split(/=/,$line);
      $min_overlap = $split[1];
    }elsif($line =~m/^MAXIMUM_PERCENTAGE_DIFFERENCE/){
      @split = split(/=/,$line);
      $perc_max_diff = $split[1];
    }elsif($line =~m/^SINGLETONS/){
      @split = split(/=/,$line);
      $singletons = $split[1];
    }elsif($line =~m/^ITS_EXTRACTION/){
      @split = split(/=/,$line);
      $ITS = $split[1];
    }elsif($line =~m/^OTU_METHOD/){
      @split = split(/=/,$line);
      $otu_m = $split[1];
    }elsif($line =~m/^SIMILARITY/){
      @split = split(/=/,$line);
      $similarity = $split[1];
    }elsif($line =~m/^OTU_SIZE/){
      @split = split(/=/,$line);
      $OTU_size = $split[1];
    }elsif($line =~m/^TAX_METHOD/){
      @split = split(/=/,$line);
      $tax_m = $split[1];
    }elsif($line =~m/^CONFIDENCE/){
      @split = split(/=/,$line);
      $confidence = $split[1];
    }elsif($line =~m/^EVALUE/){
      @split = split(/=/,$line);
      $e_value = $split[1];
    }elsif($line =~m/^NUM_ALIGNMENTS/){
      @split = split(/=/,$line);
      $num_alignments = $split[1];
    }elsif($line =~m/^CLOSED_REFERENCE/){
      @split = split(/=/,$line);
      $closed_ref = $split[1];
    }elsif($line =~m/^PAIRED_END/){
      @split = split(/=/,$line);
      $pairedend = $split[1];
    }elsif($line =~m/^KINGDOMS/){
      @split = split(/=/,$line);
      $kingdoms = $split[1];
    }
    LogFile::addToLog("Options","Configuration",{'Path' => $config_path,
                   'Threads' => $threads});

  }
  
  #check if all the values are defined

  close CONF;
}


__END__

=head1 NAME

=over 12

=item B<M>iB<S>eq B<A>mplicon B<M>etagenomics B<P>ipeline (MSAMP)

=back

=head1 SYNOPSIS

perl MSAMP.pl [-i] [Options] [--help]

=head1 DESCRIPTION

The MiSeq Amplicon Metagenomics Pipeline process sequences generated by MiSeq sequencing machines using data gathered from various biodiversity surveys. 

To run, you must give a path to the input raw data folder. If the raw data files are not zipped (FASTQ files), they will be copied to output folder before running the pipeline. Output folder will be your current working directory if you do not provide one.

=head1 OUTPUTS

Stuff...

=head1 OPTIONS

=over 12

=item B<--help>

Program information and usage instructions. Detailed information and instructions are in the manual

=item B<-i>

Specify the path to the directory of the input data files [REQUIRED]

=item B<-o>

Specify the path of the output directory [Default: current working directory]

=item B<--closed>

Closed reference otu picking [Default: FALSE]

=item B<--reference>

Specify the path to the reference sequences for assigning taxonomy and/or closed reference otu picking

=item B<--taxonomy>

Specify the path to tab-delimited file mapping sequences to assigned taxonomy and/or closed reference otu picking

=item B<--ITS>

Specify which region of ITS you want to extract, must be one of ITS1, ITS2 [Default: none]

=item B<--threads> [N]

Specify how many processors/threads you want to use to run the multi-threaded portions of this pipeline. N processors will be used [Default: 1]

=item B<--trim_method>

Sequence trimming method, must be one of usearch, mothur [Default: usearch]

=item B<--extract>

For if the raw data file(s) need to be decompressed [Default: FALSE]

=item B<--min_overlap> [N]

N-minimum overlap required when joining forward and reverse sequence [Default: 50]

=item B<--perc_max_diff> [N]

N-percent B<MAXIMUM> difference for the overlapping region when joining [Default: 1]E<10> E<8>(i.e. 1% maximum difference = 99% minimum similarity)

=item B<--minlen> [N]

Specify a minimum sequence length to keep. Lengths of sequences will be at least N bp long after sorting [Default: 100]

=item B<--qthreshold> [N]

B<mothur>: Set a minimum average quality score allowed over a window [Default: 20]E<10> E<8>B<usearch>: Truncate the sequence at the first position having quality score <= N [Default: 20]

=item B<--qwindowsize> [N]

Set a minimum average quality score allowed over a window, only used for mothur [Default: 10]

=item B<--OTU_size>

Minimum cluster size for size filtering with usearch [Default: 1]

=item B<--similarity> [N]

Sequence similarity threshold, only used for usearch, cd-hit [Default: 0.97]

=item B<--nosingletons>

Does not process unjoined forward/reverse reads [Default: FALSE]

=item B<--otu_method>

Clustering method, must be one of usearch, cdhit [Default: usearch]

=item B<--tax_method>

Taxonomy assignment method, must be one of mothur, rdp, blast [Default: mothur]

=item B<--max_memory> [N]

Maximum memory allocation, in MB, only used for cdhit, rdp [Default: 12000]

=item B<--confidence>

Minimum confidence to record an taxonomy assignment, only used for rdp and mothur methods [Default: 0.5]

=item B<--evalue> [N]

Expectation value threshold for saving hits

=item B<--blastdb>

Specify the path to the blast database when it is needed


