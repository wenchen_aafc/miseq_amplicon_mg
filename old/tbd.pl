#!/usr/bin/env perl

package sub_1;

use strict;
use warnings;

use Bio::SeqIO;
use File::Basename;
use File::Copy;
use List::MoreUtils qw(uniq);
use Cwd;

my $fasta= "rep.fasta";
my $reference="/home/AAFC-AAC/chenw/references/UNITE/sh_refs_qiime_ver7_99_s_02.03.2015.genbank_O_20150530.qiime.mothur.fasta";
my $threads = 18;

chimera_uchime($fasta, $reference, $threads);

sub chimera_uchime {
  my $fasta=shift;
  my $reference=shift;
  my $threads=shift;

  my $basename=basename($fasta, ".fasta");
  open (MOTHUR, ">mothur.bat");
  print MOTHUR "chimera.uchime(fasta=$fasta, reference=$reference, processors=$threads)\n";
  close MOTHUR;
  system("mothur mothur.bat");
  my $count_total=0;
  open (CHIM, "<$basename.uchime.chimeras");
  my $ID_non_chimera={};
  my $count_chim=0;
  my $count_nonchim=0;
  while (<CHIM>) {
    $count_total++;
    chomp $_;
    my @fields = split /\t/, $_;
    my $otu_id = $fields[1];
    my $chimera = $fields[(scalar @fields)-1];
    if ($chimera eq "N") {
      $count_nonchim++;
      $ID_non_chimera->{$otu_id} = $chimera;
      #print $otu_id, "\t", $chimera, "\n";
    } elsif ( $chimera eq "Y") {
      $count_chim++;
    }
  }
  print "$fasta: $count_total sequences\n$count_chim chimera sequences\n$count_nonchim non-chimera sequences\n\n";
  my $in = Bio::SeqIO->new(-file=>"<$fasta", -format=>"fasta");
  my $out = Bio::SeqIO->new(-file=>">$basename.nonchimera.fasta", -format=>"fasta");
  #open(OUT, ">$basename.nonchimera.fasta");
  my $count_out=0;
  while (my $seq=$in->next_seq()) {
    my $seqid=$seq->id;
    #print $seqid, "\n";
    if ( exists $ID_non_chimera->{$seqid} ) {
      $count_out++;
      print $seqid, "\t", $ID_non_chimera->{$seqid}, "\n";
      $out->write_seq($seq)
      #print OUT ">", $seq->id, " ", $seq->desc, "\n", $seq->seq, "\n";
    }    
  }
  print "$count_out non chimera sequences are written to $basename.nonchimera.fasta\n\n";
}
  
