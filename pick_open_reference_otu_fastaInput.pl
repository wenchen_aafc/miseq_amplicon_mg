#!/usr/bin/env perl

use strict;
use warnings;
#cat rdp_assigned_taxonomy/ITS2_rep_set_tax_assignments.txt | iconv -f utf8 -t ascii//TRANSLIT//IGNORE > ITS2_rep_set_tax_assignments.noacc.txt
use Bio::SeqIO;
use Getopt::Long;
use File::Basename;
use File::Copy;
use File::Spec;
use FindBin;
use Cwd;

#modules
use lib "$FindBin::Bin"; #Path to the modules
use LogFile;
use sub_1;

#variables for options
my $dir;
my $outdir = getcwd();

my $help = 0;
#my $extract = 0;  #default is no extracting
my $otu_m = "cdhit";  #default clustering method in qiime
my $tax_m = "rdp";  #default assign taxonomy method
my $threads = 24;
my $similarity = 0.97;
my $confidence = 0.80;
my $max_memory = 80000;  #in MB (used for both cd-hit and rdp)

my $usearch = "~/usearch7.0.1090_i86linux32"; #usearch version
# id-to-taxonomy database formated to 
# mothur format: 1) ; at the end; 2) no space in lineage
# qiime mothur format: 1) NO ; at the end; 2) no space in lineage
# qiime rdp format: 1) NO ; at the end; 2) exactly 6-levels for each lineage
my $fasta;
my $ref_seqs;
my $id_tax;
my $ref_chimera;
my $amplicon;
my $mothur="/isilon/biodiversity/users/chenw_lab/AAFC_Training_Material/Intro_Metagenomics/mothur";
my $scripts_dir="/isilon/biodiversity/users/chenw_lab/Bitbucket/scripts_repo/Bio_tools/";




## CONFIG & SETUP
&setup;

#options
sub setup{
  GetOptions ( 
  "-f=s" => \$fasta, 
  "--amplicon=s" =>\$amplicon,
  "--threads=s" =>\$threads) 
};

#help page of the pipeline
if ($help) {
  system ("perldoc /home/AAFC-AAC/zhuk/miseq_amplicon/scripts/MSAMP.pl");
  exit
}

#get full command line string
my $cmd_str= qx/ps -o args $$/;
$cmd_str=~s/COMMAND\n//g;
chomp $cmd_str;
LogFile::addToLog("Runtime","Command Line",$cmd_str);

my $DATE=POSIX::strftime('%Y_%B%d_%H_%M',localtime()); 
my $HR_DATE=POSIX::strftime('%B %d, %Y %H:%M',localtime());            
#gets the date into the format YEAR/MONTH/HOUR/MINUTE

LogFile::addToLog('Date','Date and time',$HR_DATE);

#my $output_directory = "$DATE.Output"; #create output directory based on time

my @runtime_array;
my $runtime_hash = {};
my $start_time = time();
my $st = time();
my $et;

#getting the absolute directory paths for input and output directory
my $base;
my @suf;
if ( -z $fasta || !-e $fasta ) {
  die "Input fasta file is empty or not exist\n\n";
} else {
  @suf=(".fasta", ".fna", ".fa", ".fas");
  $base=basename($fasta, @suf);
}

if ( $amplicon=~ /ITS/ ) {
    $ref_seqs = "/isilon/biodiversity/users/chenw_lab/AAFC_Training_Material/Intro_Metagenomics/UNITE/UNITEv6_sh_97_s.fasta";
    $id_tax = "/isilon/biodiversity/users/chenw_lab/AAFC_Training_Material/Intro_Metagenomics/UNITE/UNITEv6_sh_97_s_rdp.ascii.txt";
    if ( $amplicon eq "ITS") {
       $ref_chimera="/isilon/biodiversity/users/chenw_lab/AAFC_Training_Material/Intro_Metagenomics//UNITE/uchime_reference_dataset_01.01.2016/uchime_reference_dataset_01.01.2016.fasta";
    } elsif ( $amplicon eq "ITS1") {
       $ref_chimera="/isilon/biodiversity/users/chenw_lab/AAFC_Training_Material/Intro_Metagenomics//UNITE/uchime_reference_dataset_01.01.2016/ITS1_ITS2_datasets/uchime_sh_refs_dynamic_develop_985_01.01.2016.ITS1.fasta";
    } elsif ( $amplicon eq "ITS2") {
       $ref_chimera="/isilon/biodiversity/users/chenw_lab/AAFC_Training_Material/Intro_Metagenomics//UNITE/uchime_reference_dataset_01.01.2016/ITS1_ITS2_datasets/uchime_sh_refs_dynamic_develop_985_01.01.2016.ITS2.fasta";
    } else {
       $ref_chimera="/isilon/biodiversity/users/chenw_lab/AAFC_Training_Material/Intro_Metagenomics//UNITE/uchime_reference_dataset_01.01.2016/uchime_reference_dataset_01.01.2016.fasta";
    }
} elsif ( $amplicon eq "16S" ) {
  $ref_seqs = "/isilon/biodiversity/users/chenw_lab/Reference/gg_13_5_otus/rep_set/97_otus.fasta";
  $id_tax = "/isilon/biodiversity/users/chenw_lab/Reference/gg_13_5_otus_FixTaxonomy/97_otu_taxonomy.fix.ascii.tax";
  $ref_chimera="/isilon/biodiversity/users/chenw_lab/Reference/rdp_gold.fa";
}

print "Start processing...", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";


#############################################
# pick_open_reference_otus for 
# forward & reverse sequences respectively
#############################################
print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nPick_open_reference_otus for $fasta...\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n";

# make pick_open_parametre file
system(`echo "pick_open_reference_otus:similarity $similarity" > params_pick_open.txt`);
system (`echo "pick_otus:similarity 0.97" >> params_pick_open.txt`);
#system(`echo "pick_open_reference_otus:prefilter_percent_id 0.97" >> params_pick_open.txt`);
system(`echo "assign_taxonomy:id_to_taxonomy_fp $id_tax" >> params_pick_open.txt`);
system(`echo "assign_taxonomy:reference_seqs_fp $ref_seqs" >> params_pick_open.txt`);
system(`echo "assign_taxonomy:assignment_method rdp" >> params_pick_open.txt`);
system(`echo "assign_taxonomy:rdp_max_memory $max_memory" >> params_pick_open.txt`);
system(`echo "assign_taxonomy:confidence 0.8" >> params_pick_open.txt`);
my $params_open="params_pick_open.txt";


remove_chimera("$fasta");
system (`time pick_open_reference_otus.py -i $base.pick.fasta -p $params_open -o pick_open_otu -r $ref_seqs --suppress_align_and_tree --percent_subsample 1 -f`);
system (`rm *formatted`);

print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nPick_open_reference_otus for $fasta...\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n";

#############################################
# logs
#############################################

my $end_time = time();
my $run_time = $end_time - $start_time;
print "Finished running. Took a total of $run_time seconds.\n";

push (@runtime_array, {'Total'=> "$run_time seconds"});
push (@runtime_array, $runtime_hash);
LogFile::addToLog("Runtime","Running time",\@runtime_array);  
LogFile::writeLog("$DATE.yaml");

chdir $outdir;

print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\nProcess finished...\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n";

###################################################
# Process finished
###################################################


###################################################
# subroutines
###################################################

sub reverse_complement {
  my $file=shift;
  my $base=basename($file, ".fasta");
  print "Making reverse complement of $file\t". POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";  
  my $in = Bio::SeqIO->new(-file=>"$file",-format=>'fasta');
  open (OUT, ">$base.rc.fasta");
  while (my $seq=$in->next_seq()){
    my $seq_rev = $seq->revcom;
    print OUT ">", $seq_rev->id, "\t", $seq_rev->desc, "\n", $seq_rev->seq, "\n";
  }  
}

sub chimera_seqid {
   my $accnos=shift;
   my $maps=shift;

   open (ACCNOS, "<$accnos");
    my @otuIDs;
    my $count_ch=0;
    while (<ACCNOS>) {
      chomp $_;
      push (@otuIDs, $_);
      $count_ch++;
    }
    close ACCNOS;
    print "\nRepresentative sequences of $count_ch OTUs were identified as chimera\n";


    my $names={};
    open (NAMES, "<$maps");
    my $count_otuall=0;
    my $sum_seq=0;
    while (<NAMES>) {
      chomp $_;
      $count_otuall++;
      my @cols=split/\t/, $_;
      my $num=scalar @cols;
      $sum_seq += $num;
      #print $cols[0], "\t", $cols[1], "\n";
      $names->{$cols[0]}=[@cols[1..(scalar @cols -1)]]
    }
    close NAMES;
    print "\nTotal $count_otuall OTUs, containing $sum_seq sequences\n";


    open (OUT, ">$accnos.seqid");  
    my $count_otu=0;
    my $sum_seq_ch=0;
    foreach my $otuid (@otuIDs) {
      if (exists $names->{$otuid}) {
        my @value=@{$names->{$otuid}};
        $count_otu++;
        my $num=scalar @value;
        $sum_seq_ch += $num;
        print OUT join("\n", @value), "\n";
      }
    }    
    close OUT;
    print "Found ", $sum_seq_ch, " sequences in ", $count_otu, " otus that are chimera\n\n";
}


sub remove_chimera {
  my $in_fasta=shift;
  my $base=basename($in_fasta, ".fasta");
  #$in_fasta="all.qiime_failures.$primer.fasta";

  # pick denovo OTUs first step
  system (`time pick_otus.py -i $in_fasta -m cdhit -s 0.97 -o cdhit_$base\_otus/ --max_cdhit_memory $max_memory`); # check time used for this step, --minsize only work for usearch method;
  # pick representative sequences;
  system(`time pick_rep_set.py -i cdhit_$base\_otus/$base\_otus.txt -f $in_fasta -o $base.rep_set.fna`);
  #Chimera checking (can be slow, break) using mothur;
  system (`echo "chimera.uchime(fasta=$base.rep_set.fna, reference=$ref_chimera, processors=$threads)" > mothur.bat`);
  system("$mothur/mothur mothur.bat");
  # rep_set.ref.uchime.accnos: otus with representative sequences being identified as chimera
  # extract sequences belonging to otus with representative sequences being identified as chimera

  if ( -z "$base.rep_set.ref.uchime.accnos" ) {
    system (`cp $in_fasta > $base.pick.fasta`);
  } else {
    my $accnos="$base.rep_set.ref.uchime.accnos";
    my $names="cdhit_$base\_otus/$base\_otus.txt";
    chimera_seqid($accnos, $names);
    system (`perl $scripts_dir/extract_seq.pl $base.rep_set.ref.uchime.accnos.seqid $in_fasta > $base.rep_set.ref.uchime.accnos.fasta`);
    # check chimera again for each sequence 
    # in the OTUs with chimeratic representative sequence
    system (`echo "chimera.uchime(fasta=$base.rep_set.ref.uchime.accnos.fasta, reference=$ref_chimera, processors=2)" > mothur.bat`);
    system ("$mothur/mothur mothur.bat");
    # remove all chimers sequences
    if ( -z "$base.rep_set.ref.uchime.accnos.ref.uchime.accnos" ) {
      system (`cp $in_fasta > $base.pick.fasta`);
    } else {
      system (`echo "remove.seqs(fasta=$in_fasta, accnos=$base.rep_set.ref.uchime.accnos.ref.uchime.accnos)" > mothur.bat`);
      system ("$mothur/mothur mothur.bat");
    }
  } 
}


#sub timer {
#  my $process = shift;
#  $et1 = time();
#  $runtime_hash->{$process}=join(" ", $et1-$st1, "seconds");
#  $st1 = $et1;
#}



__END__

=head1 NAME

=over 12

=item B<M>iB<S>eq B<A>mplicon B<M>etagenomics B<P>ipeline (MSAMP)

=back

=head1 SYNOPSIS

perl MSAMP.pl [-i] [Options] [--help]

=head1 DESCRIPTION

The MiSeq Amplicon Metagenomics Pipeline process sequences generated by MiSeq sequencing machines using data gathered from various biodiversity surveys. 

To run, you must give a path to the input raw data folder. If the raw data files are not zipped (FASTQ files), they will be copied to output folder before running the pipeline. Output folder will be your current working directory if you do not provide one.

=head1 OUTPUTS

Stuff...

=head1 OPTIONS

=over 12

=item B<--help>

Program information and usage instructions. Detailed information and instructions are in the manual

=item B<-i>

Specify the path to the directory of the input data files [REQUIRED]

=item B<-o>

Specify the path of the output directory [Default: current working directory]

=item B<--closed>

Closed reference otu picking [Default: FALSE]

=item B<--reference>

Specify the path to the reference sequences for assigning taxonomy and/or closed reference otu picking

=item B<--taxonomy>

Specify the path to tab-delimited file mapping sequences to assigned taxonomy and/or closed reference otu picking

=item B<--ITS>

Specify which region of ITS you want to extract, must be one of ITS1, ITS2 [Default: none]

=item B<--threads> [N]

Specify how many processors/threads you want to use to run the multi-threaded portions of this pipeline. N processors will be used [Default: 1]

=item B<--trim_method>

Sequence trimming method, must be one of usearch, mothur [Default: usearch]

=item B<--extract>

For if the raw data file(s) need to be decompressed [Default: FALSE]

=item B<--min_overlap> [N]

N-minimum overlap required when joining forward and reverse sequence [Default: 50]

=item B<--perc_max_diff> [N]

N-percent B<MAXIMUM> difference for the overlapping region when joining [Default: 1]E<10> E<8>(i.e. 1% maximum difference = 99% minimum similarity)

=item B<--minlen> [N]

Specify a minimum sequence length to keep. Lengths of sequences will be at least N bp long after sorting [Default: 100]

=item B<--qthreshold> [N]

B<mothur>: Set a minimum average quality score allowed over a window [Default: 20]E<10> E<8>B<usearch>: Truncate the sequence at the first position having quality score <= N [Default: 20]

=item B<--qwindowsize> [N]

Set a minimum average quality score allowed over a window, only used for mothur [Default: 10]

=item B<--OTU_size>

Minimum cluster size for size filtering with usearch [Default: 1]

=item B<--similarity> [N]

Sequence similarity threshold, only used for usearch, cd-hit [Default: 0.97]

=item B<--nosingletons>

Does not process unjoined forward/reverse reads [Default: FALSE]

=item B<--otu_method>

Clustering method, must be one of usearch, cdhit [Default: usearch]

=item B<--tax_method>

Taxonomy assignment method, must be one of mothur, rdp, blast [Default: mothur]

=item B<--max_memory> [N]

Maximum memory allocation, in MB, only used for cdhit, rdp [Default: 80000]

=item B<--confidence>

Minimum confidence to record an taxonomy assignment, only used for rdp and mothur methods [Default: 0.5]

=item B<--evalue> [N]

Expectation value threshold for saving hits

=item B<--blastdb>

Specify the path to the blast database when it is needed


