#!/bin/sh
#
# Usage: q_AODP.sh fasta
# -- our name ---
#$ -S /bin/sh
# Make sure that the input fasta file arrive in the # working directory
#$ -cwd
/bin/echo In directory: `pwd`
/bin/echo Starting on: `date`
#/bin/echo Using threads: $1
# Send mail at submission and completion of script
#$ -m be
#$ -M wen.chen@agr.gc.ca

qsubFile=qsub_454
echo "#!/bin/bash" > $qsubFile
#preserve environment variables
echo "#$ -V" >>$qsubFile
echo "perl /isilon/biodiversity/users/chenw_lab/Bitbucket/miseq_amplicon_mg/new_454_debug.pl -i /isilon/biodiversity/users/chenw_lab/Processing/New_Miseq_Data/test_454 -o /isilon/biodiversity/users/chenw_lab/Processing/New_Miseq_Data/test_454_output --ITS FALSE --tax_method mothur --otu_method cdhit --primer_file /isilon/biodiversity/users/chenw_lab/Processing/New_Miseq_Data/primer --amplicon 16S --threads 19" >> $qsubFile

#make the script executable
chmod a+x $qsubFile
# submit the script to the queue
qsub -cwd -pe smp 19 -q all.q $qsubFile

#Call using (for example):
#qsub -cwd -pe smp 24 q_mafft.sh to_be_aligned_sequences ref_alignement


