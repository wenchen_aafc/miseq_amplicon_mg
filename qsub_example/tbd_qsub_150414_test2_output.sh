#!/bin/bash
#$ -pe smp 12
#$ -q all.q
#$ -cwd
#$ -S /bin/bash

perl /isilon/biodiversity/users/chenw_lab/Bitbucket/miseq_amplicon_mg/new_MSAMP.pl -i /isilon/biodiversity/users/chenw_lab/Processing/New_Miseq_Data/test_fastq/ -o /isilon/biodiversity/users/chenw_lab/Processing/New_Miseq_Data/150414_test2_output3 -c /isilon/biodiversity/users/chenw_lab/Processing/New_Miseq_Data/config_150414_test2_output.txt

assign_taxonomy.py -i all.trim.qiime.sorted.cdhit.rep.fasta -r /home/AAFC-AAC/chenw/references/UNITE/sh_refs_qiime_ver7_99_s_02.03.2015.fasta  -t /home/AAFC-AAC/chenw/references/UNITE/sh_taxonomy_qiime_ver7_99_s_02.03.2015.txt -m mothur -c 0.70


assign_taxonomy.py -i all.trim.qiime.sorted.cdhit.rep.fasta -r /AAFC-AAC/chenw/references/UNITE/sh_refs_qiime_ver7_99_s_02.03.2015.fasta  -t /home/AAFC-AAC/chenw/referenUNITE/sh_taxonomy_qiime_ver7_99_s_02.03.2015.txt -m mothur -c 0.70

assign_taxonomy.py -i all.trim.qiime.sorted.cdhit.rep.fasta -r tbd.fasta  -t tbd1.tax -m mothur -c 0.70


classify.seqs(fasta=all.trim.qiime.sorted.cdhit.rep.fasta,   template=/home/AAFC-AAC/chenw/references/UNITE/sh_refs_qiime_ver7_99_s_02.03.2015.fasta, taxonomy=/home/AAFC-AAC/chenw/references/UNITE/sh_taxonomy_qiime_ver7_99_s_02.03.2015.mothur.txt, processors=12, cutoff=80, probs=F)
