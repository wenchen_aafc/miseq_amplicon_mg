#!/bin/bash
#$ -pe smp 8
#$ -q all.q
#$ -cwd
#$ -S /bin/bash

new_MSAMP.pl -i ~/pipeline_test/454 -o ./ -c ~/miseq_amplicon/scripts/config_454.txt 
