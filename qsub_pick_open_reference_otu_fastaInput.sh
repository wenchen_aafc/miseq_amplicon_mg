#!/bin/bash
#$ -q all.q
#$ -pe smp 24
#$ -cwd
#$ -N "pick_open_fastaInput"
#$ -S /bin/bash

fasta=$1
amplicon=$2
threads=$3

source /isilon/biodiversity/pipelines/qiime-1.7.0/activate.sh
perl /isilon/biodiversity/users/chenw_lab/Bitbucket/miseq_amplicon_mg/pick_open_reference_otu_fastaInput.pl -f $fasta --amplicon $amplicon --threads $threads
