#!/usr/bin/env perl

package sub_1;

use strict;
use warnings;

use Bio::SeqIO;
use File::Basename;
use File::Copy;
use Cwd;

#modules
use lib "$FindBin::Bin"; #Path to the modules
use LogFile;
use sub_1;

sub unzip_1 {
  my $start_run = time();
  print "\n\nStart decompress fastq.gz files: ", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";

  my $dir = shift;
  my $outdir = shift;
  opendir (DIR, $dir) or die $!;
  my @files = grep ($_ =~ m/\Qfastq.gz\E/ && $_ !~ m/Undetermined/, readdir(DIR));
  closedir (DIR);
  #unzips all the files in the array @files
  if ( scalar @files == 0 ) {
    print "\nNo fastq.gz files were found in $dir, will not do decompression\n";
    copy_files_1 ($dir, $outdir);
  } else {
    
    
    foreach my $file (@files) {
      my $basename = basename($file, ".gz");    
      print "Unzipping $file\n";
      system ("gunzip -c $dir/$file > $outdir/$basename");
     }
  }
  my $end_run = time();
  my $run_time = $end_run - $start_run;
  print "Finish converting timmed fastq files to fasta files:", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n"; 
  print "Job took $run_time seconds\n\n";
}

sub copy_files_1 {
  my $dir = shift;
  my $outdir = shift;
  opendir (DIR, $dir) or die $!;
  my @files = grep ($_ =~ m/\Q.fastq\E/ && $_ !~ m/\Qfastq.gz\E/ && $_ !~ m/Undetermined/, readdir(DIR));
  closedir (DIR);
  foreach my $file (@files) {
    copy ("$dir/$file", "$outdir/$file");
  }
}

sub name_correction_1 {
  my @special_char=("/", "*", "__", "\\","(",")",":",";",",","-");
  print "\nSpecial characters: @special_char,  will be removed from file name.\n\n";
  
  open (NAME, ">filename_match.txt"); #mapping file for old and new names
  print NAME "Old name\tNew name\n";
  
  my @files = grep ($_ =~ m/fastq$/, <*>);
  
  foreach my $file (@files) {
    my $new_name = $file;
    
    foreach (@special_char) {
      if ($file =~ m/\Q$_\E/) {
        $new_name =~ s/\Q$_\E//g;
      }
    }
    if ($file ne $new_name) {
      rename ("$file", "$new_name");
      print NAME "$file\t$new_name\n";
      print "$file renamed to $new_name\n";
    }
  }
  close NAME;
}

sub trim_1 {
  my $start_run = time();

  my $method = shift;  #trimming method
  my $threshold = shift;  #quality threshold
  my $windowsize = shift;  #mothur only
  my $threads = shift;  #mothur onlymy $end_run = time();
  my $minlen = shift;  #usearch only
  my $usearch = shift;  #usearch only
  my $file = shift;
  my $basename = basename($file, ".fastq");
  print "\n\nStart trimming fastq files: $file, \n";
  if ($method eq "usearch") {  
    system ("$usearch --fastq_filter $file --fastq_truncqual $threshold --fastq_minlen $minlen --fastqout $basename.trim.fastq");
    my $usearch_com="$usearch --fastq_filter $file --fastq_truncqual $threshold --fastq_minlen $minlen --fastqout $basename.trim.fastq";
    print "$usearch_com\n";
  } elsif ($method eq "mothur") {
    open (MOTHUR, ">mothur.bat");
    print MOTHUR "fastq.info(fastq=$file)\n";
    print MOTHUR "trim.seqs(fasta=$basename.fasta, qfile=$basename.qual, maxambig=0, qwindowsize=$windowsize, qwindowaverage=$threshold, processors=$threads, minlength=$minlen, maxhomop=10)\n";
    my $mothur_com = "trim.seqs(fasta=$basename.fasta, qfile=$basename.qual, maxambig=0, qwindowsize=$windowsize, qwindowaverage=$threshold, processors=$threads, minlength=$minlen, maxhomop=10)";
    print MOTHUR "make.fastq(fasta=$basename.trim.fasta, qfile=$basename.trim.qual)\n";
    close MOTHUR;
    system ("mothur mothur.bat");
    print "$mothur_com\n"; 
    #system ("rm mothur.bat");      
  }
  
  if ( $method eq "mothur" ) {
    #system ("mkdir Mothur_Scrap");
    #system ("mkdir Mothur_Log");
    system ("mv *scrap* temp/");
    system ("mv mothur.* temp/");
    system ("rm *qual");
    system ("rm *fasta");
  }
  my $end_run = time();
  my $run_time = $end_run - $start_run;
  print "Finish trimming $file to $basename.trim.fastq: ", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n"; 
  print "Job took $run_time seconds\n\n";
  return $basename;
}

sub reverse_complement_1 {
  my @files = grep ($_ =~ m/.fasta$/, <*>);
  foreach my $file (@files) {
    my $base=basename($file, ".fasta");
    print "Making reverse complement of $file\t". POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";
    my $basename = basename ($file, ".fasta");
    
    my $in = Bio::SeqIO->new(-file=>"$file",-format=>'fasta');
    open (OUT, ">$basename.rc.fasta");
    while (my $seq=$in->next_seq()){
      my $seq_rev = $seq->revcom;
      print OUT ">", $seq_rev->id, "\t", $seq_rev->desc, "\n", $seq_rev->seq, "\n";
    }  
  }
}

sub reverse_complement {
  my @files = grep ($_ =~ m/.fasta$/ && ($_ =~ m/.un2./ || $_ =~ m/_R2_/ ), <*>);
  foreach my $file (@files) {
    print "Making reverse complement of $file\t". POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";
    my $basename = basename ($file, ".fasta");
    
    my $in = Bio::SeqIO->new(-file=>"$file",-format=>'fasta');
    open (OUT, ">$basename.rc.fasta");
    while (my $seq=$in->next_seq()){
      my $seq_rev = $seq->revcom;
      print OUT ">", $seq_rev->id, "\t", $seq_rev->desc, "\n", $seq_rev->seq, "\n";
    }  
  }
}

#chimera_uchime($fasta, $reference, $threads);

sub chimera_uchime {
  my $fasta=shift;
  my $reference=shift;
  my $threads=shift;

  my $basename=basename($fasta, ".fasta");
  open (MOTHUR, ">mothur.bat");
  print MOTHUR "chimera.uchime(fasta=$fasta, reference=$reference, processors=$threads)\n";
  close MOTHUR;
  system("mothur mothur.bat");
  my $count_total=0;
  open (CHIM, "<$basename.uchime.chimeras");
  my $ID_non_chimera={};
  my $count_chim=0;
  my $count_nonchim=0;
  while (<CHIM>) {
    $count_total++;
    chomp $_;
    my @fields = split /\t/, $_;
    my $otu_id = $fields[1];
    my $chimera = $fields[(scalar @fields)-1];
    if ($chimera eq "N") {
      $count_nonchim++;
      $ID_non_chimera->{$otu_id} = $chimera;
      #print $otu_id, "\t", $chimera, "\n";
    } elsif ( $chimera eq "Y") {
      $count_chim++;
    }
  }
  print "$fasta: $count_total sequences\n$count_chim chimera sequences\n$count_nonchim non-chimera sequences\n\n";
  my $in = Bio::SeqIO->new(-file=>"<$fasta", -format=>"fasta");
  my $out = Bio::SeqIO->new(-file=>">$basename.nonchimera.fasta", -format=>"fasta");
  #open(OUT, ">$basename.nonchimera.fasta");
  my $count_out=0;
  while (my $seq=$in->next_seq()) {
    my $seqid=$seq->id;
    #print $seqid, "\n";
    if ( exists $ID_non_chimera->{$seqid} ) {
      $count_out++;
      #print $seqid, "\t", $ID_non_chimera->{$seqid}, "\n";
      $out->write_seq($seq)
      #print OUT ">", $seq->id, " ", $seq->desc, "\n", $seq->seq, "\n";
    }    
  }
  print "$count_out non chimera sequences are written to $basename.nonchimera.fasta\n\n";
  return "$basename.nonchimera";
}
  
  

sub fastq2fasta_1 {
  
  my $start_run = time();
  print "Start converting trimmed fastq files to fasta files: ", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";

  my @files = grep ($_ =~ m/.trim.fastq$/, <*>);
  
  foreach my $file (@files) {
    print "Converting $file\t". POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";
    my $basename = basename ($file, ".fastq");
    
    my $in = Bio::SeqIO->new(-file=>"$file",-format=>'fastq');
    my $out = Bio::SeqIO->new(-file=>">$basename.fasta",-format=>'fasta');
    while (my $seq=$in->next_seq()){
      $out->write_seq($seq);
    }       
  }
  my $end_run = time();
  my $run_time = $end_run - $start_run;
  print "Finish converting timmed fastq files to fasta files: ", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n"; 
  print "Job took $run_time seconds\n";
  
}

sub fastq2fasta2_1 {
  my $file = shift or die $!;
  my $start_run = time();
  my $base = basename($file, ".fastq");

  print "Converting $file to $base.fasta...\t". POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n";
  my $basename = basename ($file, ".fastq");
    
  my $in = Bio::SeqIO->new(-file=>"$file",-format=>'fastq');
  my $out = Bio::SeqIO->new(-file=>">$basename.fasta",-format=>'fasta');
  while (my $seq=$in->next_seq()){
    $out->write_seq($seq);
  }       
  my $end_run = time();
  my $run_time = $end_run - $start_run;
  print "Finish converting $file to $base.fasta...: ", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\n"; 
  print "Job took $run_time seconds\n";
  return $basename;
  
}



sub qiime_format_1 {
  #my @files = grep ($_ =~ m/fasta$/, <*>);
  #foreach my $fasta (@files) {
  my $fasta=shift or die $!;
  my $basename = basename($fasta, ".fasta");
    
  print "Converting $fasta to $basename.qiime.fasta\n";
  my $sampleid = (split (/_/, $fasta))[0];
    
  open (OUT, ">$basename.qiime.fasta");
  my $in_fasta = Bio::SeqIO->new(-file=>"$fasta", -format=>"Fasta");
  my $count=1;
  while (my $seq = $in_fasta->next_seq()) {
    my $seqid=join("_",$sampleid, $count);
    my $seqdesc=$seq->id; #obtain description line of a sequence
    print OUT ">", $seqid, "\t",$seqdesc, "\n", $seq->seq, "\n";
    $count++;
  }
    close OUT;
}



sub qiime_format_454 {
  my @files = grep ($_ =~ m/fasta$/, <*>);
  foreach my $fasta (@files) {
    my $basename = basename($fasta, ".fasta");
    
    print "Converting $fasta to $basename.qiime.fasta\n";
    my $sampleid = (split (/-/, $fasta))[0];
    
    open (OUT, ">$basename.qiime.fasta");
    my $in_fasta = Bio::SeqIO->new(-file=>"$fasta", -format=>"Fasta");
    my $count=1;
    while (my $seq = $in_fasta->next_seq()) {
            #my $seqid=join("_",$sampleid, $count);
            my $seqdesc=$seq->id; #obtain description line of a sequence
            my $seqid=join("_",$sampleid, $seqdesc);
            print OUT ">", $seqid, "\n", $seq->seq, "\n";
            $count++;
    }
    close OUT;
  }
}

sub qiime_format_Miseq {
  #my @files = grep ($_ =~ m/fasta$/, <*>);
  #foreach my $fasta (@files) {
  my $fasta=shift or die $!;
  my $basename = basename($fasta, ".fasta");
    
  print "Converting $fasta to $basename.qiime.fasta\n";
  my $sampleid = (split (/_/, $fasta))[0];
    
  open (OUT, ">$basename.qiime.fasta");
  my $in_fasta = Bio::SeqIO->new(-file=>"$fasta", -format=>"Fasta");
  my $count=1;
  while (my $seq = $in_fasta->next_seq()) {
    #my $seqid=join("_",$sampleid, $count);
    my $seqdesc1=$seq->id; #obtain description line of a sequence
    $seqdesc1=~ s/:/_/g; # convert : to _
    $seqdesc1=~ s/-/__/g; # convert - to __
    my $seqdesc2=$seq->desc;
    $seqdesc2=~ s/:/_/g; # convert : to _
    my $seqid=join("|",$sampleid, $seqdesc1, $seqdesc2, $count);
    print OUT ">", $seqid, "\n", $seq->seq, "\n";
    $count++;
  }
    close OUT;
}

sub cat_1 {
  my $prefix = shift;
  open CAT, ">$prefix.qiime.fasta";
  
  my @files = grep ($_ =~ m/qiime/, <*>);
  foreach my $file (@files) {
    my $in = Bio::SeqIO->new(-file=>"$file", -format=>'fasta');
    while (my $seq=$in->next_seq()) {
      print CAT ">", $seq->id, "\t", $seq->desc, "\n", $seq->seq, "\n";
    }
  }
  close CAT;
  print "Merged all files.\n";
}

sub length_sort_1 {
  my $usearch = shift;
  my $minlen = shift; #minimum length for sequences  
  my $prefix = shift;
  
  system ("$usearch -sortbylength $prefix.qiime.fasta -output $prefix.qiime.sorted.fasta -minseqlength $minlen");
  print "$prefix.qiime.fasta sorted.\n";
}

sub length_sort_ITS_1 {
  my $usearch = shift;
  my $minlen = shift; #minimum length for sequences  
  my $prefix = shift;
  
  system ("$usearch -sortbylength $prefix.fasta -output $prefix.sorted.fasta -minseqlength $minlen");
  print "$prefix.fasta sorted.\n";
}


sub pick_otu_rep_1 {
  my $otu_m = shift;
  my $similarity = shift;
  my $size = shift;
  my $max_memory = shift; #cdhit only
  my $threads = shift; # threads to use
  #for file name purposes
  my $ITS = shift;
  my $prefix = shift;
  
  my $input_file = "$prefix.qiime.recount.sorted$ITS.fasta";
  $input_file =~ s/\.NULL\./\./g;
  my $basename = basename($input_file, ".fasta");
  
  my $out_dir = "$otu_m\_OTU_results";
  
  print "Picking representative sequences for $input_file using $otu_m.\n";
  
  #if ($otu_m eq "usearch") {
  #  system ("pick_otus.py -i $input_file -o $out_dir -m $otu_m -s $similarity -g $size --suppress_reference_chimera_detection --suppress_de_novo_chimera_detection");
  if ( $otu_m eq "uclust") {
    system ("pick_otus.py -i $input_file -o $out_dir -s $similarity -g $size --optimal_uclust --suppress_reference_chimera_detection --suppress_de_novo_chimera_detection"); 

    system ("pick_rep_set.py -i $out_dir/$basename\_otus.txt -f $input_file -o $basename.$otu_m.rep.fasta");
  } 
  if ($otu_m eq "cdhit") {
    mkdir $out_dir;
    system ("cd-hit -i $input_file -o $out_dir/$basename -c $similarity -M $max_memory -g 1 -d 999");
    
   ##reformatting the files to match the input for the scripts afterwards
   open (INMAP, "<$out_dir/$basename.clstr") or die $!;

    my %clusters;
    my $OTU_id;
    while (my $line = <INMAP>) {
     if ($line =~ m/Cluster/) {
       my $cluster_id = (split (' ', $line))[1];
       $OTU_id = $cluster_id;
     } else {
       if ($line =~ m/\*/) {
         my $tmp = (split ('>', $line))[1];
         my $rep = (split ('\.\.\.', $tmp))[0];
         push (@{$clusters{$OTU_id}}, $rep);
       } else {
         my $tmp = (split ('>', $line))[1];
         my $seq = (split ('\.\.\.', $tmp))[0];
         push (@{$clusters{$OTU_id}}, $seq);
       }
     }
   }

   close INMAP;

   open (OUTMAP, ">$out_dir/$basename\_otus.txt") or die $!;

   foreach my $key (sort keys %clusters) {
     print OUTMAP "$key", "\t", join("\t", @{$clusters{$key}}), "\n";
   }
   close OUTMAP;
   system ("pick_rep_set.py -i $out_dir/$basename\_otus.txt -f $input_file -o $basename.$otu_m.rep.fasta");
  }

  return "$basename";
}

sub pick_otu_rep_454 {
  my $otu_m = shift;
  my $similarity = shift;
  my $size = shift;
  my $max_memory = shift; #cdhit only
  my $threads = shift; # threads to use
  #for file name purposes
  my $ITS = shift;
  my $prefix = shift;
  
  my $input_file = "$prefix.qiime.nonchimera.sorted$ITS.fasta";
  $input_file =~ s/\.NULL\./\./g;
  my $basename = basename($input_file, ".fasta");
  
  my $out_dir = "$otu_m\_OTU_results";
  
  print "Picking representative sequences for $input_file using $otu_m.\n";
  
  #if ($otu_m eq "usearch") {
  #  system ("pick_otus.py -i $input_file -o $out_dir -m $otu_m -s $similarity -g $size --suppress_reference_chimera_detection --suppress_de_novo_chimera_detection");
  if ( $otu_m eq "uclust") {
    system ("pick_otus.py -i $input_file -o $out_dir -s $similarity -g $size --optimal_uclust --suppress_reference_chimera_detection --suppress_de_novo_chimera_detection"); 

    system ("pick_rep_set.py -i $out_dir/$basename\_otus.txt -f $input_file -o $basename.$otu_m.rep.fasta");
  } 
  if ($otu_m eq "cdhit") {
    mkdir $out_dir;
    system ("cd-hit -i $input_file -o $out_dir/$basename -c $similarity -M $max_memory -g 1 -d 999");
    
   ##reformatting the files to match the input for the scripts afterwards
   open (INMAP, "<$out_dir/$basename.clstr") or die $!;

    my %clusters;
    my $OTU_id;
    while (my $line = <INMAP>) {
     if ($line =~ m/Cluster/) {
       my $cluster_id = (split (' ', $line))[1];
       $OTU_id = $cluster_id;
     } else {
       if ($line =~ m/\*/) {
         my $tmp = (split ('>', $line))[1];
         my $rep = (split ('\.\.\.', $tmp))[0];
         push (@{$clusters{$OTU_id}}, $rep);
       } else {
         my $tmp = (split ('>', $line))[1];
         my $seq = (split ('\.\.\.', $tmp))[0];
         push (@{$clusters{$OTU_id}}, $seq);
       }
     }
   }

   close INMAP;

   open (OUTMAP, ">$out_dir/$basename\_otus.txt") or die $!;

   foreach my $key (sort keys %clusters) {
     print OUTMAP "$key", "\t", join("\t", @{$clusters{$key}}), "\n";
   }
   close OUTMAP;
   system ("pick_rep_set.py -i $out_dir/$basename\_otus.txt -f $input_file -o $basename.$otu_m.rep.fasta");
  }

  return "$basename";
}


sub assign_taxon_1 {
  my $otu_m = shift;
  my $tax_m = shift;
  my $ref_seqs = shift;
  my $id_tax = shift;
  my $confidence = shift;
  my $max_memory = shift;
  my $threads = shift;
  
  my $basename = shift ;
  my $cutoff = $confidence*100;
  
  die "$basename.$otu_m.rep.fasta does not exit: $!" unless -e "$basename.$otu_m.rep.fasta";
  die "$ref_seqs does not exit: $!" unless -e $ref_seqs;
  die "$id_tax does not exit: $!" unless -e $id_tax;

  print "\nUsing $tax_m to assign taxonomy to $basename.$otu_m.rep.fasta\n";
  
  my $start_run = time();

 # if ($tax_m eq "uclust") {
  #  print "uclust for taxonomy assignment is not available in qiime 1.6"
  #  print "\n\nusing uclust in qiime to assign taxonomy. \n";  
  #  system ("assign_taxonomy.py -i $basename.$otu_m.rep.fasta -m $tax_m -r $ref_seqs -t $id_tax");
 # } elsif ($tax_m eq "mothur") {

  if ($tax_m eq "mothur") {
    
    print "\n\nusing mothur in qiime to assign taxonomy. \n";
    print "reference fasta file: $ref_seqs;\nreference taxonomy file: $id_tax\n\n";  
    system ("assign_taxonomy.py -i $basename.$otu_m.rep.fasta -m $tax_m -r $ref_seqs -t $id_tax -c $confidence");

  } elsif ($tax_m eq "rdp") {
    print "\nRDP classifier is currently not implemented, as for assignment with rdp, each assigned taxonomy must be exactly 6 levels deep, with is not comparable to UNITE ITS database format (7 levels)!\n";
    print "Unless you have a reference database that was properly formated, rdp classifier implemented in qiime will not be functional\n";
    
    print "\n\nusing mothur in qiime to assign taxonomy. \n";
    print "reference fasta file: $ref_seqs;\nreference taxonomy file: $id_tax\n\n"; 
    system ("assign_taxonomy.py -i $basename.$otu_m.rep.fasta -m $tax_m -r $ref_seqs -t $id_tax -c $confidence");
    system ("iconv -f UTF8 -t US-ASCII//TRANSLIT $tax_m\_assigned_taxonomy/$basename.$otu_m.rep_tax_assignment.txt > $tax_m\_assigned_taxonomy/$basename.$otu_m.rep_tax_assignment.ascii.txt");
    system ("mv $tax_m\_assigned_taxonomy/$basename.$otu_m.rep_tax_assignment.ascii.txt $tax_m\_assigned_taxonomy/$basename.$otu_m.rep_tax_assignment.txt");
    
    #system ("assign_taxonomy.py -i $basename.$otu_m.rep.fasta -m $tax_m -r $ref_seqs -t $id_tax -c $confidence --rdp_max_memory $max_memory");
  }

  my $end_run = time();
  my $run_time = $end_run - $start_run;
  print "Job took $run_time seconds\n";
  
}

sub otu_table_1 {
  my $otu_method = shift;
  my $tax_method = shift;
  my $basename = shift;
  my $region = shift;
  
  my $OTU_dir = "$otu_method\_OTU_results";
  
  print "Making OTU table.\n\n";

#all_forward.qiime.sorted.cdhit.rep.mothur.wang.taxonomy  

  system ("make_otu_table.py -i $OTU_dir/$basename\_otus.txt -t $tax_method\_assigned_taxonomy/$basename.$otu_method.rep_tax_assignments.txt -o $basename.$tax_method\_otu.biom");  
  system ("convert_biom.py -i $basename.$tax_method\_otu.biom -o $basename.$tax_method\_otu.tab -b --header_key=taxonomy");
}


sub ITS_1 {
  my $prefix = shift;
  my $threads = shift;
  my $kingdoms = shift;
  
  my $input_file = "$prefix.fasta";
  my $basename = basename ($input_file, ".fasta");
  
  my @non_empty;
  
  #mkdir "its_extracted";
  
  print "Extracting ITS from $input_file sequences.\n";
  #system ("~/ITSx_1.0.11/ITSx -i $input_file -o $basename -cpu $threads --preserve T -t $kingdoms");
  system ("~/ITSx_1.0.11/ITSx -i $input_file -o $basename -cpu $threads --preserve T ");
  
}

sub join_seqs_1 {
  my $min_overlap = shift; #minimum overlap for the forward and reverse reads
  my $perc_max_diff = shift; #maximum percentage difference allowed for the overlapping region
  
  #my @forward = sort (grep ($_ =~ m/comm/ && $_ =~ m/_R1_/, <*>));
 # my @reverse = sort (grep ($_ =~ m/comm/ && $_ =~ m/_R2_/, <*>));

  my @forward = sort (grep $_ =~ m/_R1_.*.match./, <*>);
  my @reverse = sort (grep $_ =~ m/_R2_.*.match./, <*>);

  print join(",", @forward), "\n";
  print join(",", @reverse), "\n";
  #assuming each forward reads file has a matching reverse reads file
  for (my $i = 0; $i < scalar (@forward); $i ++) {
    my $basename = join ('_', (split ('_', $forward[$i]))[0..2]); 
    print "\n\n", $forward[$i], "\t", $reverse[$i], "\n\n";
    print "Joining $forward[$i] and $reverse[$i]\n";
    system ("~/fastq-join -m $min_overlap -p $perc_max_diff $forward[$i] $reverse[$i] -o $basename.%.fastq");
      
   }
  }
#}

sub recount_1 {
    my $fasta=shift; 
    my $name=basename($fasta);
    my $basename=basename($fasta, ".fasta");
    print "Recounting\t$name\t", POSIX::strftime('%B %d, %Y %H:%M',localtime()), "\t";
    my $st = time();
#    print "\n$sampleid\n";
    open (OUT, ">$basename.recount.fasta");
    my $in_fasta = Bio::SeqIO->new(-file=>"$fasta", -format=>"Fasta");
    my $count=1;
    while (my $seq = $in_fasta->next_seq()) {
      my @col=split /_/, $seq->id;
      my $sampleid=$col[0];
      my $seqid=join("_",$sampleid, $count);
#     print "\n$seqid";
      my $seqdesc=$seq->desc; #obtain description line of a sequence
      $count++;
      print OUT ">", $seqid, "\t",$seqdesc, "\n", $seq->seq, "\n";
#   }    
  }
  my $et = time();
  my $rt = $et - $st;
  print "Job took $rt seconds\n";
  print "Completed recounting\t$fasta\n\n";

  close OUT;
}

sub sort_seq_length_1 {
  my $minlen= shift;
  my $fastafile=shift or die "fasta file?";
  my @suffix = (".fasta", ".fas", ".fna", ".fa");
  my $basename=basename ($fastafile, @suffix);
  my $name=basename($fastafile);
  print "Sorting\t$name\n";
# print "\n$sampleid\n";
  open (OUT, ">$basename.sorted.fasta");

  my $s_wrote1;
  my $s_wrote2;
  my $s_read1;

  my $in=Bio::SeqIO->new(-file=>"$fastafile", -format=>"fasta");
  my $seq_des = {};
  my $seq_seq = {};
  my $seq_length = {};
  my %ids1=();
  my $id;
  my $des;
  my $sequence;

  while (my $seq=$in->next_seq()) {
    $id=$seq->id;
    $des=$seq->desc;
    $sequence=$seq->seq;
    $seq_des->{$id}=$des;
    $seq_seq->{$id}=$sequence;
    $seq_length->{$id} = length($seq->seq);
    $s_read1++; 
    $ids1{$id}++; 
  }

  # sort hash value descending
  my @id_sort = sort { $seq_length->{$b} <=> $seq_length->{$a} } keys(%$seq_length);
  my %seen=();
  my @id_sort_uniq = grep { !$seen{$_}++ } @id_sort;
  #my @length = @{$h}{@keys};

  $s_wrote2=0;
  for my $k (@id_sort) {
    my $len = length($seq_seq->{$k});
    if ($len >= $minlen) { 
      print OUT ">", $k, "\t", $seq_des->{$k}, "\n", $seq_seq->{$k}, "\n";
      $s_wrote2++;
    }
  }
 
  print "sorted $s_read1 FASTA records.\nsorted and kept $s_wrote2 sequences longer than $minlen.\nFile write to $basename.sorted.fasta\n\n"; 

}

sub make_fastq {
  my $in_dir = shift or die "Input folder does not exist: $!";
  my $out_dir = shift or die "$!";
  my $fasta_ext = shift or die "FASTA filename extension?: $!";
  my $qual_ext = shift or die "QUAL filename extension?: $!";
  system("cp $in_dir/*.$fasta_ext $out_dir | cp $in_dir/*.$qual_ext $out_dir");

  print $in_dir, "\n";
  print $out_dir, "\n";
  print $fasta_ext, "\n";
  print $qual_ext, "\n";

  opendir (DIR, $out_dir) or die $!;
  my @fasta = sort (grep $_ =~ m/\.$fasta_ext$/, readdir(DIR));
  my @qual = sort (grep $_ =~ m/\.$qual_ext/, readdir(DIR));
  print join(",", @qual), "\n";
  print join(",", @fasta), "\n";
  close DIR;
 

  print "A total of ", scalar @fasta, " FASTA files \n\n";

  my @fasta_base;
  foreach my $fas (@fasta) {
    my $fas_base = basename($fas, ".$fasta_ext");
    print $fas_base,"\t";
    push (@fasta_base, $fas_base);
  }
  foreach my $fn (@fasta_base) { 
    open (MOTHUR, ">mothur.bat");
    print MOTHUR "make.fastq(fasta=$out_dir/$fn.$fasta_ext, qfile=$out_dir/$fn.$qual_ext)\n";
    close MOTHUR;
    system ("mothur mothur.bat");
    system ("mv *mothur* temp");   
  }
  
}



sub blast_primer_fasta {
  my $primer = shift or die $!;
  my $fasta = shift or die $!;
  my $threads = shift;
  my @suffix = (".fna", ".fasta", ".fas", ".fa");
  my $basename=basename($fasta, @suffix);
  system("formatdb -p F -o T -i $fasta");
  system("blastn -query $primer -db $fasta -evalue 0.01 -task blastn -out $basename.blast -max_target_seqs 100000000 -perc_identity 90 -dust no -num_threads $threads -outfmt '7 qseqid sseqid pident evalue bitscore qlen length mismatch gapopen qstart qend sstart send'");

  open (BL, "<$basename.blast");
  my @forward_seqid;
  my @reverse_seqid;
  
  while (<BL>) {
    chomp $_;
    if ( $_=~ m/#/) {
      next;
    } else {
      my ($query_id, $subject_id, $identity, $evalue, $bit_score, $query_length, $alignment_length, $mismatches, $gap_opens, $qstart, $qend, $sstart, $send) = split ("\t", $_);
      my $primer_direction= $qend - $qstart;
      my $sequence_direction = $send - $sstart;
      #print $query_id, ",", $subject_id, ",", $qstart, ",", $qend, ",", $sstart, ",", $send, ",", "\n";
      my $direction;
      if ( ($primer_direction > 0) && ($sequence_direction > 0) ) {
        $direction = "forward";
      } elsif ( ($primer_direction >0) && ($sequence_direction <0)) {
        $direction = "reverse";
      }
      if ( ($query_id eq "forward") && ( $direction eq "forward") ) {
        push (@forward_seqid, $subject_id);
      } elsif ( ($query_id eq "reverse") && ($direction eq "forward") ) {
        push (@reverse_seqid, $subject_id);
      } elsif ( ($query_id eq "forward") && ( $direction eq "reverse")) {
        push (@reverse_seqid, $subject_id);
      } elsif ( ($query_id eq "reverse") && ( $direction eq "reverse")) {
        push (@forward_seqid, $subject_id);
      }
    }
  }
  my @forward_seqid_uniq = do { my %seen; grep { !$seen{$_}++ } @forward_seqid };
  my @reverse_seqid_uniq = do { my %seen1; grep { !$seen1{$_}++ } @reverse_seqid };
  my $for ={};
  my $rev ={};
  foreach my $for_seq (@forward_seqid_uniq) {
    $for->{$for_seq} = "forward";
  }
  foreach my $rev_seq (@reverse_seqid_uniq) {
    $rev->{$rev_seq} = "reverse";
  }
  
  close BL;

  my $in = Bio::SeqIO->new(-file=>"$fasta",-format=>'fasta');
 
  my $out_for = Bio::SeqIO->new(-file=>">$basename.forward.fna",-format=>'fasta');
  my $out_rev = Bio::SeqIO->new(-file=>">$basename.reverse.fna",-format=>'fasta');
  #open (OUT, ">$basename.forward.fna");
  while (my $seq=$in->next_seq()){
    my $seqid = $seq->id;
    if ( exists $for->{$seqid})  {
      $out_for->write_seq($seq);
    } 
    if ( exists $rev->{$seqid})  {
      $out_rev->write_seq($seq);
    }
  }
 
}



sub blast_primer_ID {
  my $primer = shift or die $!;
  my $fasta = shift or die $!;
  my $threads = shift;
  my @suffix = (".fna", ".fasta", ".fas", ".fa");
  my $basename=basename($fasta, @suffix);
  system("formatdb -p F -o T -i $fasta");
  system("blastn -query $primer -db $fasta -evalue 0.01 -task blastn -out $basename.blast -max_target_seqs 100000000 -perc_identity 90 -dust no -num_threads $threads -outfmt '7 qseqid sseqid pident evalue bitscore qlen length mismatch gapopen qstart qend sstart send'");

  my $fas=Bio::SeqIO->new(-file=>"$fasta", -format=>"fasta");
  my $count;
  while (my $fas_seq = $fas->next_seq()) {
    $count++;
  }

  open (BL, "<$basename.blast");
  my @forward_seqid;
  my @reverse_seqid;
  
  while (<BL>) {
    chomp $_;
    if ( $_=~ m/#/) {
      next;
    } else {
      my ($query_id, $subject_id, $identity, $evalue, $bit_score, $query_length, $alignment_length, $mismatches, $gap_opens, $qstart, $qend, $sstart, $send) = split ("\t", $_);
      my $primer_direction= $qend - $qstart;
      my $sequence_direction = $send - $sstart;
      #print $query_id, ",", $subject_id, ",", $qstart, ",", $qend, ",", $sstart, ",", $send, ",", "\n";
      my $direction;
      if ( ($primer_direction > 0) && ($sequence_direction > 0) ) {
        $direction = "forward";
      } elsif ( ($primer_direction >0) && ($sequence_direction <0)) {
        $direction = "reverse";
      }
      if ( ($query_id eq "forward") && ( $direction eq "forward") ) {
        push (@forward_seqid, $subject_id);
      } elsif ( ($query_id eq "reverse") && ($direction eq "forward") ) {
        push (@reverse_seqid, $subject_id);
      } elsif ( ($query_id eq "forward") && ( $direction eq "reverse")) {
        push (@reverse_seqid, $subject_id);
      } elsif ( ($query_id eq "reverse") && ( $direction eq "reverse")) {
        push (@forward_seqid, $subject_id);
      }
    }
  }
  my @forward_seqid_uniq = do { my %seen; grep { !$seen{$_}++ } @forward_seqid };
  my @reverse_seqid_uniq = do { my %seen1; grep { !$seen1{$_}++ } @reverse_seqid };
  open (OUT_FOR, ">$basename.forward.id");
  open (OUT_REV, ">$basename.reverse.id");
  print OUT_FOR join("\n", @forward_seqid_uniq);
  print OUT_REV join("\n", @reverse_seqid_uniq);
  close OUT_FOR;
  close OUT_REV;
  print "$basename.fastq: $count sequences; ", scalar @forward_seqid_uniq, " forward sequences; ", scalar @reverse_seqid_uniq, " reverse sequences!", "\n\n";
  return $basename;
  #system("~/seqtk/seqtk subseq $basename.fastq $basename.forward.id > $basename.forward.fastq");
  #system("~/seqtk/seqtk subseq $basename.fastq $basename.reverse.id > $basename.reverse.fastq");
 
}  


sub split_miseq {
  my $oligo_file = shift;
  my $outdir = shift;
  my $dir = shift;
  my $threads = shift;
  opendir(DIR, "$outdir/temp");
  my @raw_fastaF = grep($_=~ m/_R1_/ && $_=~ m/\.fasta$/,readdir(DIR));
  closedir(DIR);

  opendir(DIR, ".");
  my @raw_fastaR = grep($_=~ m/_R2_/ && $_=~ m/\.fasta$/,readdir(DIR));
  closedir(DIR);

  opendir(DIR, ".");
  my @raw_fasta = grep($_=~ m/\.fasta$/,readdir(DIR));
  closedir(DIR);

  open (OL, "<$oligo_file");
  while (<OL>) {
    chomp $_;
    if ( $_ =~ m/#/) {
      next;
    } else {
      #print $_, "\n";
      my ($olig, $seq, $prim) = split /\t/, $_;
      my ($reg, $primer_direction, $primer_name) = split /:/, $prim;
      
      if ( $primer_direction eq "forward" ) {
        open (TEMP, ">temp.oligo");
        print $_, "\n";
        print TEMP $_;
        close TEMP;
        foreach my $fasta (@raw_fastaF) {
          my $fasta_base = basename($fasta, ".fasta");
          print "Start extract $prim from $fasta_base.fastq:\n";
          open (MOTHUR, ">mothur.bat");
          print MOTHUR "trim.seqs(fasta=$fasta, oligos=temp.oligo, pdiffs=2)\n";
          close MOTHUR;
          system("mothur mothur.bat");
          #unlink "temp.oligo";
        #system("mv $basename.trim.fasta $outdir/$reg/$fasta");
          my $prim_new =$prim;
          $prim_new=~ s/:/_/g;
          unless ( -z "$fasta_base.groups" ) {
            system("cut -f1 $fasta_base.groups |sort|uniq|sed 's/_/:/g' > $fasta_base.$prim_new.id");
            my $count_all=`grep '>' $fasta |wc -l`;
            chomp $count_all;
            my $count= `wc -l $fasta_base.$prim_new.id`;
            chomp $count;
            system("~/seqtk/seqtk subseq $outdir/raw_data/$fasta_base.fastq $fasta_base.$prim_new.id > $outdir/$reg/$fasta_base.fastq");
            system("mv $fasta_base.$prim_new.id $outdir/$reg/$fasta_base.ID");
            print "subset $count from $fasta_base.fastq to $outdir/$reg/$fasta_base.fastq, \n\n";
          }
        }
      } elsif ( $primer_direction eq "reverse" ) {
        open (TEMP, ">temp.oligo");
        print $_, "\n";
        print TEMP $_;
        close TEMP;
        foreach my $fasta (@raw_fastaR) {
          print $fasta, "\n";
          my $fasta_base = basename($fasta, ".fasta");
          print "Start extract $prim from $fasta_base.fastq:\n";
          open (MOTHUR, ">mothur.bat");
          print MOTHUR "trim.seqs(fasta=$fasta, oligos=temp.oligo, pdiffs=2)\n";
          close MOTHUR;
          system("mothur mothur.bat");
          #unlink "temp.oligo";
        #system("mv $basename.trim.fasta $outdir/$reg/$fasta");
          my $prim_new =$prim;
          $prim_new=~ s/:/_/g;
          unless ( -z "$fasta_base.groups" ) {
            system("cut -f1 $fasta_base.groups |sort|uniq|sed 's/_/:/g' > $fasta_base.$prim_new.id");
            my $count_all=`grep '>' $fasta |wc -l`;
            chomp $count_all;
            my $count= `wc -l $fasta_base.$prim_new.id`;
            chomp $count;         
            system("~/seqtk/seqtk subseq $outdir/raw_data/$fasta_base.fastq $fasta_base.$prim_new.id > $outdir/$reg/$fasta_base.fastq");
            system("mv $fasta_base.$prim_new.id $outdir/$reg/$fasta_base.ID");
            print "subset $count from $fasta_base.fastq to $outdir/$reg/$fasta_base.fastq, \n\n";
          }
        }
      } 
    } 
  }
}
close OL;



1; #last line of any perl modules
