#!/usr/bin/perl

use strict;
use warnings;

use File::Basename;
use File::Copy;
use Cwd;

my $input = shift;
my $otu_method = shift;
my $basename = basename ($input, ".taxonomy.export.txt");

my @patterns = ("[0-9][0-9][0-9]", "[0-9][0-9]", "[0-9]");
my @tax_ranks = ("k__", "p__", "c__", "o__", "f__", "g__", "s__");

my %names;

open (NAMES, "<corrected.txt");
while (my $line = <NAMES>) {
	chomp $line;
	my @fields = split ("\t", $line);
	foreach my $field (@fields[1..($#fields)]) {
		$names{$field} = $fields[0];
	}
}
close NAMES;

open (OTU_TAXON, ">$basename.$otu_method.rep_tax_assignments.txt");
open (MEGAN_OUTPUT, "<$input");
while (my $line = <MEGAN_OUTPUT>) {
	chomp $line;
	foreach (@patterns) {
		$line =~ s/; $_;/;/g; #removing confidence scores
	}
	$line =~ s/; /;/g; #removing spaces at the beginning of each section separated by ";"
	$line =~ s/;;/\t/;
	$line =~ s/ /_/g;
	$line =~ s/\.//g; #removing dots
	
	my @fields = split ("\t", $line);
	my @ranks = split (";", $fields[1]);
	my @taxonomy = ();
	
	foreach my $rank (@ranks) {
		if (exists($names{$rank})) {
			my $prefix = substr($names{$rank}, 0, 1);
			my $correct_format = "$prefix\__$rank";
			push (@taxonomy, $correct_format);
		}
	}
	
	for (my $i=0; $i<7; $i++){
		if (exists($taxonomy[$i])) {
			if ($taxonomy[$i] !~ m/$tax_ranks[$i]/) {
				splice (@taxonomy, $i, 0, "$tax_ranks[$i]unidentified");
			}
		} else {
			push (@taxonomy, $tax_ranks[$i]);
		}
	}
	
	my $correct_output = join ("; ", @taxonomy);
	print OTU_TAXON "$fields[0]\t$correct_output\n";
}
close OTU_TAXON;
close MEGAN_OUTPUT;
