#!/usr/bin/env perl

package sub_1;

use strict;
use warnings;

use Bio::SeqIO;
use File::Basename;
use File::Copy;
use List::MoreUtils qw(uniq);
use Cwd;


my $blast = "rep.blast";
my $id_tax = "/home/AAFC-AAC/chenw/references/UNITE/sh_taxonomy_qiime_ver7_99_s_02.03.2015.genbank_NO_20150530.qiime.mothur.txt";
process_blast("cdhit", $blast, $id_tax, "all_join.qiime.recount.sorted.cdhit.rep");

# blastn($basename.rep.fasta, $ref_seqs, $e_value, $threads, $basename)
sub blastn {
  my $query = shift;
  my $db = shift; #/home/AAFC-AAC/chenw/references/UNITE/sh_refs_qiime_ver7_97_s_02.03.2015.genbank_NO_20150530.qiime.mothur.fasta
  my $evalue = shift;
  my $num_threads = shift;
  my $basename = shift;

  # -outfmt 6: $queryId, $subjectId, $percIdentity, $alnLength, $mismatchCount, $gapOpenCount, $queryStart, $queryEnd, $subjectStart, $subjectEnd, $eVal, $bitScore)

  system("blastn -query $basename.rep.fasta -db $db -evalue $evalue -task blastn -out $basename.rep.blast -max_target_seqs 50 -perc_identity 90 -dust no -num_threads 20 -outfmt '7 qseqid sseqid pident evalue bitscore qlen length mismatch gapopen qstart qend sstart send'");
  # filter blast output
  # need to include query coverage length/qlen, if lower than a threshold, should be unclassified
}
  
  

sub process_blast {
  my $tax_method = "blast";
  my $otu_method = shift;
  my $blast = shift;
  my $id_tax = shift;
  my $basename = shift;

  # read id_tax in hash
  open (TAX, "<$id_tax") or die $!;
  my $tax={};
  my @tax_id;
  while (<TAX>) {
    chomp $_;
    my @cols = split /\t/, $_;
    my $tid = $cols[0];
    my $tlin = $cols[1];

    my @tlin_split = split /;/, $tlin;
    $tax->{$tid} = \@tlin_split;
    #print join(",", @{$tax->{$tid}}), "\n";
  }
  close TAX;
  
  open (INMAP, "<$blast") or die $!;

  my %blast_out=();
  my @rep_id; 
  while (my $line = <INMAP>) {
    chomp $line;
    my @fields = split /\t/, $line;
    my $rep = $fields[0];
    my $blast_hit = $fields[1];
    my @hit_lin= @{$tax->{$blast_hit}};
    push (@rep_id, $rep);
    print join(",", @hit_lin), "\n";
    #push(@{$blast_out->{$rep}}, \@hit_lin);
    $blast_out{$rep}{$blast_hit} = \@hit_lin;
  }
  close INMAP;
  my @rep_id_uniq = do { my %seen; grep { !$seen{$_}++ } @rep_id };
  print join(",", @rep_id_uniq), "\n";

  my %lins = {};
  my $lins_consen = {};
  foreach my $key (sort keys(%blast_out)) {
    #print $key, "\t", join(",", @{$blast_out->{$key}}), "\n";
    print $key, "\n";
    
    my %seen;
    while (my ($k, $v) = each $blast_out{$key} ) {
      #print $k, "\t", $v, "\n";
      $seen{$_}{$k} = 1 for @$v;
    }    

    my @lin_comm; 
    my %ranks=();
    #print "$_\n" for grep { keys %{$seen{$_}} == keys $blast_out{$key} } keys %seen;
    @lin_comm = grep { keys %{$seen{$_}} == keys $blast_out{$key} } keys %seen;
    #print join(";", @lin_comm), "\n";
    foreach my $rk ( @lin_comm) {
      if ($rk =~ m/k__/) {
        $ranks{"k__"} = $rk;
      } elsif ($rk =~ m/p__/) {
        $ranks{"p__"} = $rk;
      } elsif ($rk =~ m/c__/) {
        $ranks{"c__"} = $rk;
      } elsif ($rk =~ m/o__/) {
        $ranks{"o__"} = $rk;
      } elsif ($rk =~ m/g__/) {
        $ranks{"f__"} = $rk;
      } elsif ($rk =~ m/g__/) {
        $ranks{"g__"} = $rk;
      } elsif ($rk =~ m/s__/) {
        $ranks{"s__"} = $rk;
      } 
    }

    my @lin_comm_sort;
#    foreach my $k (keys %ranks) {
#      print $k, "\t", $ranks{$k}, "\n";
#    }
   
    if ( exists $ranks{"k__"} ) {
      push(@lin_comm_sort, $ranks{"k__"});
    }
    if ( exists $ranks{"p__"} ) { 
      push(@lin_comm_sort, $ranks{"p__"});
    }
    if ( exists $ranks{"c__"} )  {
      push(@lin_comm_sort, $ranks{"c__"});
    }
    if ( exists $ranks{"o__"} )  {
      push(@lin_comm_sort, $ranks{"o__"});
    }
    if ( exists $ranks{"f__"} )  {
      push(@lin_comm_sort, $ranks{"f__"});
    }
    if ( exists $ranks{"g__"} )  {
      push(@lin_comm_sort, $ranks{"g__"});
    }
    if ( exists $ranks{"s__"} )  {
      push(@lin_comm_sort, $ranks{"s__"});
    }
    print join(";", @lin_comm_sort), "\n";
    $lins_consen->{$key} = join(";", @lin_comm_sort);
  }
  #open (OUT, ">blast_assigned_taxonomy/$basename.$otu_method.rep_tax_assignments.txt
  foreach my $otu (sort keys %$lins_consens) {
    print 

}


