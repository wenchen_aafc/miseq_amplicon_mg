Getting started

# rename sff file "-" to "_"
mothur > sffinfo(sff=JBMDGAS01.MID31.RA_BAC_13_B265_Barley_st.sff, flow=T, processors=18)

mothur > summary.seqs(fasta=JBMDGAS01.MID31.RA_BAC_13_B265_Barley_st.fasta, processors=18)

		Start	End	NBases	Ambigs	Polymer
Minimum:	1	39	39	0	2
2.5%-tile:	1	116	116	0	4
25%-tile:	1	454	454	0	4
Median: 	1	512	512	0	5
75%-tile:	1	539	539	0	5
97.5%-tile:	1	558	558	1	6
Maximum:	1	1040	1040	28	31
# of Seqs:	763373
#This is the sequence profile for the entire half plate. In the flow files we have provided to you, there are a total of ~69000 sequences.

#Reducing sequencing error
#Using flowgrams
#Our SOP will use the shhh.flows command, which is the mothur implementation of the PyroNoise component of the AmpliconNoise suite of programs. Others that don't have the computational capacity or don't have their flow data may want to jump ahead to the trim.seqs section of the tutorial. The rest of the SOP will assume that you are using the SOP approach.

#To run shhh.flows, we need to process our flow data to do several things. First, we need to separate each flowgram according to the barcode and primer combination. We also need to make sure that our sequences are a minimum length and we need to cap the the number of flowgrams to that length. The default of trim.flows is 450 flows. If you are using GSFLX data you will probably want to play around with something in the order of 300-350 flows. We will also allow for 1 mismatch to the barcode and 2 mismatches to the primer. Running trim.flows will make use of the Oligos_File that is provided with the SOP Data that you pulled down from the wiki. You may want to change the number of processors to suit your hardware:
mothur > trim.flows(flow=JBMDGAS01.MID31.RA_BAC_13_B265_Barley_st.flow, minflow=352,maxflow=800, oligos=oligo.reverse.txt, pdiffs=2, bdiffs=1, processors=18)

#This will create 96 separate trim.flows and scrap.flows files as well as a file called GQY1XT001.flow.files, which holds the names of each of the trim.flows files. The folder you have pulled down has 21 of these flow files along with a file called GQY1XT001.flow.files. Next we will want to run shhh.flows to de-noise our sequence data. If you don't want take the time to run shhh.flows, you can use the processed files that are in the folder you pulled down above. shhh.flows can be run several ways depending on your hardware or operating system. The first way is to use the precomputed output for this dataset that came with the files you downloaded. Clearly this won't work for your own data :). Alternatively, we can stay within mothur and run the following:
mothur > shhh.flows(file=JBMDGAS01.MID31.RA_BAC_13_B265_Barley_st.flow.files, processors=18)
#It took me about 8 hours (wall time) to run shhh.flows on the 96 samples in GQY1XT001.flow.files with 8 processors. Note that if you are using data recently generated on the new 454 platform and chemistry there is a different flow order. If this is your case, you'll want to include order=B in trim.flows and shhh.flows. If you're using IonTorrent (why!?), you'll want to use order=I.

#The two important files from running shhh.flows are the shhh.fasta and shhh.names. You will now feed these into trim.seqs to actually remove the barcode and primer sequences, make sure everything is 200 bp long, remove sequences with homoopolymers longer than 8 bp, and get the reverse complement for each sequence. This will also create a new names file, which maps the names of redundant sequences to a unique sequence and it will create a group file that indicates what group or sample each sequence came from:
mothur > trim.seqs(fasta=GQY1XT001.shhh.fasta, name=GQY1XT001.shhh.names, oligos=GQY1XT001.oligos, pdiffs=2, bdiffs=1, maxhomop=8, minlength=200, flip=T, processors=2)
Lets see what the new fasta data look like:
mothur > summary.seqs(fasta=GQY1XT001.shhh.trim.fasta, name=GQY1XT001.shhh.trim.names)
To demonstrate the current option, notice that you can also run this command as follows and get the same output:
mothur > summary.seqs()

		Start	End	NBases	Ambigs	Polymer	NumSeqs
Minimum:	1	247	247	0	3	1
2.5%-tile:	1	255	255	0	4	1725
25%-tile:	1	261	261	0	4	17244
Median: 	1	266	266	0	4	34488
75%-tile:	1	274	274	0	5	51732 
97.5%-tile:	1	292	292	0	6	67251
Maximum:	1	312	312	0	8	68975
Mean:	1	268.629	268.629	0	4.36483
# of unique seqs:	21886
total # of seqs:	68975
If you are ever curious what is "current" and what isn't, you can run the get.current command:
mothur > get.current()


