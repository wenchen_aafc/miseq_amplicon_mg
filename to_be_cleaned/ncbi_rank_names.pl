#!/usr/bin/env perl

use strict;
use warnings;

use Data::Dumper;

open (NAMES, "<names.dmp");
open (NODES, "<nodes.dmp");
open (OUT, ">corrected.txt");

my %names;
my %nodes;
my %ranks;

my @tax_ranks = ("kingdom", "phylum", "class", "order", "family", "genus", "species");

while (my $line = <NAMES>) {
	if ($line !~ m/authority/ && $line !~ m/"/ && $line !~ m/\Qet al.\E/ && $line !~ m/ and / && $line !~ m/,/) {
		$line =~ s/\t//g;
		my @fields = split ("\Q|\E", $line);
		my $name = join ("_", split (" ", $fields[1]));
		$name =~ s/\.//g;
		
		push (@{$names{$fields[0]}}, $name);
	}
}

while (my $line = <NODES>) {
	$line =~ s/\t//g;
	my @fields = split ("\Q|\E", $line);
	
	foreach my $rank (@tax_ranks) {
		if ($fields[2] eq $rank && exists($names{$fields[0]})) {
			push (@{$ranks{$rank}}, @{$names{$fields[0]}});
		}
	}
}

while (my ($rank, $members) = each %ranks) {
	print OUT "$rank\t";
	
	foreach my $member (@$members) {
		print OUT "$member\t";
	}
	print OUT "\n";
}


close OUT;
close NAMES;
close NODES;
