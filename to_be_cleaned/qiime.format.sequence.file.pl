#!/usr/bin/env perl

#use warnings;

use strict;
use warnings;
use strict;
use warnings;
use File::Basename;
use Bio::SeqIO;
use File::Find;
use File::Spec;
use Carp;



########################################################

#my $path=shift or die "path to input fasta files?"; # fasta file

#my $dirname=dirname($path);
#print $dirname, "\n";
#opendir(DIR, "$dirname");
#my @FILES= readdir(DIR); 
#foreach my $file (@FILES) {
#	my $fasta=basename($file);
	my $fasta=shift; 
	my $name=basename($fasta);
	my $basename=basename($fasta, ".fasta");
	print "Start to process\t$name\n";
	my @col=split /_/, $name;
	my $sampleid=$col[0];
#	print "\n$sampleid\n";
	open (OUT, ">$basename.qiime.fasta");
	my $in_fasta = Bio::SeqIO->new(-file=>"$fasta", -format=>"Fasta");
	my $count=1;
	while (my $seq = $in_fasta->next_seq()) {
		my $seqid=join("_",$sampleid, $count);
#		print "\n$seqid";
		my $seqdesc=$seq->id; #obtain description line of a sequence
		$count++;
		print OUT ">", $seqid, "\t",$seqdesc, "\n", $seq->seq, "\n";
#	}	
}
print "Completed converting\t$fasta\n";

close OUT;
