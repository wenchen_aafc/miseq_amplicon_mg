  ###############################################
  # Step4: extract ITS1 & 2 regions
  ###############################################
  chdir $outdir;
  
  mkdir "ITS";
  sub_1::fastq2fasta2_1 ("$outdir/raw_data", "$outdir/ITS"); # only convert trim.fastq files
  chdir "ITS";
  mkdir "temp";
  
  open (DIR, ".");
  my @fas = sort (grep $_ =~ m/_R1_/, <*>);
  sub_1::ITS_1("all_forward.qiime.recount.sorted", $threads, $kingdoms); 
  # all_forward.ITS1.fasta; all_forward.ITS2.fasta, maybe empty

  sub_1::qiime_format_1 ();
  system("mv *trim.fasta temp");
  #sub_1::cat_1 ("all_forward");
  #system("mv *.trim.qiime.fasta temp");
  # join joined with uniq forward;
  mkdir "forward";
  system("mv *_R1_*trim.qiime.fasta forward");
  chdir "forward";
  mkdir "temp";
  sub_1::cat_1("all_forward");
  sub_1::recount_1("all_forward.qiime.fasta");
  system("mv *_R1_* temp | mv all_forward.qiime.fasta temp ");
  chdir $outdir;

  
  print "Processing forward sequences\n";
  chdir "forward";
  # extract ITS1&2 from all_forward.qiime.recount.sorted.fasta
  
  
  ## ITS1
  if ( -z "all_forward.qiime.recount.sorted.ITS1.fasta" ) {
    print "No ITS1 being extracted from forward sequences!\n";
  } else {


    sub_1::sort_seq_length_1($minlen, "all_forward.qiime.recount.sorted.ITS1.fasta");
    system("mv all_forward.*ITS1.fasta temp");
    # rename all_forward.qiime.recount.sorted.ITS1.sorted.fasta
    system("mv all_forward.qiime.recount.sorted.ITS1.sorted.fasta all_forward.ITS1.qiime.recount.sorted.fasta");
    my $basename_f = sub_1::pick_otu_rep_1 ($otu_m, $similarity, $OTU_size, $max_memory, $threads, "", "all_forward.ITS1");
    print $basename_f, "\n\n";
    system("mv *qiime.recount.sorted.fasta temp");
    sub_1::assign_taxon_1 ($otu_m, $tax_m, $ref_seqs, $id_tax, $confidence, $max_memory, $threads, $basename_f);
    sub_1::otu_table_1 ($otu_m, $tax_m, $basename_f);
    sub_1::otu_table_1 ($otu_m, "classify.seqs", $basename_f);    
    chdir $outdir;
  }

  ## ITS2
  if ( -z "all_forward.qiime.recount.sorted.ITS2.fasta" ) {
    print "No ITS2 being extracted from forward sequences!\n";
  } else {
    sub_1::sort_seq_length_1($minlen, "all_forward.qiime.recount.sorted.ITS2.fasta");
    system("mv all_forward.*ITS2.fasta temp");
    # rename all_forward.qiime.recount.sorted.ITS1.sorted.fasta
    system("mv all_forward.qiime.recount.sorted.ITS2.sorted.fasta all_forward.ITS2.qiime.recount.sorted.fasta");
    my $basename_f = sub_1::pick_otu_rep_1 ($otu_m, $similarity, $OTU_size, $max_memory, $threads, "", "all_forward.ITS2");
    print $basename_f, "\n\n";
    system("mv *qiime.recount.sorted.fasta temp");
    sub_1::assign_taxon_1 ($otu_m, $tax_m, $ref_seqs, $id_tax, $confidence, $max_memory, $threads, $basename_f);
    sub_1::otu_table_1 ($otu_m, $tax_m, $basename_f);
    sub_1::otu_table_1 ($otu_m, "classify.seqs", $basename_f);    
    chdir $outdir;
  }

  ####################
  ## Reverse folder
  ####################
  print "Processing reverse sequences\n";
  chdir "Reverse";
  # extract ITS1&2 from all_reverse.rc.qiime.recount.sorted.fasta
  
  sub_1::ITS_1("all_reverse.rc.qiime.recount.sorted", $threads, $kingdoms); 
  # all_reverse.rc.ITS1.fasta; all_reverse.rc.ITS2.fasta, maybe empty

  ## ITS1
  if ( -z "all_reverse.rc.qiime.recount.sorted.ITS1.fasta" ) {
    print "No ITS1 being extracted from reverse.rc sequences!\n";
  } else {
    sub_1::sort_seq_length_1($minlen, "all_reverse.rc.qiime.recount.sorted.ITS1.fasta");
    system("mv all_reverse.rc.*ITS1.fasta temp");
    # rename all_forward.qiime.recount.sorted.ITS1.sorted.fasta
    system("mv all_reverse.rc.qiime.recount.sorted.ITS1.sorted.fasta all_reverse.rc.ITS1.qiime.recount.sorted.fasta");
    my $basename_f = sub_1::pick_otu_rep_1 ($otu_m, $similarity, $OTU_size, $max_memory, $threads, "", "all_reverse.rc.ITS1");
    print $basename_f, "\n\n";
    system("mv *qiime.recount.sorted.fasta temp");
    sub_1::assign_taxon_1 ($otu_m, $tax_m, $ref_seqs, $id_tax, $confidence, $max_memory, $threads, $basename_f);
    sub_1::otu_table_1 ($otu_m, $tax_m, $basename_f);
    sub_1::otu_table_1 ($otu_m, "classify.seqs", $basename_f);    
    chdir $outdir;
  }

  ## ITS2
  if ( -z "all_reverse.rc.qiime.recount.sorted.ITS2.fasta" ) {
    print "No ITS2 being extracted from reverse.rc sequences!\n";
  } else {
    sub_1::sort_seq_length_1($minlen, "all_reverse.rc.qiime.recount.sorted.ITS2.fasta");
    system("mv all_reverse.rc.*ITS2.fasta temp");
    # rename all_forward.qiime.recount.sorted.ITS1.sorted.fasta
    system("mv all_reverse.rc.qiime.recount.sorted.ITS2.sorted.fasta all_reverse.rc.ITS2.qiime.recount.sorted.fasta");
    my $basename_f = sub_1::pick_otu_rep_1 ($otu_m, $similarity, $OTU_size, $max_memory, $threads, "", "all_reverse.rc.ITS2");
    print $basename_f, "\n\n";
    system("mv *qiime.recount.sorted.fasta temp");
    sub_1::assign_taxon_1 ($otu_m, $tax_m, $ref_seqs, $id_tax, $confidence, $max_memory, $threads, $basename_f);
    sub_1::otu_table_1 ($otu_m, $tax_m, $basename_f);
    sub_1::otu_table_1 ($otu_m, "classify.seqs", $basename_f);    
    chdir $outdir;
  }

