#!/opt/perl/bin/perl

use warnings;
use strict;
use Bio::SeqIO;
use File::Basename;
use Getopt::Long;
use Pod::Usage;
use Bio::Index::Fasta;
use File::Find;
use Data::Dumper;

  my $minlen= shift;
  my $fastafile=shift or die "fasta file?";
  my @suffix = (".fasta", ".fas", ".fna", ".fa");
  my $basename=basename ($fastafile, @suffix);
  my $name=basename($fastafile);
  print "Start to process\t$name\n";
# print "\n$sampleid\n";
  open (OUT, ">$basename.sort.fasta");

  my $s_wrote1;
  my $s_wrote2;
  my $s_read1;

  my $in=Bio::SeqIO->new(-file=>"$fastafile", -format=>"fasta");
  my $seq_des = {};
  my $seq_seq = {};
  my $seq_length = {};
  my %ids1=();
  my $id;
  my $des;
  my $sequence;

  while (my $seq=$in->next_seq()) {
    $id=$seq->id;
    $des=$seq->desc;
    $sequence=$seq->seq;
    $seq_des->{$id}=$des;
    $seq_seq->{$id}=$sequence;
    $seq_length->{$id} = length($seq->seq);
    $s_read1++; 
    $ids1{$id}++; 
  }

  # sort hash value descending
  my @id_sort = sort { $seq_length->{$b} <=> $seq_length->{$a} } keys(%$seq_length);
  my %seen=();
  my @id_sort_uniq = grep { !$seen{$_}++ } @id_sort;
  #my @length = @{$h}{@keys};

  $s_wrote2=0;
  for my $k (@id_sort) {
    my $len = length($seq_seq->{$k});
    if ($len >= $minlen) { 
      print OUT ">", $k, " ", $seq_des->{$k}, "\n", $seq_seq->{$k}, "\n";
      $s_wrote2++;
    }
  }
 
  print "sorted $s_read1 FASTA records.\nsorted and kept $s_wrote2 sequences longer than $minlen.\n file write to $basename.sort.fasta\n\n"; 

