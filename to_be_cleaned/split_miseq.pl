#!/usr/bin/env perl

use strict;
use warnings;
use Bio::SeqIO;
use Getopt::Long;
use File::Basename;
use File::Copy;
use File::Spec;
use FindBin;
use Cwd;

#modules
use lib "$FindBin::Bin"; #Path to the modules
use OTU;
use Convert;
use Qual_Trim;
use LogFile;
use sub_1;

my $oligo_file="/isilon/biodiversity/users/chenw_lab/Processing/New_Miseq_Data/oligo_file_ITS52_34";
my $outdir = "/isilon/biodiversity/users/chenw_lab/Processing/New_Miseq_Data/150414_test2_output3";
my $threads = 12;
# check format of $oligo_file
unless ( -e $oligo_file) {
  die "please provide primer information in the following format and then re-try:\n\nforward\tGGAAGTAAAAGTCGTAACAAGG\tITS1_region:forward:ITS5\nforward\tGCTGCGTTCTTCATCGATGC\tITS1_region:reverse:ITS2\n\n
: $!";
}
open (OL, "<$oligo_file");
my @regions;
my @region_primer;
while (<OL>) {
  chomp $_;
  print $_, "\n";
  if ( $_ =~ /#/ ) {
    next
  } else {
    my ($oli, $sequence, $primer) = split /\t/, $_;
    my ($region, $direction, $primer_name) = split /:/, $primer;
    push (@regions, $region);
    push (@region_primer, $primer);
  }
}
close OL;
  ##### split foward and reverse sequences
chdir $outdir;
foreach my $regi (@regions) {
  mkdir "$outdir/$regi";
  mkdir "$outdir/$regi/temp";
  mkdir "$outdir/$regi/fast_qc";
}

#system("cp $outdir/raw_data/*fastq $outdir/temp");
chdir "$outdir/raw_data";
print "$outdir/raw_data", "\n";
opendir(DIR, "$outdir/raw_data") or die "$!";
#my @raw_fastqF = grep(/.*_R1_.*fastq$/,readdir(DIR));
#my @raw_fastqR = grep(/.*_R2_.*fastq$/,readdir(DIR));
my @raw_fastq = grep(/\.fastq$/,readdir(DIR));
#print join(",", @raw_fastqF), "\n";
#print join(",", @raw_fastqR), "\n";
print join(",", @raw_fastq), "\n";

foreach my $fq (@raw_fastq) {
  print $fq, "\n";
  sub_1::fastq2fasta2_1($fq);
}
system("mv *fasta $outdir/temp");
closedir(DIR);

chdir "$outdir/temp";
#opendir(DIR, "$outdir/temp");
opendir(DIR, ".");
my @raw_fastaF = grep($_=~ m/_R1_/ && $_=~ m/\.fasta$/,readdir(DIR));
closedir(DIR);

opendir(DIR, ".");
my @raw_fastaR = grep($_=~ m/_R2_/ && $_=~ m/\.fasta$/,readdir(DIR));
closedir(DIR);

opendir(DIR, ".");
my @raw_fasta = grep($_=~ m/\.fasta$/,readdir(DIR));
closedir(DIR);

print scalar @raw_fastaF, " forward files: ", join(", ", @raw_fastaF), "\n";
print scalar @raw_fastaR, " reverse files: ", join(", ", @raw_fastaR), "\n";


split_miseq($oligo_file, $outdir, "$outdir/temp", $threads);

sub split_miseq {
  my $oligo_file = shift;
  my $outdir = shift;
  my $dir = shift;
  my $threads = shift;
  opendir(DIR, "$dir");
  my @raw_fasta = grep(/\.fasta$/,readdir(DIR));
  closedir(DIR);
  open (OL, "<$oligo_file");
  while (<OL>) {
    chomp $_;
    if ( $_ =~ m/#/) {
      next;
    } else {
      #print $_, "\n";
      my ($olig, $seq, $prim) = split /\t/, $_;
      my ($reg, $primer_direction, $primer_name) = split /:/, $prim;
      
      if ( $primer_direction eq "forward" ) {
        open (TEMP, ">temp.oligo");
        print $_, "\n";
        print TEMP $_;
        close TEMP;
        foreach my $fasta (@raw_fastaF) {
          my $fasta_base = basename($fasta, ".fasta");
          print "Start extract $prim from $fasta_base.fastq:\n";
          open (MOTHUR, ">mothur.bat");
          print MOTHUR "trim.seqs(fasta=$fasta, oligos=temp.oligo, pdiffs=2)\n";
          close MOTHUR;
          system("mothur mothur.bat");
          #unlink "temp.oligo";
        #system("mv $basename.trim.fasta $outdir/$reg/$fasta");
          my $prim_new =$prim;
          $prim_new=~ s/:/_/g;
          unless ( -z "$fasta_base.groups" ) {
            system("cut -f1 $fasta_base.groups |sort|uniq|sed 's/_/:/g' > $fasta_base.$prim_new.id");
            my $count_all=`grep '>' $fasta |wc -l`;
            chomp $count_all;
            my $count= `wc -l $fasta_base.$prim_new.id`;
            chomp $count;
            system("~/seqtk/seqtk subseq $outdir/raw_data/$fasta_base.fastq $fasta_base.$prim_new.id > $outdir/$reg/$fasta_base.fastq");
            print "subset $count from $fasta_base.fastq to $outdir/$reg/$fasta_base.fastq, \n\n";
          }
        }
      } elsif ( $primer_direction eq "reverse" ) {
        open (TEMP, ">temp.oligo");
        print $_, "\n";
        print TEMP $_;
        close TEMP;
        foreach my $fasta (@raw_fastaR) {
          print $fasta, "\n";
          my $fasta_base = basename($fasta, ".fasta");
          print "Start extract $prim from $fasta_base.fastq:\n";
          open (MOTHUR, ">mothur.bat");
          print MOTHUR "trim.seqs(fasta=$fasta, oligos=temp.oligo, pdiffs=2)\n";
          close MOTHUR;
          system("mothur mothur.bat");
          #unlink "temp.oligo";
        #system("mv $basename.trim.fasta $outdir/$reg/$fasta");
          my $prim_new =$prim;
          $prim_new=~ s/:/_/g;
          unless ( -z "$fasta_base.groups" ) {
            system("cut -f1 $fasta_base.groups |sort|uniq|sed 's/_/:/g' > $fasta_base.$prim_new.id");
            my $count_all=`grep '>' $fasta |wc -l`;
            chomp $count_all;
            my $count= `wc -l $fasta_base.$prim_new.id`;
            chomp $count;         
            system("~/seqtk/seqtk subseq $outdir/raw_data/$fasta_base.fastq $fasta_base.$prim_new.id > $outdir/$reg/$fasta_base.fastq");
            print "subset $count from $fasta_base.fastq to $outdir/$reg/$fasta_base.fastq, \n\n";
          }
        }
      } 
    } 
  }
}
close OL;

#system("rm *.fna* | rm *.qual | rm *trim.fasta| mv mothur* temp | mv *scrap* temp | mv *.groups temp | mv *id temp");
#system("mv *blast temp | mv *id temp");

chdir $outdir;

